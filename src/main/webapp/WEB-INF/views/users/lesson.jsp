<section class="lesson">
    <div class="container">
        <div class="lesson-header">
            <h1 class="lesson-name" id="lesson_name">My Lessons</h1>
            <div class="lesson-number">
                <h4>Lessons
                    <span id="currentLessonCount">0</span>
                    <cite id="totalLessonCount">/0</cite>
                </h4>
            </div>
        </div>
        <ul id="lesson_ul">
            <li class="lesson-main">
                <div class="text">
                    <p></p>
                </div>
                <div class="image">
                    <img src="">
                </div>
                
                <div class="video">
                    <video controls width="521" height="309">
                        <source src="" type="video/mp4">
                    </video>
                </div>
            </li>
        </ul>
        <div class="footer-wrapper">
            <div class="mark-complete">
                <label class="b-contain">
                    <span id="complete_label">Mark as completed</span>
                    <input type="checkbox" id="complete" class="checked">
                    <div class="b-input"></div>
                </label>
            </div>
            <div class="btn-wrapper">
                <button id="lesson-prev" class="btn-sec">
                    <a class="prev"></a> Previous Lesson</button>
                <button id="lesson-next" class="btn-primary" style="float: right;">
                    <a class="next"></a> Next Lesson</button>
                <button id="take-quiz" class="btn-primary hide" style="float: right;">
                    <a class="next"></a> Take Quiz</button>
            </div>
        </div>
    </div>
</section>