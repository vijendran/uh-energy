<section class="course-listing">
    <div class="container" id="course_container">
        <div class="lesson-header">
            <h1 class="lesson-name">My Courses</h1>
            <div class="lesson-number">
                <h4 id="course_count">0 courses</h4>
            </div>
        </div>


        <section class="assigned-course">
            <div class="create-course course-pos hide">
                <h4>Add New Course</h4>
            </div>
            <div class="course">
                <img src="">
                <div>
                    <h4 class="course-name">Foundation course to under stand about Software</h4>
                    <span class="lesson-count" id="lesson_count">23 lessons</span>
                </div>
            </div>
        </section>
        <section class="unassigned-courses hide">
            <h2>Recommended courses to learn</h2>
            <div class="course">
                <img src="">
                <div>
                    <h4 class="course-name">Foundation course to under stand about Software</h4>
                    <span class="lesson-count">23 lessons</span>
                </div>
            </div>
            <div class="course">
                <img src="">
                <h4 class="course-name">Foundation course to under stand about Software</h4>
                <span class="lesson-count">23 lessons</span>
            </div>
        </section>
    </div>
</section>