<section class="quiz">
  <div id="quiz-container"class="container">
    <div class="lesson-header">
      <h1 class="lesson-name">Questions</h1>
      <div class="lesson-number">
        <h4>
          <span id="currentLessonCount"></span>
          <cite id="totalLessonCount"></cite>
        </h4>
      </div>
    </div>
    <div class="question-holder">
      <ul class="qa template" id="single-question">
        <template id="question">
          <li id="test" class="question">
            <p class="question-template">What is your favourite color</p>
            <div class="options-template"></div>
          </li>
        </template>
      </ul>
      <p id="quiz-result" class="quiz-status">
        <span></span>
      </p>
    </div>
    <div class="btn-wrapper">
      <button id="go_to_lesson" class="btn-sec">
      <a class="prev"></a> Go To Lesson</button>
      <button class="btn-primary submit-question">Submit</button>
      <button id="quiz-next" class="btn-primary" style="float: right;">
        <a class="next"></a> Next Question</button>
    </div>
  </div>

  <div id="summary" class="summary hide">
      <span class="pass-percentage"></span>
      <p class="message"></p>      
  </div>
 

  <template id="singleselect">
    <label class="b-contain">
      <span class="singleselect">First radio</span>
      <input type="radio" class="options">
      <div class="b-input"></div>
    </label>
  </template>

  <template id="multiselect">
    <label class="b-contain">
      <span class="multiselect">Option 1</span>
      <input type="checkbox" class="options">
      <div class="b-input"></div>
    </label>
  </template>

  <template id="trueorfalse">
    <label class="b-contain">
      <span class="trueorfalse">First radio</span>
      <input type="radio" class="options">
      <div class="b-input"></div>
    </label>
  </template>


  <template id="paragraph">
    <span>
      <textarea class="paragraph" placeholder="Type your answer here ..."></textarea>
    </span>
  </template>

</section>