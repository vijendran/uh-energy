<!DOCTYPE html>
<html lang="en">

<head>
    <title>Energy Project</title>
    <link rel="stylesheet" href="../../../css/main.css">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;600;700&family=Rubik:ital,wght@0,400;0,500;0,700;0,900;1,400&display=swap"
        rel="stylesheet">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glightbox/dist/css/glightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
    
    <script src="https://cdn.jsdelivr.net/gh/mcstudios/glightbox/dist/js/glightbox.min.js"></script>
    <script src="../../../js/utils/xhr.js"></script>
    <script src="../../../js/utils/util.js?v=${version}"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <script>
        
    </script>
</head>
<header>
    <a id="home" href="/course"class="logo">Home</a>
    <div id="admin-header" class="admin-menu">
        <a id="dashboard-tab" href="/admin/admin-dashboard">Dashboard</a>
        <a id="approval-tab" href="/admin/admin-approval">Requests</a>
    </div>
    <div class="profile">
        <a class="profile-name"></a>
        <ul class="profile-dd">
            <li id="profile-view">
                <span>My Profile</span>
            </li>
            <li>
                <a href="/logout">Logout</a>
            </li>
        </ul>
    </div>
</header>
<body>
    <div class="backdrop"></div>
    <div id="status-msg" class="vb hide">
        <span id="content"></span>
    </div>
    <section class="modal profile-modal">
       <div class="modal-header"> 
         <h3>My Profile</h3>
         <i class="close"></i>
       </div>
       <div class="modal-content">
            <div class="main-profile">
                <label>User Name</label>
                <input class="course-name" type="text">
                <a id="update-password" class="update-password"> Update Password </a>
            </div>
            <div id="pwd-msg"class="pw-msg hide">
                <p>An Email has been sent to you on updating your password! Please check!</p>
            </div>
        </div>
       <div class="modal-footer">
         <button id="profile-save"class="btn-primary">Save</button>
         <span id="profile-cancel" class="cancel">Cancel</span>
       </div>
     </section>

    <button id="upload" class="hide">upload</button>

    <div id="contactID" style="display:none;">${contactID}</div>
    <div id="version" class="hide" style="display:none;">${version}</div>
    <div id="page" style="display:none;">${page}</div>
    <div id="user" style="display:none;">${user}</div>


    <article id="view-page">
        <%@ include file="./lesson.jsp" %>
            <%@ include file="./course-listing.jsp" %>
                <%@ include file="./quiz.jsp" %>
                    <%@ include file="../admin/course-creation.jsp" %>
                        <%@ include file="../admin/lesson-creation.jsp" %>
                            <%@ include file="../admin/subscription.jsp" %>
    </article>

    <script type="text/javascript" src="../../../js/history.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/upload/uploadFile.js?v=${version}"></script>
    <script type="text/javascript" src=".../../../js/content/course.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/admin/subscription.js?v=${version}"></script>

    <script type="text/javascript" src="../../../js/admin/course-creation.js?v=${version}"></script>
    <script type="text/javascript" src=".../../../js/content/lesson.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/content/quiz-content.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/admin/creation-utils.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/admin/quiz-creation.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/admin/lesson-creation.js?v=${version}"></script>
    <script type="text/javascript" src="../../../js/page.js?v=${version}"></script>
</body>

</html>