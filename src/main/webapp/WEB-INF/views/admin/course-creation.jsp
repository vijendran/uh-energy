<section id="course-creation" class="course-listing course-creation">
 <div class="container">
     <section class="modal creation-modal">
       <div class="modal-header"> 
         <h3>Add New Course</h3>
         <i class = "close"></i>
       </div>
       <div class="modal-content">
         <label> Course Name</label>
         <input class="course-name"type ="text" />
         <div class="pass-percentage">
            <label> Pass Percent</label>
            <input id="percentage" class="course-name">
            <span>%</span>
         </div>
         <img class="course-image-preview" src="https://storage.googleapis.com/gajendran-mohan.appspot.com/test/course-dummy.jpg">
         <span id="course-image" class="upload">
          <p>+ Upload Course Image</p>
         </span>
       </div>
       <div class="modal-footer">
         <button id="course-create" class="btn-primary">Proceed</button>
         <span class = "cancel">Cancel</span>
       </div>
     </section>
 </div>
 <template class="course">
  <img src="">
  <div>
      <h4 class="course-name">Foundation course to under stand about Software</h4>
      <span class="lesson-count" id="lesson_count">23 lessons</span>
  </div>
 </template>
</section>
