<section id="data-reports" class="admin-dashboard">

  <div id="user-requests" class="table-wrapper">
    <table id="subscription" class="table hide">
      <thead>
        <tr>
          <th class="table-header">Name</th>
          <th class="table-header">Email</th>
          <th class="table-header">Course Name</th>
          <th class="table-header">Date Requested</th>
          <th class="table-header"></th>
          <th class="table-header"></th>
        </tr>
      </thead>
      <tbody id="subscription-list">
        <tr class="subscription-template" style="display:none;">
          <td>
            <span class="name">Pooja</span>
          </td>
          <td>
            <span class="email">pooja.msd@gmail.com</span>
          </td>
          <td>
            <span class="c-name">Electrical Engineering</span>
          </td>
          <td>
            <span class="date-added dateAdded">06-08-2010</span>
          </td>
          <td>
            <a class="approve action-approve">Approve</a>
          </td>
          <td>
            <a class="reject deny-request">Reject</a>
          </td>
        </tr>

      </tbody>
    </table>
  </div>


  <div id="stats-dashboard" class="stats table-wrapper">
    <div class="chart">
      <canvas id="myChart"></canvas>
    </div>
    <table id="quiz-stats" class="table hide">
      <thead>
        <tr>
          <th class="table-header">Name</th>
          <th class="table-header">Email</th>
          <th class="table-header">Course Name</th>
          <th class="table-header">Date Completed</th>
          <th class="table-header">Status</th>
        </tr>
      </thead>
      <tbody id="course-status">
        <tr class="quiz-stat-template" style="display:none;">
          <td>
            <span class="name">Pooja</span>
          </td>
          <td>
            <span class="email">pooja.msd@gmail.com</span>
          </td>
          <td>
            <span class="c-name">Electrical Engineering</span>
          </td>
          <td>
            <span class="dateCompleted">06-08-2020</span>
          </td>
          <td>
            <span class="status">Pass</span>
          </td>
        </tr>

      </tbody>
    </table>
  </div>
</section>