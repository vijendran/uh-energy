<section class="create-lesson">
  <div class="course-head">
    <h4> Course Name </h4>
    <button class="publish btn-primary" id="publish_btn">Publish</button>
    <spam id="course-cancel" class="cancel">Cancel</spam>
  </div>
  <div class="steps">
    <span class="go-back"></span>
    <div id="header-tabs" class="">
      <a id="lesson-tab" class="">Lesson</a>
      <a id="quiz-tab" class="">Quiz</a>
    </div>
  </div>
  <article class="creation-main">
    <aside class="sidebar">
      <ul class="" id="sidebar_ul">
        <li class="add-lesson" id="sidebar-add"></li>
      </ul>
    </aside>
    <article class="content-main create-lesson">
      <input type="text" placeholder="Enter the lesson name" id="les_name" />
      <div class="component">
        <h5>Add Component</h5>
        <a class="pill" id="add_text">+ Add Text</a>
        <a class="pill" id="add_image">+ Add Image</a>
        <a class="pill" id="add_video">+ Add Video</a>
      </div>
      <section class="modal text-modal">
        <div class="modal-header">
          <h3>Create Lesson Text</h3>
          <i class="close"></i>
        </div>
        <div class="modal-content">
          <div id="editor" class="editor">
          </div>
        </div>
        <div class="modal-footer">
          <button id="text_save" class="btn-primary">Save</button>
          <span id="lesson_cancel" class="cancel">Cancel</span>
        </div>
      </section>
      </div>
      <ul class="lesson-main" id="lesson_main_ul"></ul>
      <button id="lesson_save" class="btn-primary">Save</button>
      <span id="lesson_admin_cancel" class="cancel">Cancel</span>
      <span class="delete" id="delete_btn">Delete Lesson</span>
    </article>

    <article class="content-main create-quiz">
      <ul id="quiz-view"></ul>
      <button id="save" class="btn-primary">Save</button>
      <span id="quiz-cancel" class="cancel">Cancel</span>
      <span class="delete" id="quiz-delete_btn">Delete Quiz</span>
    </article>
  </article>


  <template id="sidebar_li">
    <li>Untitled</li>
  </template>

  <template id="basic_li">
    <li class="li-main lesson_main"></li>
  </template>

  <template id="each-question">
    <li class="each-question">
      <input class="question-input" type="text" placeholder="Enter the question name" />
      <div class="question-type">
        <select>
          <option value="singleselect-create">Single Select</option>
          <option value="multiselect-create">Multi Select</option>
          <option value="trueorfalse-create">True / False</option>
          <option value="paragraph-create">Open Ended</option>
        </select>
      </div>
      <ul class="lesson-main" id="quiz_main_ul"></ul>
    </li>
  </template>

  <template id="singleselect-create">
    <li class="question-options">
      <input class="input-option" type="text" placeholder="Option 1" />
      <span class="b-contain quiz-input">
        <input type="radio" class="options">
        <div class="b-input"></div>
      </span>
    </li>
  </template>

  <template id="multiselect-create">
    <li class="question-options">
      <input class="input-option" type="text" placeholder="Option" />
      <span class="b-contain quiz-input">
        <input type="checkbox" class="options">
        <div class="b-input"></div>
      </span>
    </li>
  </template>

  <template id="trueorfalse-create">
    <li class="question-options">
      <input class="input-option" type="text" placeholder="Option" />
      <span class="b-contain quiz-input">
        <input type="radio" class="options">
        <div class="b-input"></div>
      </span>
    </li>
  </template>


  <template id="paragraph-create">
    <li>
      <span>
        <textarea class="paragraph" placeholder="Type your answer here ..."></textarea>
      </span>
    </li>
  </template>

  <template id="lesson_image_template">
    <div class="image" id="image_creation_div">
      <span class="loader"></span>
      <a href="">
        <img src="">
      </a>
      <a class="delete-item">Delete</a>
    </div>
  </template>

  <template id="lesson_text_template">
    <div class="text" id ="lesson_text_div">
      <div class="lesson_text"></div>
      <a class="delete-item">Delete</a>
    </div>
  </template>

  <template id="lesson_video_template">
    <div class="video" id="video_creation_div">
      <span class="loader"></span>
      <a href="">
        <video controls="" width="521" height="309" controlslist="nodownload">
          <source class= "video_src" src="" type="video/mp4">
        </video>
      </a>
      <a class="delete-item">Delete</a>
    </div>
  </template>

</section>