<!DOCTYPE html>
<html lang="en">

<head>
    <title>Energy Project</title>
    <link rel="stylesheet" href="../../../css/main.css">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;600;700&family=Rubik:ital,wght@0,400;0,500;0,700;0,900;1,400&display=swap"
        rel="stylesheet">
    <script type="text/javascript" src="/js/utils/util.js?v=${version}"></script>
    <script src="../../../js/utils/xhr.js"></script>
</head>

<body>
    <div id="status-msg" class="auth vb hide">
        <span id="content"></span>
    </div>
    <section class="signup-main">
        <div class="signup-img"></div>
        <p class="success-msg hide">Admin is notified of your request and you will receive an email once he approves</p>
        <div class="signup">
            <h2>Register</h2>
            <div class="input-group">
                <label>Name</label>
                <input class="input" name="name" type="text" placeholder="Enter your name">
            </div>
            <div class="input-group">
                <label>Email</label>
                <input class="input" name="email" type="email" placeholder="Enter your email">
            </div>
            <div class="input-group">
                <label>Phone Number</label>
                <input class="input" name="phone_no" type="tel" placeholder="Enter your phone number">
            </div>
            <div class="input-group">
                <label>Select your course</label>
                <select id="course-list"style="font-size: 14px;">
                    <option value="Select your course">Select your course</option>
                </select>
            </div>
            <button id="btn_primary" class="btn-primary signup-btn">Register with us</button>
            <div>
                <p class="exist">Already a user?
                    <a href="/login"> Log In</a>
                </p>
            </div>
        </div>
    </section>

                    
    <template id=course-list-option>
            <option value=""></option>
    </template>

    <script type="text/javascript" src="/js/authenticate/signup.js?v=${version}"></script>
</body>

</html>