<!DOCTYPE html>
<html lang="en">

<head>
    <title>Energy Project</title>
    <link rel="stylesheet" href="../../../css/main.css">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;600;700&family=Rubik:ital,wght@0,400;0,500;0,700;0,900;1,400&display=swap"
        rel="stylesheet">
    <script src="../../../js/utils/xhr.js"></script>
    <script src="../../../js/utils/util.js?v=${version}"></script>
</head>

<body>
    <div id="version" class="hide" style="display:none;">${version}</div>
    <div id="status-msg" class="auth vb hide">
        <span id="content"></span>
    </div>
    <section class="signup-main">
        <div class="login-img"></div>
        <div class="signup login">
            <h2>Login</h2>
            <div class="login-group">
                <div class="input-group">
                    <label>Email</label>
                    <input id="email" class="input" type="email" placeholder="Enter your email">
                </div>
                <div class="input-group">
                    <label>Password</label>
                    <input id="password" class="input" type="password" placeholder="Enter your password">
                </div>
                <button id="login" class="btn-primary signup-btn">Log in </button>
                <div>
                    <p class="exist">New User?
                        <a href="/signup"> Register</a>
                    </p>
                </div>
            </div>
            <script src="../../../js/authenticate/login.js?v=${version}"></script>
    </section>
</body>

</html>