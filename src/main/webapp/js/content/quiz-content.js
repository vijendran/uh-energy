async function getQuizForACourse(courseID) {

    if (document.querySelectorAll('.options')) {
        document.querySelectorAll('.options').forEach(e => e.classList.remove('checked'));
    }

    if (document.querySelectorAll('.paragraph')) {
        document.querySelectorAll('.paragraph').forEach(e => e.value = '');
    }

    let response = await xhr("GET", "/api/quiz?courseID=" + courseID);

    if (response && response.success && response.data.quiz) {
        document.getElementById('single-question').querySelectorAll('li').forEach(function (node) {
            node.remove();
        });

        for (var object in response.data.quiz) {

            let quiz = response.data.quiz;
            var parentNode = document.getElementById('single-question');

            parentNode.setAttribute("c_id", courseID);

            var temp = document.getElementById('question').content.cloneNode(true);
            temp.querySelector('.question').setAttribute('id', quiz[object].key);
            temp.querySelector('.question').setAttribute('type', quiz[object].type);

            temp.querySelector('.question-template').textContent = quiz[object].question.value;

            if (quiz[object].type == "paragraph") {
                var template = document.querySelector('#' + quiz[object].type);
                var clonedNode = template.content.cloneNode(true);
                temp.querySelector('.options-template').appendChild(clonedNode);
            } else {
                for (var eachOption in quiz[object].options) {
                    var template = document.querySelector('#' + quiz[object].type);
                    var clonedNode = template.content.cloneNode(true);
                    clonedNode.querySelectorAll('.' + quiz[object].type)[0].innerHTML = quiz[object].options[eachOption];
                    clonedNode.querySelectorAll('.options')[0].addEventListener('click', (e) => {
                        selectUnselect(e.target);
                    });
                    temp.querySelector('.options-template').appendChild(clonedNode);
                }
            }

            parentNode.appendChild(temp);

        }
        document.querySelector('#single-question > li').classList.add('active');
        document.querySelector('.quiz-status').classList.add('hide');
        document.getElementById('quiz-next').classList.add('hide');


    } else statusMsg(capitalize(response.errorMessage), "error");
}


var submitButton = document.getElementsByClassName('submit-question');

for (let i = 0; i < submitButton.length; i++) {
    submitButton[i].addEventListener("click", function (e) {


        validateCorrectAns(this.parentNode);
    });
}

if (document.getElementById('quiz-next')) {
    document.getElementById('quiz-next').addEventListener('click', () => {
        quizNext();
    });
}

async function validateCorrectAns(question) {

    document.getElementById('go_to_lesson').classList.add('hide');
    let node = document.getElementById('single-question').querySelectorAll('li.active');

    if(!node){
        statusMsg("Please select any answer", "error");
        return;
    }

    let selectedAnswerArray = [];

    
    let option = node[0].querySelector('.options-template').querySelectorAll('input.checked');

    for (var i = 0, element; element = option[i]; i++) {
        console.log(element.previousElementSibling.innerHTML);
        selectedAnswerArray[i] = element.previousElementSibling.innerHTML;
    }

    if (selectedAnswerArray.length == 0 && node[0].getAttribute('type') != "paragraph") {
        statusMsg("Please select any answer", "error");
        return;
    } else if (node[0].getAttribute('type') == "paragraph") {
        if (!isString(node[0].querySelector('.paragraph').value)) {
            statusMsg("Answer cannot be empty", "error");
            return;
        }
        selectedAnswerArray.push(node[0].querySelector('.paragraph').value);
    }
    enableOrDisable(true);

    let json = {};
    json.questionID = node[0].getAttribute('id');
    json.selectedAnswer = selectedAnswerArray;
    let response = await xhr("POST", "/api/quiz/validate", json);

    if (response && response.success) {
        console.log(response.data);

        let ans = "| ";
        if (isString(response.data.status) && response.data.type != "paragraph") {
            for (let eachAns in response.data.correctAnswer) {
                ans = ans + response.data.correctAnswer[eachAns] + ' | ';
            }
        } else {
            if (isString(response.data.status) && 'correct' == response.data.status) {
                ans += "That's a great answer, keep up your good work";
            } else {
                ans += "The expected answer doesn't match";
            }
        }

        document.querySelector('#quiz-result').classList.remove('hide', 'incorrect', 'correct');

        document.querySelectorAll('.submit-question')[0].classList.add('hide');

        node[0].setAttribute('status', response.data.status);

        if ('correct' == response.data.status) {
            document.querySelector('#quiz-result').querySelector('span').innerHTML = 'Correct answer: ' + ans;
            document.querySelector('#quiz-result').classList.add("correct");
        } else {
            document.querySelector('#quiz-result').querySelector('span').innerHTML = 'Correct answer: ' + ans;
            document.querySelector('#quiz-result').classList.add("incorrect");
        }

        if (document.getElementById('single-question').lastElementChild.classList.contains('active')) {
            setTimeout(function name(params) {
                quizSummary();
            }, 500);
        } else
            document.getElementById('quiz-next').classList.remove('hide');

    } //else statusMsg(capitalize(response.errorMessage), "error");

}

enableOrDisable = function (whatToDO) {

    let node = document.querySelectorAll('.question.active')[0];
    if ('paragraph' != node.getAttribute('type')) {
        node.querySelectorAll('.options').forEach(function (node) {
            node.setAttribute('disabled', whatToDO);
        });
    } else {
        node.querySelector('textarea').setAttribute('disabled', whatToDO);
    }

};

quizNext = function next() {


    var qElems = document.querySelectorAll('#single-question>li');
    let nextButton = document.getElementById('quiz-next');

    let nextNode;
    for (var i = 0; i < qElems.length; i++) {
        let node = qElems[i];
        if (node.classList.contains('active')) {
            if (node.nextElementSibling) {
                nextNode = node.nextElementSibling;
                node.classList.remove('active');
            }
            break;
        }
    }
    if (nextNode) {

        document.querySelectorAll('.submit-question')[0].classList.remove('hide');

        document.getElementById('quiz-result').classList.add('hide');


        nextNode.classList.add('active');
        nextButton.classList.add('hide');
        // var index = Array.prototype.indexOf.call(nextNode.parentNode.children, nextNode);
        // document.getElementById('currentLessonCount').innerText = index + 1;
        // manageLessonStatus();

        // document.getElementById('lesson-prev').classList.remove('hide');
    }

    if (!nextNode || !nextNode.nextElementSibling) {
        nextButton.classList.add('hide');
    }
};

function selectUnselect(node) {

    // node.classList.toggle('checked');

    let type = node.parentNode.parentNode.parentElement.getAttribute('type');

    if (type.trim().toLowerCase() != "multiselect") {
        document.querySelectorAll('.checked').forEach(e => e.classList.remove('checked'));
        node.classList.add('checked');
    } else {
        if (node.classList.contains('checked'))
            node.classList.remove('checked');
        else node.classList.add('checked');
    }

}

async function quizSummary() {

    let json = {};

    let courseID = document.getElementById('single-question').getAttribute('c_id');
    if (courseID)
        json.courseID = courseID;
    else
        json.courseID = window.location.pathname.split('/')[2];

    json.totalQuestion = document.querySelectorAll('li.question').length;

    json.correctAnswer = document.querySelectorAll('[status="correct"]').length;

    let response = await xhr("POST", "/api/quiz/status", json);

    if (response && response.success) {

        document.getElementById('quiz-container').classList.add('hide');

        let node = document.getElementById('summary');

        node.classList.remove('hide');

        node.classList.add(response.data.quizStatus.status);

        if ('pass' == response.data.quizStatus.status)
            node.querySelector('p.message').innerHTML = `Congratulation, you have passed with ${response.data.resultPercentage}% and successfully completed the course`;
        else
            node.querySelector('p.message').innerHTML = `We are sorry you have got ${response.data.resultPercentage}%, but expected the pass percentage is ${response.data.passPercentage}%`;

        // node.querySelector('span.status').innerHTML = response.data.quizStatus.status;
    }
}

checkQuizStatus = async (courseID) => {

    let courseCompleted = await xhr("GET", "/api/quiz/status?courseID=" + courseID);

    if (courseCompleted.success && courseCompleted.data.status == "completed") {
        return true;
    }
    return false;
}

document.getElementById('go_to_lesson').addEventListener('click', async () => {

    getLessonByCourseID(window.location.pathname.split('/')[2], "gotolesson");
});