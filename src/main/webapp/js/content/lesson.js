generateLessonView = async (lessons, courseID, fromWhere) => {

    if (isObject(lessons)) {

        document.getElementById('currentLessonCount').innerText = 1;
        document.getElementById('totalLessonCount').innerText = "/" + lessons.length;

        document.querySelectorAll('.lesson-main').forEach(e => e.remove());

        document.getElementById('lesson_ul').setAttribute('c_id', courseID);

        var lastCompleted = 0;

        for (var ind = 0; ind < lessons.length; ind++) {

            let content_div = "",
                video_div = "",
                img_div = "",
                li = "",
                p_tag = "",
                expand_tag = "",
                img_tag = "",
                video_tag = "",
                source_tag = "";

            li = document.createElement("li"),
                li.setAttribute('class', 'lesson-main');
            li.setAttribute('id', lessons[ind].key);
            li.setAttribute('name', lessons[ind].lessonName);

            if (lessons[ind].lessonStatus == "completed") {
                li.setAttribute('status', lessons[ind].lessonStatus);
                lastCompleted++;
            }

           let count = 0;

           
            if (lessons[ind].lessonImage && lessons[ind].lessonImage.length > 0) {


                img_div = document.createElement("A");
                img_div.setAttribute('class', 'image glightbox');

                if(isString(lessons[ind].lessonText.value))
                    img_div.setAttribute('data-glightbox', 'descPosition: right; description: .gparagraph-'+lessons[ind].key);

                img_div.setAttribute('data-width', '900px');
    
                img_tag = document.createElement('IMG');

                img_div.setAttribute('href', lessons[ind].lessonImage[0]);

                img_tag.setAttribute('src', lessons[ind].lessonImage[0]);

                img_div.appendChild(img_tag);

                // img_div.appendChild(expand_tag);

                li.appendChild(img_div);
            }

            if (lessons[ind].lessonVideo && lessons[ind].lessonVideo.length > 0) {


                video_div = document.createElement("A");
                video_div.setAttribute('class', 'video glightbox');
                video_div.setAttribute('data-width', '900px');

                video_div.setAttribute('href', lessons[ind].lessonVideo[0]);

                video_tag = document.createElement('VIDEO');
                video_tag.controls = true;
                video_tag.setAttribute('width', '521');
                video_tag.setAttribute('height', '309');
                video_tag.setAttribute('controlslist', 'nodownload');

                source_tag = document.createElement('SOURCE');
                source_tag.setAttribute('src', lessons[ind].lessonVideo[0]);
                source_tag.setAttribute('type', "video/mp4");

                video_tag.appendChild(source_tag);

                video_div.appendChild(video_tag);

                // video_div.appendChild(expand_tag);

                li.appendChild(video_div);

            }

            if (isString(lessons[ind].lessonText.value)) {

                content_div = document.createElement("DIV");

                if(lessons[ind].lessonImage && lessons[ind].lessonImage.length > 0 && lessons[ind].lessonVideo && lessons[ind].lessonVideo.length > 0){
                    content_div.setAttribute('class', 'text text-content gparagraph gparagraph-'+lessons[ind].key);
                }
                else content_div.setAttribute('class', 'text gparagraph gparagraph-'+lessons[ind].key);

                
                content_div.setAttribute('data-type', 'content');

                p_tag = document.createElement('P');
                p_tag.innerHTML = lessons[ind].lessonText.value;
                content_div.appendChild(p_tag);

                li.appendChild(content_div);
            }

            document.getElementById('lesson_ul').appendChild(li);

        }

        if (lastCompleted != lessons.length) { // this will be called when the lesson view need to be rendered without completion of quiz and it is not a last lesson
            document.querySelectorAll('.lesson-main')[lastCompleted].classList.add('active');
            manageLessonStatus();
            document.getElementById('currentLessonCount').innerText = lastCompleted + 1;

            if (lastCompleted == 0) {
                document.getElementById('lesson-prev').classList.add('hide');
            }

            handleDivPositions('lesson_ul');

            if (document.querySelectorAll('#lesson_ul>li.active')[0].children.length == 2 ) {

                if(document.querySelectorAll('#lesson_ul>li.active>.image').length != 0 && document.querySelectorAll('#lesson_ul>li.active>.text').length !=0 ){
                    document.querySelectorAll('#lesson_ul>li.active>.image')[0].classList.add('img-text');
                }
            }

        } else {

            var success = await checkQuizStatus (courseID);

            if (!success && !fromWhere) { // this will go inside to this condition when user completes all lesson and quiz is not taken

               renderPage("quiz", document.getElementById('lesson_ul').getAttribute('c_id'));

            } else { // user has complete the quiz so making the first one as default and selecting it

                document.getElementById('view-page').classList.remove(document.getElementById('view-page').classList[0]);
                document.getElementById('view-page').classList.add("lesson-content");

                lastCompleted = 0;
                document.querySelectorAll('.lesson-main')[0].classList.add('active');
                manageLessonStatus();
                document.getElementById('currentLessonCount').innerText = lastCompleted + 1;

                if(lastCompleted == 0) {
                    document.getElementById('lesson-prev').classList.add('hide');
                }

            }

        }

        document.getElementById('lesson_name').innerText = document.querySelector('#lesson_ul>.active').getAttribute('name');

        document.getElementsByClassName('mark-complete')[0].classList.remove('hide');

        document.querySelectorAll('.glightbox').forEach(function (node) {
            node.addEventListener('click', function (e) {
                e.preventDefault();
                handle_lightbox(e.target);
            });
        });


        document.querySelectorAll('.gparagraph').forEach(function (node) {
            node.addEventListener('click', function (e) {
                 let content = node.querySelectorAll('p')[0].innerHTML;

                handle_lightbox_for_para(node, content);

            });

        });


    }

}


markAsComplete = async (courseID, lessonID, contactID) => {

    if (isString(lessonID) && isString(courseID) && isString(contactID)) {

        var payload = {
            lessonID: lessonID,
            courseID: courseID,
            contactID: contactID
        }

        let response = await xhr("POST", "/api/course/status", payload);

        if (response.success) {

            if (isObject(response.data.courseStatus)) {

                document.getElementById('complete').setAttribute('disabled', true);
                document.getElementById('complete').setAttribute('checked', true);
                document.getElementById('complete_label').innerText = "Completed";
                document.getElementById(response.data.courseStatus.lessonID).setAttribute('status', 'completed');
                manageLessonStatus();

            }

        }


    } else {

        console.log('Invalid parameters for marking course as completed');
    }

}

manageLessonStatus = async () => {

    var status = document.querySelectorAll("#lesson_ul > li.active")[0].getAttribute('status');

    let complete = document.getElementById('complete');
    let complete_label = document.getElementById('complete_label');

    if (isString(status) && status == "completed") {
        complete_label.innerText = "Completed";
        complete.style.display = "none";
        complete.classList.add('checked');
        complete.setAttribute('checked', true);
        complete.setAttribute('disabled', true);

        document.getElementById('currentLessonCount').innerText

        if (document.getElementById('lesson_ul').lastChild.classList.contains('active')) {
            var success = await checkQuizStatus (window.location.pathname.split('/')[2]);

            if (!success) {
                document.getElementById('take-quiz').classList.remove('hide');
            }

            document.getElementById('lesson-next').classList.add('hide');
        } else {
            document.getElementById('lesson-next').classList.remove('hide');
            document.getElementById('take-quiz').classList.add('hide');
        }
    } else {
        complete_label.innerText = "Mark as completed";
        complete.style.display = "block";
        complete.removeAttribute('checked');
        complete.removeAttribute('disabled');
        complete.classList.remove('checked');
        document.getElementById('lesson-next').classList.add('hide');
        document.getElementById('lesson-prev').classList.remove('hide');
        if (document.getElementById('take-quiz'))
            document.getElementById('take-quiz').classList.add('hide');
    }

    handleDivPositions('lesson_ul');

    if (document.querySelectorAll('#lesson_ul>li.active')[0].children.length == 2 ) {

        if(document.querySelectorAll('#lesson_ul>li.active>.image').length != 0 && document.querySelectorAll('#lesson_ul>li.active>.text').length !=0 ){
            document.querySelectorAll('#lesson_ul>li.active>.image')[0].classList.add('img-text');
        }
    }

}

document.getElementById('complete').addEventListener('click', () => {

    let courseID = document.getElementById('lesson_ul').getAttribute('c_id'),
        lessonID = document.querySelectorAll('#lesson_ul > li.active')[0].getAttribute('id'),
        contactID = window.contactID;

    markAsComplete(courseID, lessonID, contactID);


});

if (document.getElementById('lesson-next')) {
    document.getElementById('lesson-next').addEventListener('click', () => {
        moveNext();
    });
}

if (document.getElementById('lesson-prev')) {
    document.getElementById('lesson-prev').addEventListener('click', () => {
        movePrev();
    });
}


movePrev = function () {

    var qElems = document.querySelectorAll('#lesson_ul>li');
    let prevButton = document.getElementById('lesson-prev');
    let prevNode;
    for (var i = 0; i < qElems.length; i++) {
        let node = qElems[i];
        if (node.classList.contains('active')) {

            if (node.previousElementSibling) {
                prevNode = node.previousElementSibling;
                node.classList.remove('active');
            }

            break;
        }
    }
    if (prevNode) {
        prevNode.classList.add('active');
        prevButton.classList.remove('hide');
        var index = Array.prototype.indexOf.call(prevNode.parentNode.children, prevNode);
        document.getElementById('currentLessonCount').innerText = Math.abs(index + 1);
        document.getElementById('take-quiz').classList.add('hide');
        manageLessonStatus();

    }


    //    if (!prevNode || !prevNode.prevElementSibling || !prevNode.nextElementSibling) {
    //
    //        prevButton.classList.add('hide');
    //    }
    //    else{
    //        prevButton.classList.remove('hide');
    //    }

    document.getElementById('lesson_name').innerText = document.querySelector('#lesson_ul>.active').getAttribute('name');

};

moveNext = async () => {

    var qElems = document.querySelectorAll('#lesson_ul>li');
    let nextButton = document.getElementById('lesson-next');

    let nextNode;
    for (var i = 0; i < qElems.length; i++) {
        let node = qElems[i];
        if (node.classList.contains('active')) {
            if (node.nextElementSibling) {
                nextNode = node.nextElementSibling;
                node.classList.remove('active');
            }
            break;
        }
    }
    if (nextNode) {
        nextNode.classList.add('active');
        nextButton.classList.remove('hide');
        var index = Array.prototype.indexOf.call(nextNode.parentNode.children, nextNode);
        document.getElementById('currentLessonCount').innerText = index + 1;
        manageLessonStatus();

        document.getElementById('lesson-prev').classList.remove('hide');
    }

    if (!nextNode || !nextNode.nextElementSibling) {

        if (document.querySelectorAll("#lesson_ul > li.active")[0].getAttribute('status')) {

            var success = await checkQuizStatus(document.getElementById('lesson_ul').getAttribute('c_id'));

            if (!success) {
                document.getElementById('take-quiz').classList.remove('hide');
            }

        }

        nextButton.classList.add('hide');
    }

    document.getElementById('lesson_name').innerText = document.querySelector('#lesson_ul>.active').getAttribute('name');



};

document.getElementById('take-quiz').addEventListener('click', async () => {

    var success = await checkQuizStatus (window.location.pathname.split('/')[2]);

    if (document.getElementById('complete').classList.contains('checked') && !success)
        renderPage("quiz", document.getElementById('lesson_ul').getAttribute('c_id'));
});

handle_lightbox = (node) => {

    const lightbox = GLightbox({
        selector: '.glightbox'
    });
    //    lightbox.open(node);

};

handle_lightbox_for_para = (node, content) => {

    const glightbox = GLightbox({});
    glightbox.setElements([{
        content: content
    }]);
    glightbox.open(node);

};




