getAllCourses = async () => {

    let response = await xhr("GET", "/api/course/all");

    if ('admin' == window.user.role) { // we need to remove true
        document.querySelector('.create-course').classList.remove('hide');
        document.querySelector('.admin-menu').style.display = "inline-block";
    }

    if (response.success) {

        var course_count = 0;

        if (isObject(response.data.course)) {

            for (let ind = 0; ind < response.data.course.length; ind++) {

                let course_div = "",
                    img_tag = "",
                    div_tag = "",
                    edit_button = "",
                    enroll_button = "",
                    h4_tag = "",
                    span_tag = "";

                course_div = document.createElement("DIV"),
                course_div.setAttribute('class', 'course course-pos');
                course_div.setAttribute('id', response.data.course[ind].key);
                course_div.setAttribute('name', response.data.course[ind].key);

                img_tag = document.createElement('IMG');
                if (isString(response.data.course[ind].courseImage)) {
                    img_tag.setAttribute('src', response.data.course[ind].courseImage);
                } else {
                    img_tag.setAttribute('src', 'https://storage.googleapis.com/gajendran-mohan.appspot.com/test/course-dummy.jpg');
                }

                course_div.appendChild(img_tag);

                div_tag = document.createElement('DIV'),

                h4_tag = document.createElement('H4');
                h4_tag.setAttribute('class', "course-name");
                h4_tag.innerText = response.data.course[ind].courseName;

                div_tag.appendChild(h4_tag);

                span_tag = document.createElement('SPAN');
                span_tag.setAttribute('class', "lesson-count");


                div_tag.appendChild(span_tag);

                let existenceResposne;

                if (window.user.role == "admin") { //
                    course_count++;
                    edit_button = document.createElement('span');
                    edit_button.setAttribute('id', response.data.course[ind].key);
                    edit_button.setAttribute('class', 'course-edit');

                    div_tag.appendChild(edit_button);

                } else if (window.user.role == "user") {

                    existenceResposne = await xhr("GET", "/api/user/course?courseID=" + response.data.course[ind].key + '&contactID=' + window.user.userID);

                    if (response.data.course[ind].status != "DRAFTED") {
                        course_count++;
                        if (!existenceResposne.success) {

                            enroll_button = document.createElement('span');
                            enroll_button.innerText = "Enroll";
                            enroll_button.setAttribute('class', 'enrol');
                            div_tag.appendChild(enroll_button);
                        }
                    }

                }

                course_div.appendChild(div_tag);


                if (window.user.role == "admin") { // || window.user.role == "admin"

                    document.getElementsByClassName('assigned-course')[0].appendChild(course_div);

                    course_div.addEventListener('click', (e) => {
                        getLesson();

                    });

                } else {

                    if (response.data.course[ind].status != "DRAFTED" && existenceResposne.success) {

                        document.getElementsByClassName('assigned-course')[0].appendChild(course_div);

                        course_div.addEventListener('click', (e) => {
                            getLesson();

                        });

                    } else if (response.data.course[ind].status != "DRAFTED" && !existenceResposne.success) {

                        document.getElementsByClassName('unassigned-courses')[0].classList.remove('hide');
                        document.getElementsByClassName('unassigned-courses')[0].appendChild(course_div);

                    }

                }

            }

            response.data.course.forEach( async(e)=> {

                if(document.getElementsByName(e.key)){

                    let resp = await xhr("GET", "/api/course/lesson?courseID="+e.key);
                    var count=0;
                    if (resp.success) {

                        if (isObject(resp.data.lesson)) {
                            count = resp.data.lesson.length;
                        }
                    }
                    document.getElementById(e.key).querySelector('div>.lesson-count').innerText = count + " Lessons";
                }

            })


            document.getElementById('course_count').innerText = course_count + " courses";

            if (window.user.role == "admin") {
                document.querySelectorAll('.course-edit').forEach(function (node) {
                    node.addEventListener('click', function (e) {
                        e.stopPropagation();
                        editCourse(e.target);
                    });
                });
            } else {
                // enrol button
                document.querySelectorAll('.enrol').forEach(function (node) {
                    node.addEventListener('click', function (e) {
                        e.stopPropagation();

                        e.target.innerText = '';
                        e.target.classList.add('btn-loader');
                        enrollCourse(e.target, e.target.parentElement.parentElement.getAttribute('id'));
                    });
                });
            }

        }

    } else {
        statusMsg(capitalize(response.errorMessage), "error");
    }

};





getLesson = () => {

    let courseID = '';

    if (event.target.nodeName == 'DIV') {
        if (event.target.classList.contains('course')) {
            courseID = event.target.attributes['id'].value;
        } else {
            courseID = event.target.parentNode.attributes['id'].value;
        }
    } else if (event.target.nodeName == 'IMG') {
        courseID = event.target.parentNode.attributes['id'].value;
    } else if (event.target.nodeName == 'H4') {
        courseID = event.target.parentNode.parentNode.attributes['id'].value;
    }

    historyPush("Course", '/course/' + courseID);
    renderPage('lesson', '/course/' + courseID);
    //getLessonByCourseID(courseID);
};




getLessonByCourseID = async (courseID, fromWhere) => {

    if (isString(courseID)) {

        let response = await xhr("GET", "/api/course/lesson?courseID=" + courseID);

        if (response.success) {

            if (isObject(response.data.lesson)) {

                generateLessonView(response.data.lesson, courseID, fromWhere);
            }

        }
    } else {
        console.log("Invalid param");
        statusMsg(capitalize("Something went wrong"), "error");
    }

};

enrollCourse = async (node, courseID) => {

    if (isString(courseID)) {

        var payload = {
            courseID: courseID,
            contactID: window.user.userID
        };

        var resp = await xhr('POST', '/api/user/enroll', payload);

        if (resp.success) {
            if (resp.data && resp.data.subscription && resp.data.subscription.status) {
                if ('pending' == resp.data.subscription.status) {
                    statusMsg('You have requested for the course, wait until the admin approves', 'success');
                }
            }
            node.classList.remove('btn-loader');
            node.innerText = 'Pending...';

            var subscription = resp.data.subscription;
        }

    }

}