renderPage = (page, pathMap) => {


  if ('/admin/lesson' == pathMap)
    page = 'lesson-creation';
  else if (pathMap == '/admin/admin-approval') {

  } else if (pathMap == '/admin/admin-dashboard') {

  } else document.getElementById('view-page').setAttribute('class', page + "-content");

  document.getElementById('home').classList.remove('active');

  if (window.user && window.user.role && 'admin' == window.user.role) {
    document.getElementById('dashboard-tab').classList.remove('hide');
    document.getElementById('approval-tab').classList.remove('hide');
  } else {
    document.getElementById('dashboard-tab').classList.add('hide');
    document.getElementById('approval-tab').classList.add('hide');
  }


  if ("course" == page) {
    document.getElementById('home').classList.add('active');
    document.querySelectorAll('.course').forEach(e => e.remove());
    getAllCourses();
  } else if ("lesson" == page) {
    if (pathMap) {
      let pathArray = pathMap.split('/');
      if ("course" == pathArray[1]) {
        if (pathArray[2])
          getLessonByCourseID(pathArray[2], "");
      }
    }
  } else if ("quiz" == page) {
    if (pathMap)
      getQuizForACourse(pathMap);
    else
      getQuizForACourse(window.location.pathname.split('/')[2]);

  } else if ("lesson-creation" == page) {
    // window.location.href = '/admin/lesson-creation';
    const urlParams = new URLSearchParams(window.location.search);
    let courseID = urlParams.get('courseID');

    if (!courseID)
      courseID = pathMap;

    historyPush("Lesson", '/admin/lesson?courseID=' + courseID);

    handle_admin_lesson_view(courseID);
    //    construct_admin_new_lesson_view();
  } else if ('subscription' == page) {

    if (window.user && window.user.role && 'admin' == window.user.role) {
      document.getElementById('view-page').setAttribute('class', 'admin-approval');



      document.getElementById('data-reports').setAttribute('page', page);
      if (pathMap == '/admin/admin-approval') {
        document.getElementById('dashboard-tab').classList.remove('active');

        document.getElementById('approval-tab').classList.add('active');
        document.getElementById('user-requests').classList.add('request-active');
        document.getElementById('subscription').classList.remove('hide');
        listSubscription();
      } else if (pathMap == '/admin/admin-dashboard') {

        document.getElementById('dashboard-tab').classList.add('active');
        document.getElementById('approval-tab').classList.remove('active');
        document.getElementById('stats-dashboard').classList.add('stats-active');
        document.getElementById('quiz-stats').classList.remove('hide');
        generateGraph();
        listCourseStatus();
      }
    }
  } else if ("reset-password" == page) {

  }
};

if (document.getElementById('contactID')) {
  let contactID = document.getElementById('contactID').innerHTML;
  if (contactID)
    window.contactID = contactID;
}

if (document.getElementById('user')) {
  let user = document.getElementById('user').innerHTML;
  if (user) {
    window.user = JSON.parse(user);
  }
}

if (document.getElementById('page')) {
  let page = document.getElementById('page').innerHTML;
  if (page) {
    window.page = page;
    renderPage(page, window.location.pathname);
  }
}





if ('user' != window.user.role) {
  let node = document.getElementById('admin-header');
  node.style.display = 'inline-block';
}

if (document.getElementsByClassName('profile')) {
  document.getElementsByClassName('profile')[0].querySelector('.profile-name').innerHTML = window.user.name;
  document.getElementsByClassName('profile')[0].addEventListener('click', function (node) {
    document.getElementsByClassName('profile')[0].classList.add('open');
  });

  document.getElementsByClassName('profile')[0].addEventListener('blur', function (node) {
    document.getElementsByClassName('profile')[0].classList.remove('open');
  });

  document.getElementsByClassName('profile-dd')[0].addEventListener('mouseleave', function (node) {
    document.getElementsByClassName('profile')[0].classList.remove('open');
  });
}

document.getElementById('profile-view').addEventListener('click', function (e) {
  document.querySelector('.profile-modal').classList.add('active');
  document.querySelector('.backdrop').classList.add('active');

  window.scrollTo({
    top: 20,
    behavior: 'smooth'
  });

  document.getElementById('update-password').classList.remove('hide');
  document.getElementById('pwd-msg').classList.add('hide');

  if (window.user.name)
    document.querySelector('.profile-modal').querySelector('.course-name').value = window.user.name;
  else
    statusMsg('Something went wrong', 'error');
});


document.getElementById('profile-cancel').addEventListener('click', function (e) {
  document.querySelector('.profile-modal').classList.remove('active');
  document.querySelector('.backdrop').classList.remove('active');
  document.querySelector('.profile-modal').querySelector('.course-name').value = '';
});

document.getElementById('update-password').addEventListener('click', async function (e) {

  let json = {};
  json.contactID = window.contactID;

  let response = await xhr('POST', '/api/updatepassword/mail', json);
  if (response && response.success && response.data && response.data.user) {}
  document.getElementById('update-password').classList.add('hide');
  document.getElementById('pwd-msg').classList.remove('hide');
});



document.getElementById('profile-save').addEventListener('click', async function (e) {
  document.querySelector('.profile-modal').classList.remove('active');
  document.querySelector('.backdrop').classList.remove('active');

  let name = document.querySelector('.profile-modal').querySelector('.course-name').value;
  if (!name) {
    statusMsg('Name cannot be empty', 'error');
    return;
  }

  let json = {};
  json.name = name;
  let response = await xhr('PUT', '/api/user/name', json);
  if (response && response.success && response.data && response.data.user) {
    window.user.name = response.data.user.name;
    document.getElementsByClassName('profile-name')[0].innerHTML = response.data.user.name;
  }

});