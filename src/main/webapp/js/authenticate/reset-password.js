async function resetPassword(password, confirmpassword, contactID) {

 var json = {};
 json.password = password;
 json.confirmpassword = confirmpassword;
 json.contactID = contactID;


 let response = await xhr("PUT", "/api/user/password", json);

 if (response && response.success && response.data) {
  statusMsg('Password updated successfully', "success");
  window.location.href='/login';
 } else statusMsg(capitalize(response.errorMessage), "error");
}

const urlParams = new URLSearchParams(window.location.search);

let contactID = urlParams.get('contactID');

// window.InitiateResetPassword.contactID = contactID;

if (document.getElementById("submit-password")) {
 document.getElementById("submit-password").addEventListener("click", function () {

  var password = document.getElementById("new-password").value;
  var confirmpassword = document.getElementById("confirm-password").value;

  if (password.trim() != confirmpassword.trim()) {
   statusMsg("Password and confirm password doesn't match", "error");
   return;
  }

  resetPassword(password, confirmpassword, contactID);

 });
}