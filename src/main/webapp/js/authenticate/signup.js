register_user = async (emailID, name, phoneNumber, courseID) => {

    if (!isString(name) || name.length <= 4) {
        statusMsg(capitalize("Name should be greater than 4 characters"), "error");
        document.getElementById("btn_primary").removeAttribute('disabled', false);
        return;
    }

    if (!isEmailID(emailID)) {
        statusMsg(capitalize("Invalid email id"), "error");
        document.getElementById("btn_primary").removeAttribute('disabled', false);
        return;
    }

    if (!isValidPhoneNumber(phoneNumber)) {
        statusMsg(capitalize("Invalid phone number"), "error");
        document.getElementById("btn_primary").removeAttribute('disabled', false);
        return;
    }

    if (!courseID) {
        statusMsg("Please select any course", "error");
        document.getElementById("btn_primary").removeAttribute('disabled', false);
        return;
    }

    var payload = {
        emailID: emailID,
        name: name,
        courseID: courseID,
        phoneNumber: phoneNumber
    }

    let response = await xhr("POST", "/api/user", payload);

    if (response.success) {
        document.getElementsByClassName('signup')[0].classList.add('hide');

        document.getElementsByClassName('success-msg')[0].classList.remove('hide');

        document.getElementsByName('name')[0].value = "";
        document.getElementsByName('email')[0].value = "";
        document.getElementsByName('phone_no')[0].value = "";
    } else {
        statusMsg(capitalize(response.errorMessage), "error");
    }
    document.getElementById("btn_primary").removeAttribute('disabled', false);
}


document.getElementById("btn_primary").addEventListener("click", () => {

    document.getElementById("btn_primary").setAttribute('disabled', true);

    let name = document.getElementsByName('name')[0].value.trim(),
        email = document.getElementsByName('email')[0].value.trim(),
        courseID = (document.getElementById('course-list').querySelector('option[selected=selected]')) ? document.getElementById('course-list').querySelector('option[selected=selected]').getAttribute('id') : '';
    phoneNumber = document.getElementsByName('phone_no')[0].value.trim();

    register_user(email, name, phoneNumber, courseID);

//    document.getElementById("btn_primary").removeAttribute('disabled', false);
});

function removeAllSelectedOptions() {
    document.getElementById('course-list').querySelectorAll('option').forEach(function (node) {
        node.removeAttribute('selected');
    });
}

(function () {
    getAllCourse();
    document.getElementById('course-list').addEventListener('change', function (e) {
        removeAllSelectedOptions();
        document.getElementById(e.target.value).setAttribute('selected', 'selected');
    });
})();

async function getAllCourse() {
    let response = await xhr("GET", "/api/course/all");

    if (response && response.success && response.data && response.data.course) {

        let node = document.getElementById('course-list-option');

        let courses = response.data.course;
        for (let eachCourse in courses) {
            let templateNode = node.content.cloneNode(true);
            templateNode.firstElementChild.value = courses[eachCourse].key;
            templateNode.firstElementChild.innerText = courses[eachCourse].courseName;
            templateNode.firstElementChild.setAttribute('id', courses[eachCourse].key);
            if (templateNode) {
                document.getElementById('course-list').appendChild(templateNode);
            }
        }

    }
}