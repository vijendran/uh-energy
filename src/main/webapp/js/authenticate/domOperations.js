
 var dom = function (query) {
		this.cache = this.cache || {};

		if (!this.cache[query]) {
				this.cache[query] = document.querySelectorAll(query);
		}

		return this.cache[query][0];
};

var DOMOperations = (function () {
	var domOperations = {

		addEventListener: function (element, event, callback) {
			if (window.addEventListener) {
				element.addEventListener(event, callback, false);
			} else if (document.attachEvent) {
				element.attachEvent('on' + event, callback);
			} else {
				element['on' + event] = callback;
			}
		},
		removeEventListener: function (element, event) {
			if (window.removeEventListener) {
				element.removeEventListener(event);
			} else if (document.attachEvent) {
				element.detachEvent('on' + event);
			} else {
				element['on' + event] = null;
			}
		}
	};
	return domOperations;
})();