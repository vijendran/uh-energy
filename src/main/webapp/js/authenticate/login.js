if (document.getElementById("login")) {
  document.getElementById("login").addEventListener("click", function () {

    var emailID = document.getElementById("email").value;
    var password = document.getElementById("password").value;

    if (isEmailID(emailID) && validatePassword(password)) {
      login(emailID, password);
    } else {
      // status message - should be called
      statusMsg("Invalid email or password", "error");

    }
  });
}

function validateEmail(email) { 

  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email) {
    return re.test(String(email).toLowerCase());
  }
  return false;
}

function validatePassword(password) {

  if (password.trim())
    return true;

  return false;
}

async function login(emailID, password) {

  var json = {};
  json.emailID = emailID;
  json.password = password;

  let response = await xhr("POST", "/api/authenticate", json);

  if (response && response.success && response.data.redirectURI) {
      window.location.href = response.data.redirectURI;
  }
  else statusMsg(capitalize(response.errorMessage), "error");
}