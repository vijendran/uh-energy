(() => {
 if (document.getElementById('upload')) {
  document.getElementById('upload').addEventListener('click', function (e) {

   uploadFile(document.getElementById('upload'), e);
  });
 }

})();

function uploadFile(origin, type, attachmentType, attachments) {

 var ajax = req => {
  var fetchData = async () => {
   var headers = new Headers();
   if (req.contentType) {
    headers['Content-Type'] = req.contentType;
   }

   var initData = {
    method: req.method,
    body: req.data,
    headers: headers
   };
   try {
    var resp = (await fetch(new Request(req.url, initData))).json();
    resp.then(fetchJSON => {
     console.log(fetchJSON);
    });
    return resp;
   } catch (e) {
    return {};
   }
  };
  return fetchData();
 }



 attachments = {};

 attachments.node = document.createElement("input");
 attachments.node.type = "file";
 attachments.node.name = "files";

 if(attachmentType == "image") {
    attachments.node.accept = "image/png, image/jpeg";
 } else if(attachmentType == "video") {
    attachments.node.accept="video/mp4,video/x-m4v,video/*"
 }

 attachments.node.style.display = "none";
 attachments.node.setAttribute("class", "file");
 attachments.node.setAttribute("multiple", false);

 attachments.node.onchange = function (file, data) {

    if(type == 'lesson-image') {

        if( document.querySelectorAll('#lesson_main_ul>li.active').length == 0 ) {

            var template = document.querySelector('#basic_li'),
                clonedParent = template.content.cloneNode(true);

            clonedParent.firstElementChild.classList.add("class", "active");

            document.getElementById('lesson_main_ul').appendChild(clonedParent);

        }

        var childTemplate = document.querySelector('#lesson_image_template'),
            clonedChild = childTemplate.content.cloneNode(true);

//        document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);

        if( document.querySelectorAll('#video_creation_div').length == 0 && document.querySelectorAll('#lesson_text_div').length == 0) {
            document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);
        } else {
            // video or text is present so we need to append it before the video/text
            document.querySelectorAll('#lesson_main_ul>li.active')[0].insertBefore(clonedChild, document.querySelectorAll('#lesson_main_ul>li.active')[0].childNodes[0]);

            if(document.querySelectorAll('#lesson_text_div').length !=0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2){
                document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
            }
        }
        handleDivPositions('lesson_main_ul');
        document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('uploading');

        origin = document.querySelectorAll('#lesson_main_ul>li.active>.image')[0];

        document.querySelector('#lesson_main_ul').querySelectorAll('.delete-item').forEach( function (node) { node.addEventListener('click', (e)=>{
                 e.target.parentElement.remove();
                 handleDivPositions('lesson_main_ul');

                if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

                  if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                      document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
                  }
                }

            });
        });

    }else if(type == 'lesson-video') {

        if( document.querySelectorAll('#lesson_main_ul>li.active').length == 0 ) {

            var template = document.querySelector('#basic_li'),
                clonedParent = template.content.cloneNode(true);

            clonedParent.firstElementChild.classList.add( "active");

            document.getElementById('lesson_main_ul').appendChild(clonedParent);

        }

        var childTemplate = document.querySelector('#lesson_video_template'),
        clonedChild = childTemplate.content.cloneNode(true);

//        document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);
            if( document.querySelectorAll('#image_creation_div').length == 0 && document.querySelectorAll('#lesson_text_div').length == 0) {
                document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);
            } else if (document.querySelectorAll('#image_creation_div').length == 0 && document.querySelectorAll('#lesson_text_div').length != 0 ) {
            // image is not present and text is present so we need to insert before text
                document.querySelectorAll('#lesson_main_ul>li.active')[0].insertBefore(clonedChild, document.querySelectorAll('li.lesson_main')[0].childNodes[0]);
            } else if (document.querySelectorAll('#image_creation_div').length != 0 && document.querySelectorAll('#lesson_text_div').length == 0 ) {
            // image is present and text is not present so we need to insert after image
                document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);
            } else if (document.querySelectorAll('#image_creation_div').length != 0 && document.querySelectorAll('#lesson_text_div').length != 0) {
            // image is present and the text is also present then we need ot insert it in between
                document.querySelectorAll('#lesson_main_ul>li.active')[0].insertBefore(clonedChild, document.querySelectorAll('li.lesson_main')[0].childNodes[3]);
            }

        handleDivPositions('lesson_main_ul');

        document.querySelectorAll('#lesson_main_ul>li.active>.video')[0].classList.add('uploading');

        origin = document.querySelectorAll('#lesson_main_ul>li.active>.video')[0];

        document.querySelector('#lesson_main_ul').querySelectorAll('.delete-item').forEach( function (node) { node.addEventListener('click', (e)=>{
                 e.target.parentElement.remove();
                 handleDivPositions('lesson_main_ul');

                if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

                  if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                      document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
                  }
                }
            });
        });

    }

  if (file.target.files && file.target.files[0] && file.currentTarget.files[0]) {
   // build preview based on the file reader 

   // ajax after getting the file // current target
   var data = new FormData();
   data.append('files', file.currentTarget.files[0]);

   if (window.FileReader) {

    attachments.progressBarWidth = 1;

    var fileRequest = {};

    fileRequest.url = `/api/uploadUrl`;
    fileRequest.method = 'GET';

    ajax(fileRequest).then(response => {
     console.log("response", response);

     if (response) {

      var uploadRequest = {};
      uploadRequest.url = response.data.uploadURL;
      uploadRequest.method = 'POST';
      uploadRequest.data = data;
      ajax(uploadRequest).then(response => {

       console.log(response);
       if (type == 'course-image') {
             renderImagePreview(origin, response.data.url);
             console.log('detected as upload course image');
       } else if(type == 'lesson-image') {
            renderLessonUrl(origin, response.data.url, "image");
       }else if(type == 'lesson-video') {
            renderLessonUrl(origin, response.data.url, "video");
       }

      });
     }
    });
   }
  }
 }

 attachments.node.click();
}