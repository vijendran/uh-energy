document.getElementById('add_image').addEventListener('click', () => {

    if (document.querySelectorAll('#lesson_main_ul>li.active>.image').length == 0) {
        uploadFile(null, 'lesson-image', "image");
    }

});

document.getElementById('add_video').addEventListener('click', () => {

    if (document.querySelectorAll('#lesson_main_ul>li.active>.video').length == 0) {
        uploadFile(null, 'lesson-video', "video");
    }

});

document.getElementById('add_text').addEventListener('click', () => {

    if (document.querySelectorAll('#lesson_main_ul>li.active>.text').length == 0) {

        if(document.querySelectorAll('.ql-toolbar').length ==0){
            var quill = new Quill('#editor', {
                modules: {
                    toolbar: true
                },
                placeholder: 'Enter the text here...',
                theme: 'snow',

            });
        }

        document.querySelectorAll('.text-modal')[0].classList.add('active');
        document.querySelectorAll('.text-modal')[0].style.display = "block";
        document.querySelector('.backdrop').classList.add('active');
        document.getElementById('editor').querySelector('.ql-editor').innerText = ""

    }


});

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
        console.error('Async: Could not copy text: ', err);
    });
}

document.getElementById('les_name').addEventListener('keyup', () => {

    if (document.getElementById('les_name').value.trim() != "") {
        document.querySelector('#sidebar_ul>li.active').innerText = document.getElementById('les_name').value;
    } else {
        document.querySelector('#sidebar_ul>li.active').innerText = "Untitled";
    }
});

document.getElementById('delete_btn').addEventListener('click', async () => {

    if(document.querySelectorAll('#lesson_main_ul>li.active').length > 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name')){
        let resp = await xhr("DELETE", "/api/lesson/" + document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name'), '');

        if (resp.success) {
            removeActiveLI();
        }

    } else  {
        removeActiveLI();
    }

});

handle_admin_lesson_view = async (courseID) => {

    if (isString(courseID)) {

        document.getElementById('lesson_main_ul').setAttribute('courseID', courseID);
        document.getElementById('lesson-tab').classList.add('active');


        let response = await xhr("GET", "/api/course?courseID=" + courseID);

        if (response && response.success && response.data && response.data.course && response.data.course.courseName) {
            document.getElementsByClassName('course-head')[0].querySelector('h4').innerText = response.data.course.courseName;
        }

        let resp = await xhr("GET", "/api/course/lesson?courseID=" + courseID);

        if (resp.success) {

            if (isObject(resp.data.lesson)) {

                if (resp.data.lesson.length > 0) { // this is in edit mode
                    construct_admin_edit_lesson_view(resp.data.lesson);
                }
            } else {
                return false;
            }
        } else { // this is creation mode
            construct_admin_new_lesson_view();
        }
    } else {
        return false;
    }
}

construct_admin_new_lesson_view = () => {

    document.getElementById('les_name').value = "";
    document.querySelectorAll('#lesson_main_ul>li').forEach(e => e.remove());

    document.querySelectorAll('#sidebar_ul>li').forEach(e => {
        if (!e.classList.contains('add-lesson')) {
            e.remove()
        }
    });

    document.getElementById('view-page').setAttribute('class', "lesson-creation");

    showTab('lesson');

    document.querySelectorAll('#header-tabs>a').forEach(e => e.classList.remove('active'));
    document.getElementById('lesson-tab').setAttribute('class', "active");

    document.getElementById('sidebar-add').innerText = "+ Add Lesson";
    document.getElementById('sidebar-add').click();

}

construct_admin_edit_lesson_view = async (lessons) => {

    document.querySelectorAll('#lesson_main_ul>li').forEach(e => e.remove());

    document.querySelectorAll('#sidebar_ul>li').forEach(e => {
        if (!e.classList.contains('add-lesson')) {
            e.remove()
        }
    });

    showTab('lesson');

    await construct_lesson_view(lessons);

//    document.querySelectorAll('#sidebar_ul>li')[1].classList.add('active');

    document.getElementById('les_name').value = document.querySelectorAll('#sidebar_ul>li')[1].innerText;

    document.querySelectorAll('#lesson_main_ul>li')[0].classList.add('active');

    document.getElementById('view-page').classList.remove(document.getElementById('view-page').classList[0]);
    document.getElementById('view-page').classList.add('lesson-creation');

    document.querySelectorAll('#sidebar_ul>li')[1].click();

}

publishCourse = async (payload) => {

    let course = await xhr("PUT", "/api/course/status", payload);

    if (course.success) {
        statusMsg(capitalize("Course published successfully"), "success");
        window.location.href = '/course';
    }

}

document.getElementById('publish_btn').addEventListener('click', () => {

    if (document.querySelectorAll('#sidebar_ul>li.incomplete').length != 0) {
        statusMsg('Creeation is in incomplete state', "error");
        return;
    }
    var payload = {
        "courseID": document.getElementById('lesson_main_ul').getAttribute('courseID'),
        "status": "ENABLED"
    }
    publishCourse(payload);
});

document.getElementById('course-cancel').addEventListener('click', () => {

    if (document.querySelectorAll('#sidebar_ul>li.incomplete').length != 0) {
        statusMsg('Unsaved changes exists, redirecting you...', "error");
    }
    window.location.href ='/course';
});


handle_lesson_sidebar = (lessonID) => {

    if (document.getElementsByName('uploading').length == 0 ) {

        if (lessonID) {
            document.querySelectorAll('#sidebar_ul>li').forEach(e => e.classList.remove('active'));
            document.getElementById(lessonID).classList.add('active');

            document.querySelectorAll('#lesson_main_ul>li').forEach(e => e.classList.remove('active'));
            document.getElementsByName(lessonID)[0].classList.add('active');

            document.getElementById('les_name').value = document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('lesson_name');
        } else {

            if (document.querySelectorAll('#lesson_main_ul>li.active').length > 0) {
                document.querySelectorAll('#lesson_main_ul>li.active')[0].classList.remove('active');
            }
            if(document.querySelectorAll('#sidebar_ul>li.active').length>0){
                document.querySelectorAll('#sidebar_ul>li.active')[0].classList.remove('active');
            }
            document.querySelectorAll('#sidebar_ul>li.incomplete')[0].classList.add('active');
            document.querySelectorAll('#lesson_main_ul>li:last-child')[0].classList.add('active');

            if(document.querySelectorAll('#sidebar_ul>li.active')[0].innerText.trim().toLowerCase() != "" ) {
                 document.getElementById('les_name').value = document.querySelectorAll('#sidebar_ul>li.active')[0].innerText;
            } else {
                document.getElementById('les_name').value = "";
            }
        }

        handleDivPositions('lesson_main_ul');

        if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

            if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
            }
        }
    }
}

document.getElementById('lesson-tab').addEventListener('click', () => {

    showTab('lesson');
    let path = window.location.href.split('=');
    handle_admin_lesson_view(path[path.length - 1]);

});

renderLessonUrl = (origin, url, type) => {

    origin.classList.remove('uploading');

    if (type == "image") {
        origin.querySelectorAll('a')[0].setAttribute('href', url);
        origin.querySelectorAll('a>img')[0].setAttribute('src', url);
    } else {
        origin.querySelectorAll('a')[0].setAttribute('href', url);
        origin.querySelectorAll('a>video>source')[0].setAttribute('src', url);
    }

}

document.getElementById('lesson_save').addEventListener('click', async (e) => {


    if (document.getElementsByName('uploading').length == 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length !=0) {


        var img_url = '',
            img_arr = [];

        if (document.querySelectorAll('#lesson_main_ul>li.active>.image>a>img').length > 0) {
            img_url = (document.querySelectorAll('#lesson_main_ul>li.active>.image>a>img')[0].getAttribute('src')) ? document.querySelectorAll('#lesson_main_ul>li.active>.image>a>img')[0].getAttribute('src') : "";
            if (img_url != "") {
                img_arr.push(img_url);
            }
        }

        var video_url = '',
            video_arr = [];
        if (document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('.video_src').length > 0) {
//            video_url = (document.querySelectorAll('#lesson_main_ul>li.active>.video>a>source')[0].getAttribute('src')) ? document.querySelectorAll('#lesson_main_ul>li.active>.video>a>source')[0].getAttribute('src') : "";
            video_url = (document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('.video_src')[0].getAttribute('src')) ? document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('.video_src')[0].getAttribute('src') : "";
            if (video_url != "") {
                video_arr.push(video_url);
            }
        }

        var lesson_text = '';
        if (document.querySelectorAll('#lesson_main_ul>li.active>.text>div').length > 0) {
            lesson_text = document.querySelectorAll('#lesson_main_ul>li.active>.text>div')[0].innerHTML;
        }

        var lesson_name = document.getElementById('les_name').value;

        if (!isString(lesson_name)) {
            statusMsg(capitalize("Lesson name cannot be empty"), "error");
            return;
        }

        var payload = {

            lessonImage: (img_arr) ? img_arr : [],
            lessonVideo: (video_arr) ? video_arr : [],
            lessonText: lesson_text,
            lessonName: lesson_name
        }

        let resp = "";

        if (isString(document.querySelector('#sidebar_ul>li.active').getAttribute('id'))) { // update lesson

            var lessonID = '';
            if (document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name')) {
                lessonID = document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name');
            }

            if (!isString(lessonID)) {
                statusMsg(capitalize("Failed to update"), "error");
            }

            payload.lessonID = lessonID;

            resp = await xhr("PUT", "/api/lesson/", payload);

            if (resp.success) {
                statusMsg(capitalize("Successfully updated the lesson"), "success");
                document.querySelector('#sidebar_ul>li.active').classList.remove('incomplete');
            }
        } else {

            payload.courseID = document.getElementById('lesson_main_ul').getAttribute('courseid');

            resp = await xhr("POST", "/api/lesson/", payload);

            if (resp.success) {
                statusMsg(capitalize("Lesson created successfully"), "success");

                var lesson_arr = [];
                lesson_arr.push(resp.data.lesson);

                document.querySelectorAll('#lesson_main_ul>li.active')[0].remove();
                document.querySelector('#sidebar_ul>li.incomplete').remove();

                await construct_lesson_view(lesson_arr);

                document.querySelector('#sidebar_ul>li:last-child').click();
            }

        }

    } else {
        statusMsg('Upload in progress please wait', "error");
        return;

    }


});

document.getElementById('text_save').addEventListener('click', (e) => {

    if (document.getElementById('editor').querySelector('.ql-editor').innerText.trim()) {

        if (document.querySelectorAll('#lesson_main_ul>li.active').length == 0) {

            var template = document.querySelector('#basic_li'),
                clonedParent = template.content.cloneNode(true);

            clonedParent.firstElementChild.classList.add("active");

            document.getElementById('lesson_main_ul').appendChild(clonedParent);

        }

        var childTemplate = document.querySelector('#lesson_text_template'),
            clonedChild = childTemplate.content.cloneNode(true);

        if (document.querySelectorAll('#lesson_main_ul>li.active')[0]) {

            if(document.querySelectorAll('#image_creation_div').length !=0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 1){
                document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
            }

            document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);
        }


        handleDivPositions('lesson_main_ul');

        document.querySelectorAll('#lesson_main_ul>li.active>.text>.lesson_text')[0].innerHTML = document.getElementById('editor').querySelector('.ql-editor').innerHTML;

        document.querySelectorAll('.text-modal')[0].classList.remove('active');
        document.querySelectorAll('.text-modal')[0].style.display = "none";
        document.querySelector('.backdrop').classList.remove('active');

        document.querySelector('#lesson_main_ul').querySelectorAll('.delete-item').forEach( function (node) { node.addEventListener('click', (e)=>{
                 e.target.parentElement.remove();
                 handleDivPositions('lesson_main_ul');

                 if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

                     if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                         document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
                     }
                 }
            });
        });
    }

})

document.getElementById('lesson_cancel').addEventListener('click', (e) => {

    document.querySelectorAll('.text-modal')[0].classList.remove('active');
    document.querySelectorAll('.text-modal')[0].style.display = "none";
    document.querySelector('.backdrop').classList.remove('active');

});

construct_lesson_view = (lessons) => {

    for (var ind = 0; ind < lessons.length; ind++) {

        var sidebar_template = document.querySelector('#sidebar_li'),
            sidebar_clonedNode = sidebar_template.content.cloneNode(true);

        sidebar_clonedNode.firstElementChild.setAttribute("id", lessons[ind].key);
        sidebar_clonedNode.firstElementChild.setAttribute("class", 'lesson_li');
        sidebar_clonedNode.firstElementChild.setAttribute("title", lessons[ind].lessonName);
        sidebar_clonedNode.firstElementChild.innerText = lessons[ind].lessonName;

        document.querySelector('#sidebar_ul').appendChild(sidebar_clonedNode);

        var template = document.querySelector('#basic_li'),
            clonedParent = template.content.cloneNode(true);

        clonedParent.firstElementChild.setAttribute("name", lessons[ind].key);
        clonedParent.firstElementChild.setAttribute("lesson_name", lessons[ind].lessonName);
        document.getElementById('lesson_main_ul').appendChild(clonedParent);

        var childTemplate = "";

        if (lessons[ind].lessonImage && lessons[ind].lessonImage.length > 0) {

            childTemplate = document.querySelector('#lesson_image_template'),
                clonedChild = childTemplate.content.cloneNode(true);

            document.getElementsByName(lessons[ind].key)[0].appendChild(clonedChild);

            document.getElementsByName(lessons[ind].key)[0].querySelectorAll('.image>a')[0].setAttribute('href', lessons[ind].lessonImage[0]);
            document.getElementsByName(lessons[ind].key)[0].querySelector('img').setAttribute('src', lessons[ind].lessonImage[0]);

        }

        if (lessons[ind].lessonVideo && lessons[ind].lessonVideo.length > 0) {

            childTemplate = "";

            childTemplate = document.querySelector('#lesson_video_template'),
            clonedChild = childTemplate.content.cloneNode(true);

            document.getElementsByName(lessons[ind].key)[0].appendChild(clonedChild);

            document.getElementsByName(lessons[ind].key)[0].querySelector('.video').setAttribute('href', lessons[ind].lessonVideo[0]);
            document.getElementsByName(lessons[ind].key)[0].querySelector('source').setAttribute('src', lessons[ind].lessonVideo[0]);

        }

        if (isString(lessons[ind].lessonText.value)) {

            childTemplate = "";

            childTemplate = document.querySelector('#lesson_text_template'),
                clonedChild = childTemplate.content.cloneNode(true);

            document.getElementsByName(lessons[ind].key)[0].appendChild(clonedChild);

            document.getElementsByName(lessons[ind].key)[0].querySelector('.lesson_text').innerHTML = lessons[ind].lessonText.value;

        }

    }

    bindLessonSidebarEvent();

    bindLessonMainDeleteEvent();
}

handleDivPositions = (selector) => {

    if(document.querySelectorAll('#'+selector+'>li.active')[0].classList.contains('two')){
        document.querySelectorAll('#'+selector+'>li.active')[0].classList.remove('two');
    }

    if(document.querySelectorAll('#'+selector+'>li.active')[0].classList.contains('three')){
        document.querySelectorAll('#'+selector+'>li.active')[0].classList.remove('three');
    }

    if(document.querySelectorAll('#'+selector+'>li.active')[0].children.length == 2 ) {
        document.querySelectorAll('#'+selector+'>li.active')[0].classList.add('two');
    } else if ( document.querySelectorAll('#'+selector+'>li.active')[0].children.length == 3 ) {
        document.querySelectorAll('#'+selector+'>li.active')[0].classList.add('three');
    }
}

document.getElementById('lesson_admin_cancel').addEventListener('click', async( event )=> {

    if(document.querySelectorAll('#lesson_main_ul>li.active').length > 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name')){

        let resp = await xhr("GET", "/api/lesson?lessonID=" + document.querySelectorAll('#lesson_main_ul>li.active')[0].getAttribute('name'), '');

        if (resp.success) {

            document.querySelectorAll('#lesson_main_ul>li.active')[0].innerHTML = "";

            var lesson = resp.data.lesson;

            document.getElementById('les_name').value = lesson.lessonName;

            var childTemplate = "";

            if (lesson.lessonImage && lesson.lessonImage.length > 0) {

                childTemplate = document.querySelector('#lesson_image_template'),
                    clonedChild = childTemplate.content.cloneNode(true);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('.image>a')[0].setAttribute('href', lesson.lessonImage[0]);
               document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelector('img').setAttribute('src', lesson.lessonImage[0]);

            }

            if (lesson.lessonVideo && lesson.lessonVideo.length > 0) {

                childTemplate = "";

                childTemplate = document.querySelector('#lesson_video_template'),
                clonedChild = childTemplate.content.cloneNode(true);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelector('.video').setAttribute('href', lesson.lessonVideo[0]);
               document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelector('source').setAttribute('src', lesson.lessonVideo[0]);

            }

            if (isString(lesson.lessonText.value)) {

                childTemplate = "";

                childTemplate = document.querySelector('#lesson_text_template'),
                    clonedChild = childTemplate.content.cloneNode(true);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].appendChild(clonedChild);

               document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelector('.lesson_text').innerHTML = lesson.lessonText.value;

            }

            bindLessonSidebarEvent();

            bindLessonMainDeleteEvent();

            handleDivPositions('lesson_main_ul');

            if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

              if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                  document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
              }
            }

        }

    } else  {
        removeActiveLI();
    }
});

removeActiveLI = () => {


    document.getElementById('les_name').value = "";

    if (document.querySelectorAll('#lesson_main_ul>li').length != 0) {
        document.querySelectorAll('#lesson_main_ul>li.active').forEach(e => e.remove());
    }

    var prevElement = document.querySelector('#sidebar_ul>li.active').previousElementSibling,
        nextElement = document.querySelector('#sidebar_ul>li.active').nextElementSibling;

    document.querySelector('#sidebar_ul>li.active').remove();

    if (document.querySelectorAll('#sidebar_ul>li').length == 1) {
        document.getElementById('sidebar-add').click();
    } else {

        if (prevElement) {

            if (!prevElement.classList.contains('add-lesson')) {
                prevElement.click();
            } else {
                if (nextElement) {
                    nextElement.click();
                } else {
                    document.getElementById('sidebar-add').click();
                }
            }
        }
    }
}