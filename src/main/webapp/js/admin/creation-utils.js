document.getElementById('sidebar-add').addEventListener('click', () => {

    let isQuizTab = false;

    if (document.getElementById('quiz-tab').classList.contains('active')) {
        isQuizTab = true;
    }

    if (document.getElementsByClassName('incomplete').length == 0) {

        var template = document.querySelector('#sidebar_li'),
            clonedNode = template.content.cloneNode(true);

        document.querySelectorAll('#sidebar_ul>li').forEach(e => e.classList.remove('active'));
        if (isQuizTab)
            clonedNode.firstElementChild.setAttribute("class", "quiz_li active incomplete");
        else clonedNode.firstElementChild.setAttribute("class", "lesson_li active incomplete");

        document.getElementById('sidebar_ul').appendChild(clonedNode);

        if (isQuizTab) {
            buildQuestion();
            removeActiveClassForQuizBody();

            let temp = document.getElementById('quiz-view').querySelectorAll('li.each-question');
            temp[temp.length - 1].classList.add('active');

            buildOption('singleselect-create');
            removeActiveClassForSidebar();
            // hilighting the body

            document.querySelector('#sidebar_ul').querySelector('.quiz_li:last-child').classList.add('active');

            document.querySelector('#sidebar_ul').querySelector('.quiz_li:last-child').addEventListener('click', function (node) {
                if (isQuizTab) {
                    bindSidebarClickEvent(node.target);
                }
            });
            document.querySelectorAll('.question-type').forEach(function (node) {
                node.querySelector('select').addEventListener('change', function (e) {
                    selectOptions(e.target);
                });
            });

            document.querySelector('li.each-question.active').querySelectorAll('.b-input').forEach(function (node) {
                node.addEventListener('click', (e) => {
                    checkUncheck(e.target.previousElementSibling);
                });
            });
           
        } else {
            document.getElementById('les_name').value = "";
            if(document.querySelectorAll('#lesson_main_ul>li.active').length>0){
                document.querySelectorAll('#lesson_main_ul>li.active')[0].classList.remove('active');
            }
            var template = document.querySelector('#basic_li'),
                clonedParent = template.content.cloneNode(true);

            clonedParent.firstElementChild.classList.add("class", "active");

            document.getElementById('lesson_main_ul').appendChild(clonedParent);

            bindLessonSidebarEvent();
        }
    }
});

showTab = (tab) => {
    let node = document.querySelectorAll('.creation-main')[0];
    node.classList.forEach(function (className) {
        console.log(className);
        if (className.indexOf('viewport') != -1) {
            node.classList.remove(className);
        }
        node.classList.add('viewport-' + tab);
    });

    document.getElementById('header-tabs').querySelectorAll('a').forEach(function (node) {
        node.classList.remove('active');
    });
    document.getElementById(tab + '-tab').classList.add('active');


    if ('quiz' == tab)
        document.getElementById('sidebar-add').innerHTML = '+ Add Quiz';
    else if ('lesson' == tab)
        document.getElementById('sidebar-add').innerHTML = '+ Add Lesson';
};

bindLessonSidebarEvent = () => {

    document.querySelector('#sidebar_ul').querySelectorAll('.lesson_li').forEach(function (node) {
        node.addEventListener('click', (e) => {

            if (document.getElementById('lesson-tab').classList.contains('active')) {
                handle_lesson_sidebar(e.target.getAttribute('id'));
            }
        });
    });
}

bindLessonMainDeleteEvent = () => {
    document.querySelector('#lesson_main_ul').querySelectorAll('.delete-item').forEach( function (node) { node.addEventListener('click', (e)=>{
             e.target.parentElement.remove();
             handleDivPositions('lesson_main_ul');

            if (document.querySelectorAll('#lesson_main_ul>li.active')[0].children.length == 2 ) {

              if(document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#lesson_text_div').length != 0 && document.querySelectorAll('#lesson_main_ul>li.active')[0].querySelectorAll('#image_creation_div').length !=0 ){
                  document.querySelectorAll('#lesson_main_ul>li.active>.image')[0].classList.add('img-text');
              }
            }
        });
    });
}