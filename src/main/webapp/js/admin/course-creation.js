bindEventListenerForCourseCreation = (node, jsonResponse) => {

  if (jsonResponse)
    renderExistingData(node, jsonResponse);
  node.querySelector('.cancel').addEventListener('click', function () {
    node.querySelector('.course-name').value = '';
    node.querySelector('.course-image-preview').setAttribute('src', 'https://storage.googleapis.com/gajendran-mohan.appspot.com/test/course-dummy.jpg');

    node.querySelector('#percentage').value= '50';

    document.querySelector('.backdrop').classList.remove('active');

    document.getElementById('course-creation').querySelector('.creation-modal').classList.remove('active');
  });

  node.querySelector('#course-image').addEventListener('click', function () {
    uploadFile(document.getElementById('course-creation').querySelector('.creation-modal'), 'course-image', "image");
  });

  node.querySelector('#course-create').addEventListener('click', function (e) {

    // btn-loader



    let json = formCourseJOSN();

    if (!json)
      return;

    let courseID = '';
    e.target.classList.add('btn-loader');
    if (document.getElementById('course-creation').querySelector('.creation-modal').hasAttribute('courseID')) {
      json.courseID = document.getElementById('course-creation').querySelector('.creation-modal').getAttribute('courseID');
      createOrUpdateCourse('PUT', json);
    } else {
      createOrUpdateCourse('POST', json);
    }
    e.target.classList.remove('btn-loader');
  });
};

renderExistingData = async (node, jsonResponse) => {
  node.querySelector('.course-image-preview').setAttribute('src', jsonResponse.courseImage);
  node.querySelector('.course-name').value = jsonResponse.courseName;
  node.querySelector('#percentage').value = jsonResponse.percentage;
};

formCourseJOSN = () => {

  let modalNode = document.getElementById('course-creation').querySelector('.creation-modal');
  let courseCreateJSON = {};
  let courseName = modalNode.querySelector('.course-name').value;

  let quizPercentage = modalNode.querySelector('#percentage').value;

  if (!quizPercentage) {
    statusMsg('Percentage cannot be empty', 'error');
    return;
  }

  if (isNaN(quizPercentage)) {
    statusMsg('Percentage should be numeric value', 'error');
    return;
  }

  if (quizPercentage <= 0 || quizPercentage > 100) {
    statusMsg('Percentage should be greater than 0 and less than 100', 'error');
    return
  }
  courseCreateJSON.quizPercentage = quizPercentage;

  if (courseName)
    courseCreateJSON.courseName = courseName;
  else {
    statusMsg('Course name cannot be empty', 'error');
    return;
  }

  if (modalNode.querySelector('.course-image-preview').hasAttribute('src')) {
    let courseImage = modalNode.querySelector('.course-image-preview').getAttribute('src');

    if (courseImage) courseCreateJSON.courseImage = courseImage;
  } else
    courseCreateJSON.courseImage = 'https://storage.googleapis.com/gajendran-mohan.appspot.com/undraw_check_boxes_m3d0.svg';

  return courseCreateJSON;
};


createOrUpdateCourse = async (method, courseCreateJSON) => {

  let response = await xhr(method, "/api/course", courseCreateJSON);

  if (response && response.success) {

    console.log(response.data.course.key);

    document.querySelector('.backdrop').classList.remove('active');

    document.getElementById('course-creation').querySelector('.creation-modal').classList.remove('active');

    if (response.data.course.key)
      renderPage('lesson-creation', response.data.course.key);
    else renderPage('lesson-creation');

  } else statusMsg(capitalize(response.errorMessage), "error");

};


renderImagePreview = (origin, url) => {
  if (url)
    origin.querySelector('.course-image-preview').setAttribute('src', url);
};


async function editCourse(node) {
  console.log(node.getAttribute('id'));
  let response = await xhr("GET", "/api/course?courseID=" + node.getAttribute('id'));

  if (response && response.success) {

    let modalNode = document.getElementById('course-creation').querySelector('.creation-modal');
    modalNode.classList.add('active');

    window.scrollTo({
      top: 20,
      behavior: 'smooth'
    });

    modalNode.setAttribute('courseID', node.getAttribute('id'));

    document.querySelector('.backdrop').classList.add('active');

    bindEventListenerForCourseCreation(modalNode, response.data.course);
  } else statusMsg(capitalize(response.errorMessage), "error");
}



document.querySelector('.create-course').addEventListener('click', function () {

  let modalNode = document.getElementById('course-creation').querySelector('.creation-modal');
  modalNode.classList.add('active');
  document.querySelector('.backdrop').classList.add('active');
  bindEventListenerForCourseCreation(modalNode);
});