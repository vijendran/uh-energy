async function listSubscription() {

    let response = await xhr("GET", "/api/user/subscription/status");

    if (response && response.success && response.data.user) {

        var list = response.data.user;
        for (var index in response.data.user) {
            let data = list[index];
            var eachNode = document.getElementById("subscription").querySelector('.subscription-template');

            var tempNode = eachNode.cloneNode(true);
            let eachData = data.user;

            tempNode.querySelector('.name').innerHTML = eachData.name;
            tempNode.querySelector('.email').innerHTML = eachData.email;
            tempNode.querySelector('.dateAdded').innerHTML = getDate(eachData.dateAdded);
            tempNode.querySelector('.action-approve').innerHTML = 'Approve';
            tempNode.querySelector('.deny-request').innerHTML = "Deny";
            tempNode.querySelector('.deny-request').setAttribute('contactID', eachData.key);
            tempNode.querySelector('.action-approve').setAttribute('contactID', eachData.key);
            if (data && data.course && data.course.courseName) {

                tempNode.querySelector('.deny-request').setAttribute('courseID', data.course.key);
                tempNode.querySelector('.action-approve').setAttribute('courseID', data.course.key);
                
                tempNode.querySelector('.c-name').innerHTML = data.course.courseName;
                tempNode.querySelector('.c-name').setAttribute('courseID', data.course.key);
            } else tempNode.querySelector('.c-name').innerHTML = "Course name is not found";

            tempNode.querySelector('.action-approve').addEventListener('click', async function (node) {

                var json = {};
                json.contactID = node.target.getAttribute('contactID');
                json.courseID = node.target.getAttribute('courseID');
                json.action = 'approved';

                let response = await xhr("PUT", "/api/user/subscription/status", json);
                console.log(response);

                if (response && response.success) {
                    statusMsg("User is approved, Mail has been sent to the user with credentials", "success");

                    node.target.parentNode.parentNode.remove();

                }
            });

            tempNode.querySelector('.deny-request').addEventListener('click', async function (node) {

                var json = {};
                json.contactID = node.target.getAttribute('contactID');
                json.courseID = node.target.getAttribute('courseID');
                json.action = 'denied';

                let response = await xhr("PUT", "/api/user/subscription/status", json);
                console.log(response);

                if (response && response.success) {
                    statusMsg("User request is denied", "success");

                    node.target.parentNode.parentNode.remove();

                }
            });
            tempNode.removeAttribute('style');
            document.getElementById("subscription-list").appendChild(tempNode);
        }
    } else {
        document.getElementById("subscription").classList.add('hide');
        statusMsg("No Pending request found!", "success");
    }
}

async function listCourseStatus() {

    let response = await xhr("GET", "/api/user/quiz/status");

    if (response && response.success && response.data) {

        var userList = response.data.quizStatus;
        let dataset = {};
        for (var index in response.data.quizStatus) {

            let data = userList[index];
            var eachNode = document.getElementById("quiz-stats").querySelector('.quiz-stat-template');

            eachNode.setAttribute("id", data.quizStatus.key);
            var tempNode = eachNode.cloneNode(true);

            tempNode.querySelector('.name').innerHTML = data.user.name;
            tempNode.querySelector('.email').innerHTML = data.user.email;
            tempNode.querySelector('.dateCompleted').innerHTML = getDate(data.quizStatus.dateCompleted);

            if (data.course && data.course.courseName) {
                tempNode.querySelector('.c-name').innerHTML = data.course.courseName;
                if (!dataset[data.course.courseName])
                    dataset[data.course.courseName] = {};
                else {
                    if ('pass' == data.quizStatus.status)
                        dataset[data.course.courseName].pass = (dataset[data.course.courseName].pass) ? (dataset[data.course.courseName].pass + 1) : 1;
                    else dataset[data.course.courseName].fail = (dataset[data.course.courseName].fail) ? (dataset[data.course.courseName].fail + 1) : 1;
                }
                // dataset[data.course.courseName] = (dataset.data.course.courseName ? (dataset.data.course.courseName + 1) : 1);
            }
            tempNode.querySelector('.status').innerHTML = capitalize(data.quizStatus.status);

            tempNode.removeAttribute('style');
            document.getElementById("course-status").appendChild(tempNode);

        }

        window.dataset = dataset;


        let ary = [];
        response.data.quizStatus.forEach(function (node, index) {

            ary[index] = node.dateCompleted;

            node
        });
    } else {
        document.getElementById("quiz-stats").classList.add('hide');
        statusMsg("No records found!", "success");
    }
}

getDate = (longMilli) => {
    return new Date(longMilli).toDateString();
}

generateGraph = async () => {

    let resp = await xhr('GET', '/api/graph');

    if (resp.success) {

        var barChartData = await getDataForChart(resp.data);

        var ctx = document.getElementById('myChart').getContext('2d');

        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Dashboard'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false
                        },
                        barPercentage: 0.3
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            min: 0
                        }
                    }]
                }
            }
        });
    }
};

getDataForChart = async (data) => {

    var quizStatus = data.quizStatus,
        totalUserMap = data.totalUsers,
        courseNameMap = data.courseName,
        totalUsersKey = Object.keys(totalUserMap);

    var courseName = [],
        passCount = [],
        failCount = [],
        totalUsers = [];

    for (var i = 0; i < totalUsersKey.length; i++) {

        totalUsers.push(totalUserMap[totalUsersKey[i]]);

        var quizInfo = await getQuizStatus(totalUsersKey[i], quizStatus);

        if (quizInfo) {
            courseName.push(quizInfo['name']);
            passCount.push(quizInfo['pass']);
            failCount.push(quizInfo['fail']);
        } else {
            courseName.push(courseNameMap[totalUsersKey[i]]);
            passCount.push(0);
            failCount.push(0);
        }


    }

    var barChartData = {
        labels: courseName, // COURSE NAME
        datasets: [{
            label: 'Total Users',
            fill: false,
            backgroundColor: fillArray("rgba(54, 162, 235, 0.2)", totalUsers.length),
            data: totalUsers
        }, {
            label: 'Passed users',
            fill: false,
            backgroundColor: fillArray("rgba(75, 192, 192, 0.2)", passCount.length),
            data: passCount
        }, {
            label: 'Failed users',
            fill: false,
            backgroundColor: fillArray("rgba(255, 99, 132, 0.2)", failCount.length),
            data: failCount
        }]
    }

    return barChartData;

}

function fillArray(value, len) {
    var arr = [];
    for (var i = 0; i < len; i++) {
        arr.push(value);
    }
    return arr;
}

function getQuizStatus(courseID, data) {

    var resp;
    for (let each in data) {
        let eachObject = Object.keys(data[each]);

        eachObject.forEach(function (courseIDKey) {
            if (courseID == courseIDKey) {
                resp =  data[each][courseID];
                return;
            }
        });

        if(resp)
            break;
    }
    return resp;
   
}