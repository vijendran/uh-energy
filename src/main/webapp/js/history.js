 window.history.index = 0;

 historyPush = function (title, uri) {
  if (history) {

   history.pushState({
    index: window.history.index
   }, title, uri);
   window.history.index++;
  }
 }

 historyReplace = function (state, title, uri) {
  if (history) {
   history.replaceState({
    index: window.history.index
   }, title, uri);

   window.history.index++;
  }
 }

 window.onpopstate = function (event) {
  if (event.state) {
   state = event.state;
  }
  renderPage(window.location.pathname.split('/')[1], window.location.pathname);
 };