async function xhr(method, uri, body, handler) {
    var xhr = new XMLHttpRequest();
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    return new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status >= 300) {
                    console.log("rejected");
                    reject("Error, status code = " + xhr.status);
                } else {
                    if (xhr && xhr.responseText) {
                        try {
                            let jsonResponse = JSON.parse(xhr.responseText);
                            resolve(jsonResponse);
                        } catch (error) {
                            console.log("error ",error);
                            reject("Error");
                        }
                    }
                }
            }
        }

        xhr.open(method, uri);
        if (body) {
            xhr.send(JSON.stringify(body));
        } else xhr.send();
    });

}