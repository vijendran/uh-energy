var ajax = req => {
 var fetchData = async () => {
  var headers = new Headers();
  if (req.contentType) {
   headers['Content-Type'] = req.contentType;
  }

  var initData = {
   method: req.method,
   body: req.data,
   headers: headers
  };
  try {
   var resp = (await fetch(new Request(req.url, initData))).json();
   resp.then(fetchJSON => {
    console.log(fetchJSON);
   });
   return resp;
  } catch (e) {
   return {};
  }
 };
 return fetchData();
}