isNull = (parameter) => {
    if (parameter) {
        return false;
    }
    return true;
};

isString = (string) => {
    if (isNull(string) || !(typeof (string) === 'string')) {
        return false;
    }

    return (string.trim() == '') ? false : true;
};

isInteger = (intValue) => {

    if (intValue == null || typeof (intValue) == "undefined" || 'string' === typeof (intValue)) {
        return false;
    }
    let value = Number(intValue);
    if (Math.floor(value) == value) {
        return true;
    } else {
        return false;
    }
};

isArray = (array) => {

    if (isNull(array) || !(typeof array === "object")) {
        return false;
    }

    return array.length > 0 ? true : false;
};

isEmailID = (emailID) => {

    var EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        FIRST_CHAR = /^[a-zA-Z0-9]/;

    if (!isString(emailID)) {
        return false;
    }
    if (EMAIL.test(emailID.trim()) && FIRST_CHAR.test(emailID.trim())) {
        let localPart = emailID.split("@")[0];
        if (localPart.length > 64) {
            return false;
        }
        return true;
    }
    return false;
};

isObject = (obj) => {
    if (!obj) {
        return false;
    }
    if (typeof (obj) != "object" || Object.keys(obj).length === 0) {
        return false;
    }
    return true;
};

statusMsg = (message, status) => {
    if (status && message) {
        let node = document
            .querySelector('#status-msg');

        node.querySelector('span').innerHTML = message;

        status = (status === 'error') ? 'error' : status;

        node.classList.add(status);
        node.classList.add('slide-left');

        node.classList.remove('hide');

        setTimeout(() => {

            if (document.querySelector('#status-msg').classList.contains('auth')) {
                document.querySelector('#status-msg').setAttribute('class', 'auth hide vb');
            } else document.querySelector('#status-msg').setAttribute('class', 'hide vb');
        }, 2e3);
    }
};

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
};

isValidPhoneNumber = (number) => {

   var phoneno = /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/;
   if ((number.match(phoneno)) && number.length>=10 && number.length<=15) {
       return true;
   } else {
       return false;
   }
};

getDate = (longMilli) => {
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    return new Date(longMilli).getDate() + "/" + monthNames[new Date(longMilli).getMonth()] + "/" + new Date(longMilli).getFullYear();
}