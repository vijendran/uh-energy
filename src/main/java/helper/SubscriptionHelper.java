package helper;

import com.google.appengine.api.datastore.Entity;
import dao.CourseDAO;
import dao.SubscriptionDAO;
import dao.UserDAO;
import entity.Course;
import entity.Subscription;
import entity.User;
import enums.ErrorResponse;
import query.SubscriptionQueryHelper;
import query.UserQueryHelper;
import utility.*;

import java.util.*;

public class SubscriptionHelper {

    private static SubscriptionHelper instance;

    private SubscriptionHelper() {
    }

    public static synchronized SubscriptionHelper instance() {
        if (instance == null)
            return new SubscriptionHelper();
        return instance;
    }

    public static HashMap<String, Object> getPendingSubscription(String cursor) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        HashMap<String, Object> subscribedUsersMap = SubscriptionQueryHelper.instance().getPendingUsers(cursor,
                AppConstants.STATUS_PENDING);

        if (!Validator.isNullOrEmpty(subscribedUsersMap)) {

            List<Entity> subscribedUsersList = (List<Entity>) subscribedUsersMap.get("users");
            System.out.println(subscribedUsersMap + " subscribedUsersMap");
            if (!Validator.isEmptyList(subscribedUsersList)) {

                List<HashMap<String, Object>> userList = new ArrayList<>();

                for (Entity obj : subscribedUsersList) {
                    if (!Validator.isNull(obj)) {

                        HashMap<String, Object> responseObject = new HashMap<>();
                        Subscription subscription = SubscriptionDAO.instance().entityToObject(obj);

                        User objUser = UserDAO.instance().get(subscription.getContactID());

                        if (!Validator.isNullOrEmpty(subscription.getCourseID())) {
                            Course course = CourseDAO.instance().get(subscription.getCourseID());
                            if (course != null && !Validator.isNullOrEmpty(course.getKey()))
                                responseObject.put("course", course);
                        }

                        if (!Validator.isNull(objUser.getKey())) {
                            responseObject.put("user", objUser);
                            userList.add(responseObject);
                        }
                    }
                }

                responseMap.put("user", userList);
                if (subscribedUsersMap.containsKey("cursor")) {
                    responseMap.put("cursor", subscribedUsersMap.get("cursor"));
                }
            }
        }
        return responseMap;
    }

    public static HashMap<String, Object> subscription(User user, String courseID) throws Exception {
        HashMap<String, Object> responseMap = new HashMap<>();
        PreConditions.checkArgument(Validator.isNull(user), ErrorResponse.CommonErrorMsg.INVALID_SUBSCRIPTION.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(user.getKey()),
                ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value());
        Subscription subscription = new Subscription();
        subscription.setKey(String.valueOf(UUID.randomUUID()));
        subscription.setContactID(user.getKey());
        subscription.setCourseID(courseID);
        subscription.setDateAdded(new Date().getTime());
        subscription.setDateModified(new Date().getTime());
        subscription.setStatus(AppConstants.STATUS_PENDING);
        SubscriptionDAO.instance().set(subscription);
        responseMap.put("subscription", subscription);
        return responseMap;
    }

    public static HashMap<String, Object> updatePaymentStatus(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        String contactID = payloadMap.containsKey("contactID") ? (String) payloadMap.get("contactID") : "";
        String courseID = payloadMap.containsKey("courseID") ? (String) payloadMap.get("courseID") : "";
        String action = payloadMap.containsKey("action") ? (String) payloadMap.get("action") : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID),
                ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(action),
                ErrorResponse.CommonErrorMsg.INVALID_CONSTRAINTS.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        Subscription objSubscription = SubscriptionQueryHelper.getPaymentStatusForThisContactID(contactID, courseID);

        System.out.println("contactID :: +" + contactID + "courseID :: " + courseID);

        if (!Validator.isNull(objSubscription) && !Validator.isNullOrEmpty(objSubscription.getKey())) {

            if (!objSubscription.getStatus().toLowerCase().equalsIgnoreCase(action)) {

                User objUser = UserDAO.instance().get(objSubscription.getContactID());

                if (AppConstants.STATUS_APPROVED.toLowerCase().equalsIgnoreCase(action.trim().toLowerCase())) {

                    objSubscription.setStatus(AppConstants.STATUS_APPROVED);

                    HashMap<String, Object> adminMap = UserQueryHelper.getAdminMailID("admin");

                    String admin_mail = "";

                    if (!Validator.isNullOrEmpty(adminMap)) {

                        List<Entity> usersList = (List<Entity>) adminMap.get("users");
                        if (!Validator.isEmptyList(usersList)) {
                            User admiObj = UserDAO.instance().entityToObject(usersList.get(0));
                            admin_mail = admiObj.getEmail();
                        }
                    }
                    String mail_content = "";

                    if (!SubscriptionQueryHelper.checkUserAlreadySubscribed(objUser.getKey())) {
                        System.out.println("This is here false");
                        String passwordToSendInEmail = objUser.getPassword(); // This will have the unhashed password to send it
                        objUser.setPassword(Utils.convertToMD5HashValue(passwordToSendInEmail));
                        UserDAO.instance().set(objUser);

                        mail_content = "Hi " + objUser.getName() + ",\n\nThank you for registering to UH Energy "
                                + "Your subscription has approved, Please note, the following instructions."
                                + "You can login with the below credential to complete the course. \n\n";

                        mail_content = mail_content + "Your registered email: " + objUser.getEmail() + "\n"
                                + "Password : " + passwordToSendInEmail + "\n\n" +
                                // "Login URl - \n" +
                                "If you have any questions, please reply to " + admin_mail + ". \n\n" + "Thanks,\n"
                                + "UH Energy";
                    } else {

                        System.out.println("This is here true");
                        
                        mail_content = "Hi " + objUser.getName() + ",\n\nThank you for registering to UH Energy "
                                + "Your subscription has approved, You can login with the existing credential. \n\n";

                        mail_content += "If you have any questions, please reply to " + admin_mail + ". \n\n" + "Thanks,\n"
                                + "UH Energy";

                    }

                    // MailHelper.sendMail(AppConstants.MAIL_SENDER, objUser.getEmail(),
                    // "Subscription email", "Thank you for choosing the course, here is your login
                    // credentials \n login : " + objUser.getEmail() + " \n password " +
                    // passwordToSendInEmail);
                    MailHelper.sendMail(AppConstants.MAIL_SENDER, objUser.getEmail(), "Enrollment for Course",
                            mail_content);
                } else if (AppConstants.STATUS_DENIED.toLowerCase().equalsIgnoreCase(action.trim().toLowerCase())) {

                    objSubscription.setStatus(AppConstants.STATUS_DENIED);
                    MailHelper.sendMail(AppConstants.MAIL_SENDER, objUser.getEmail(), "Enrollment for Course",
                            "Thank you for choosing this course, but sorry, we are not moving you forward to take the course from you end.");

                }

                SubscriptionDAO.instance().set(objSubscription);

            }

            responseMap.put("status", objSubscription);

        } else {
            System.out.println("not inside if");
        }

        return responseMap;
    }

    public static HashMap<String, Object> getUserExistence(String courseID, String contactID) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        Subscription objSubcription = SubscriptionQueryHelper.getPaymentStatusForThisContactIDWithStatus(courseID,
                contactID, AppConstants.STATUS_APPROVED);

        if (!Validator.isNull(objSubcription)) {
            responseMap.put("subscription", objSubcription);
        }

        return responseMap;
    }

    public static HashMap<String, Object> enrollUser(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        String courseID = payloadMap.containsKey("courseID") ? (String) payloadMap.get("courseID") : "";
        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
        String contactID = payloadMap.containsKey("contactID") ? (String) payloadMap.get("contactID") : "";
        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID),
                ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());

        System.out.println(contactID + " " + courseID);

        User objUser = UserDAO.instance().get(contactID);

        Subscription objSubcription = SubscriptionQueryHelper.getPaymentStatusForThisContactID(contactID, courseID);

        System.out.println(JSONUtils.getJson(objSubcription));
        HashMap<String, Object> responseMap = new HashMap<>();

        if (!Validator.isNull(objSubcription) && !Validator.isNullOrEmpty(objSubcription.getKey())) {
            // responseMap = SubscriptionHelper.subscription(objUser, courseID);
            if (AppConstants.STATUS_PENDING.equalsIgnoreCase(objSubcription.getStatus())
                    || AppConstants.STATUS_APPROVED.equalsIgnoreCase(objSubcription.getStatus())) {
                responseMap.put("subscription", objSubcription);
            } else if (AppConstants.STATUS_DENIED.equalsIgnoreCase(objSubcription.getStatus())) {
                objSubcription.setStatus(AppConstants.STATUS_PENDING);
                SubscriptionDAO.instance().set(objSubcription);
                responseMap.put("subscription", objSubcription);
            }
        } else {
            responseMap = SubscriptionHelper.subscription(objUser, courseID);
        }

        return responseMap;

    }
}
