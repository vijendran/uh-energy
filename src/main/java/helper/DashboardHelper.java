package helper;

import com.google.appengine.api.datastore.Entity;
import dao.CourseDAO;
import dao.SubscriptionDAO;
import dao.UserDAO;
import entity.Course;
import entity.Subscription;
import entity.User;
import enums.ErrorResponse;
import query.SubscriptionQueryHelper;
import utility.AppConstants;
import utility.JSONUtils;
import utility.PreConditions;
import utility.Validator;

import java.util.HashMap;
import java.util.List;

public class DashboardHelper {

    public static HashMap<String, Object> generateGraphData() throws Exception {

        HashMap<String, Object> resp = new HashMap<>();

        HashMap<String, Object> subscribedUsersMap = SubscriptionQueryHelper.instance().getPendingUsers("", AppConstants.STATUS_APPROVED);

        if (!Validator.isNullOrEmpty(subscribedUsersMap)) {

            List<Entity> subscribedUsersList = (List<Entity>) subscribedUsersMap.get("users");

            System.out.println(subscribedUsersList);

            HashMap totalUserMap = new HashMap<String, Integer>();
            HashMap courseNameMap = new HashMap<String, Integer>();

            System.out.println(subscribedUsersList.size());
            for (Entity obj : subscribedUsersList) {

                if (!Validator.isNull(obj)) {

                    Subscription subscription = SubscriptionDAO.instance().entityToObject(obj);

                    if (!Validator.isNullOrEmpty(subscription.getCourseID())) {

                        if (totalUserMap.containsKey(subscription.getCourseID())) {

                            Integer currentValue = (Integer) totalUserMap.get(subscription.getCourseID());
                            totalUserMap.put(subscription.getCourseID(), currentValue + 1);
                        } else {
                            totalUserMap.put(subscription.getCourseID(), 1);
                        }

                        if (!courseNameMap.containsKey(subscription.getCourseID())) {
                            Course objCourse = CourseDAO.instance().get(subscription.getCourseID());
                            courseNameMap.put(subscription.getCourseID(), objCourse.getCourseName());
                        }
                    }
                }
            }

            resp.put("totalUsers", totalUserMap);
            resp.put("courseName", courseNameMap);

            HashMap<String, Object> quizStatusMap = QuizHelper.instance().getQuizStatusForAllUser("", "");

            if (!Validator.isNullOrEmpty(quizStatusMap) && quizStatusMap.containsKey("report")) {
                resp.put("quizStatus", quizStatusMap.get("report"));
            }


        }

        return resp;
    }

    public static HashMap<String, Object> sendUpdatePasswordMail(String paylaod) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(paylaod), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(paylaod);

        String contactID = payloadMap.containsKey("contactID") ? (String) payloadMap.get("contactID") : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID), ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        User objUser = UserDAO.instance().get(contactID);

        String link = "https://gajendran-mohan.el.r.appspot.com/reset-password?contactID="+contactID;
        String mail_content = "Please use the below link to reset password "+link;

        MailHelper.sendMail(AppConstants.MAIL_SENDER, objUser.getEmail(), "Update password",
                mail_content);

        System.out.println("mail_content :: "+mail_content);

        return responseMap;
    }
}
