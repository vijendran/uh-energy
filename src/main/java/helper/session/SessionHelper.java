package helper.session;

import utility.PreConditions;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionHelper {

    public static boolean hasUserSession(HttpServletRequest servletRequest) {
        return !Validator.isNull(getCurrentUserFromSession(servletRequest));
    }

    public static HttpSession currentHttpSession(HttpServletRequest servletRequest) {
        return servletRequest.getSession(false);
    }

    public static CurrentSession getCurrentUserFromSession(HttpServletRequest servletRequest) {

        PreConditions.checkArgument(Validator.isNull(servletRequest), "Invalid servlet request");

        HttpSession session = currentHttpSession(servletRequest);
        if (session == null) return null;

        return (CurrentSession) session.getAttribute("energy_user");
    }

    public static void setupSessionForUser(HttpServletRequest servletRequest, HttpServletResponse servletResponse, CurrentSession currentSession) {

        PreConditions.checkArgument(Validator.isNull(currentSession), "Invalid session user");
        PreConditions.checkArgument(Validator.isNullOrEmpty(currentSession.getUserID()), "Invalid user ID");
        PreConditions.checkArgument(Validator.isNullOrEmpty(currentSession.getEmail()), "Invalid user name");
        PreConditions.checkArgument(Validator.isNullOrEmpty(currentSession.getRole()), "Invalid role");


        invalidateSession(servletRequest);

        HttpSession session = servletRequest.getSession(true);

        session.setAttribute("energy_user", currentSession);

    }

    public static void invalidateSession(HttpServletRequest servletRequest) {

        HttpSession session = currentHttpSession(servletRequest);
        if (Validator.isNull(session))
            return;

        try {
            session.invalidate();
        } catch (IllegalStateException e) {
        }
    }
}


