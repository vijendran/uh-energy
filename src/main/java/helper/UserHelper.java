package helper;

import com.google.appengine.api.datastore.Entity;
import dao.UserDAO;
import entity.User;
import helper.session.CurrentSession;
import helper.session.SessionHelper;
import query.UserQueryHelper;
import utility.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class UserHelper {

    private static UserHelper instance;

    private UserHelper() {
    }

    public static synchronized UserHelper instance() {
        if (instance == null)
            return new UserHelper();
        return instance;
    }

    public static HashMap<String, Object> user(String payload, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HashMap userData = new HashMap<>();

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Payload cannot be empty");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Payload cannot be empty");

        String emailID = payloadMap.containsKey("emailID") ? String.valueOf(payloadMap.get("emailID")) : "";

        String phoneNumber = payloadMap.containsKey("phoneNumber") ? String.valueOf(payloadMap.get("phoneNumber")) : "";
        String name = payloadMap.containsKey("name") ? String.valueOf(payloadMap.get("name")) : "";

        String courseID = payloadMap.containsKey("courseID") ? String.valueOf(payloadMap.get("courseID")) : "";

        User user = UserQueryHelper.instance().queryForUserName(emailID);

        PreConditions.checkArgument(!Validator.isNull(user), "User already exists");

        user = setUser(new User(), emailID, phoneNumber, name, null);

        SubscriptionHelper.subscription(user, courseID);

        String admin_mail_content = "Hey, \n\nNew user has signed with for the course and his email Id is " + emailID;

        String adminMail = "";

        HashMap<String, Object> adminMap = UserQueryHelper.getAdminMailID("admin");

        if (!Validator.isNullOrEmpty(adminMap)) {

            List<Entity> usersList = (List<Entity>) adminMap.get("users");
            if (!Validator.isEmptyList(usersList)) {

                for (Entity obj : usersList) {

                    if (!Validator.isNull(obj)) {
                        User adminObj = UserDAO.instance().entityToObject(obj);
                        System.out.println(adminObj.getEmail());
                        MailHelper.sendMail(AppConstants.MAIL_SENDER, adminObj.getEmail(), "New User Registration Mail",
                                admin_mail_content);
                        adminMail = adminObj.getEmail();
                    }
                }
            }
        }

        String userMailContent = "Hi " + name + ",\n\n" + "Thank you for registering to UH Energy."
                + "Stay tuned until your subscription is approved.You will shortly notified with the instruction to proceed via email.\n\n"
                + "If you have any questions, please reply to " + adminMail + ".\n\n" + "Thanks,\n" + "UH Energy ";
        // String user_mail_content = "Thank you for choosing the course, the admin will
        // contact you with further details and send you the credentials";

        MailHelper.sendMail(AppConstants.MAIL_SENDER, emailID, "Registration Successful", userMailContent);

        userData.put("msg", "Registered successfully");

        return userData;
    }

    public static HashMap<String, Object> authenticateUser(HttpServletRequest request, HttpServletResponse response,
                                                           String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Invalid Payload");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        String emailID = payloadMap.containsKey("emailID") ? String.valueOf(payloadMap.get("emailID")) : "";
        String password = payloadMap.containsKey("password") ? String.valueOf(payloadMap.get("password")) : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(emailID), "Invalid EmailID");
        PreConditions.checkArgument(Validator.isNullOrEmpty(password), "Invalid Password");

        System.out.println(password);
        HashMap<String, Object> responseMap = new HashMap<>();

        String hashed_password = Utils.convertToMD5HashValue(password);

        User objUser = UserQueryHelper.queryForUserNameAndPassword(emailID, hashed_password);

        System.out.println(objUser);
        if (!Validator.isNull(objUser)) {
            CurrentSession objCurrentSetting = new CurrentSession();
            objCurrentSetting.setEmail(emailID);
            objCurrentSetting.setUserID(objUser.getKey());
            objCurrentSetting.setRole(objUser.getRole());
            objCurrentSetting.setName(objUser.getName());

            SessionHelper.setupSessionForUser(request, response, objCurrentSetting);
            responseMap.put("redirectURI", "/course");
            responseMap.put("message", "User logged in successfully");
        }

        return responseMap;
    }

    private static User setUser(User user, String emailID, String phoneNumber, String name, String password)
            throws Exception {

        PreConditions.checkArgument((Validator.isNullOrEmpty(emailID) && !Validator.isValidEmail(emailID)),
                "Invalid EmailID");
        PreConditions.checkArgument(Validator.isNullOrEmpty(phoneNumber), "Invalid PhoneNumber");
        PreConditions.checkArgument(Validator.isNullOrEmpty(name), "Invalid Name");

        if (Validator.isNullOrEmpty(user.getKey())) {
            user.setKey(String.valueOf(UUID.randomUUID()));
            user.setDateAdded(new Date().getTime());
        }

        user.setEmail(emailID);

        if (Validator.isNullOrEmpty(user.getPassword()) && Validator.isNullOrEmpty(password))
//            user.setPassword(makePassword(name, phoneNumber));
            user.setPassword(makePassword());
        else {
            user.setPassword(password);
        }

        user.setPhoneNumber(phoneNumber);
        user.setName(name);
        user.setRole("user");

        UserDAO.instance().set(user);

        return user;
    }

    //    private static String makePassword(String name, String phoneNumber) {
    private static String makePassword() {

//        String password = "";
//
//        if (name.length() > 4 && phoneNumber.length() > 4) {
//            password = name.substring(name.length() - 4);
//            password += phoneNumber.substring(phoneNumber.length() - 4);
//        }
//        return (!Validator.isNullOrEmpty(password)) ? password : String.valueOf(UUID.randomUUID());

        StringBuilder builder = new StringBuilder();
        int count = 6;
        while (count-- != 0) {
            int character = (int) (Math.random() * AppConstants.ALPHA_NUMERIC_STRING.length());
            builder.append(AppConstants.ALPHA_NUMERIC_STRING.charAt(character));
        }
        
        return builder.toString();

    }

    public static HashMap<String, Object> updateUserName(String payload, String contactID) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Invalid Payload");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        String name = payloadMap.containsKey("name") ? String.valueOf(payloadMap.get("name")) : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(name), "Invalid Name");

        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID), "Invalid contactID");

        User user = UserDAO.instance().get(contactID);
        user.setName(name);
        UserDAO.instance().set(user);

        responseMap.put("user", user);
        return responseMap;
    }

    public static HashMap<String, Object> updatePassword(String payload) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Invalid Payload");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        String password = payloadMap.containsKey("password") ? String.valueOf(payloadMap.get("password")) : "";

        String contactID = payloadMap.containsKey("contactID") ? String.valueOf(payloadMap.get("contactID")) : "";
        String confirmPassword = payloadMap.containsKey("confirmpassword")
                ? String.valueOf(payloadMap.get("confirmpassword"))
                : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(confirmPassword), "Invalid Confirm password");
        PreConditions.checkArgument(Validator.isNullOrEmpty(password), "Invalid Password");
        PreConditions.checkArgument(password.trim().length() != confirmPassword.trim().length(),
                "Password and confirm password should be same");

        PreConditions.checkArgument(password.length() != password.trim().length(),
                "You cannot have space at the end of password");

        PreConditions.checkArgument(password.trim().length() < 6, "Password must have minimum of 6 character");

        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID), "Invalid contactID");

        User user = UserDAO.instance().get(contactID);

        PreConditions.checkArgument((user == null || Validator.isNullOrEmpty(user.getKey())), "Invalid User");

        user.setPassword(Utils.convertToMD5HashValue(password));
        UserDAO.instance().set(user);

        responseMap.put("user", user);
        return responseMap;
    }

    public static HashMap<String, Object> updateRole(String payload, String currentUserID) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Invalid Payload");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        String role = payloadMap.containsKey("role") ? String.valueOf(payloadMap.get("role")) : "";
        String email = payloadMap.containsKey("emailID") ? String.valueOf(payloadMap.get("emailID")) : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(email), "Invalid email");

        // User currentUser = UserDAO.instance().get(currentUserID);
        // if (currentUser != null && !Strings.isNullOrEmpty(currentUser.getKey())
        //         && "admin".equalsIgnoreCase(currentUser.getRole())) {

        User user = UserQueryHelper.instance().queryForUserName(email);

        PreConditions.checkArgument((user == null || Validator.isNullOrEmpty(user.getKey())), "Invalid User");

        user.setRole(role);
        UserDAO.instance().set(user);

        responseMap.put("user", user);
        // } else
        //     PreConditions.checkArgument(true, "Current User should be admin to change the role");

        return responseMap;
    }

    public static HashMap<String, Object> createAdmin(String payload)
            throws Exception {

        HashMap userData = new HashMap<>();

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Payload cannot be empty");

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);
        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), "Payload cannot be empty");

        String emailID = payloadMap.containsKey("emailID") ? String.valueOf(payloadMap.get("emailID")) : "";

        String phoneNumber = payloadMap.containsKey("phoneNumber") ? String.valueOf(payloadMap.get("phoneNumber")) : "";
        String name = payloadMap.containsKey("name") ? String.valueOf(payloadMap.get("name")) : "";
        String password = payloadMap.containsKey("password") ? String.valueOf(payloadMap.get("password")) : "";

        System.out.println("this is here");
        User user = UserQueryHelper.instance().queryForUserName(emailID);

        PreConditions.checkArgument(!Validator.isNull(user), "User already exists");

        if (Validator.isNull(user)) {
            user = new User();
            user.setKey(String.valueOf(UUID.randomUUID()));
            user.setDateAdded(new Date().getTime());
        }

        user.setEmail(emailID);

        user.setPassword(Utils.convertToMD5HashValue(password));

        user.setPhoneNumber(phoneNumber);
        user.setName(name);
        user.setRole("admin");

        UserDAO.instance().set(user);

        userData.put("msg", "Registered successfully");

        return userData;
    }

}
