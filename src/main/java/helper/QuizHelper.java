package helper;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;

import org.json.JSONObject;

import dao.CourseDAO;
import dao.QuizDAO;
import dao.QuizStatusDAO;
import entity.Course;
import entity.Quiz;
import entity.QuizStatus;
import enums.ErrorResponse;
import query.QuizQueryHelper;
import utility.AppConstants;
import utility.JSONUtils;
import utility.PreConditions;
import utility.Validator;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class QuizHelper {

        private static QuizHelper instance;

        private QuizHelper() {
        }

        public static synchronized QuizHelper instance() {
                if (instance == null)
                        return new QuizHelper();
                return instance;
        }

        private static final Logger log = Logger.getLogger(QuizHelper.class.getSimpleName());

        public static HashMap<String, Object> createQuizz(String payload) throws Exception {

                PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

                HashMap<String, Object> quizPayload = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

                PreConditions.checkArgument(
                                !quizPayload.containsKey("question")
                                                || Validator.isNullOrEmpty(String.valueOf(quizPayload.get("question"))),
                                "question cannot be empty");

                String courseID = quizPayload.containsKey("courseID") ? String.valueOf(quizPayload.get("courseID"))
                                : "";

                PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

                String questionID = quizPayload.containsKey("questionID")
                                ? String.valueOf(quizPayload.get("questionID"))
                                : "";

                String type = quizPayload.containsKey("type") ? String.valueOf(quizPayload.get("type")) : "";

                PreConditions.checkArgument(Validator.isNullOrEmpty(type), "Invalid quiz type");

                PreConditions.checkArgument(!Arrays.stream(AppConstants.QUIZZ_TYPE).anyMatch(type::equals),
                                "Invalid quiz type");

                if (!AppConstants.PARAGRAPH.equalsIgnoreCase(type)) {

                        PreConditions.checkArgument(Validator.isNull(quizPayload.get("options")), "Invalid options");

                        ArrayList<String> optionsList = new ArrayList((Collection<?>) quizPayload.get("options"));

                        List<String> filteredOptionsList = optionsList.stream()
                                        .filter(str -> !Validator.isNullOrEmpty(str)).collect(Collectors.toList());

                        PreConditions.checkArgument((filteredOptionsList.size() < 2),
                                        "Minimum of 2 options should be given");

                        // paragraph not

                        PreConditions.checkArgument(Validator.isNull(quizPayload.get("correctAns")),
                                        "Should have correct answer");

                        ArrayList<String> correctAns = new ArrayList((Collection<?>) quizPayload.get("correctAns"));

                        PreConditions.checkArgument(Validator.isNullOrEmpty(correctAns), "Invalid correct answer");

                        ArrayList<String> filteredCorrectAns = new ArrayList<>();

                        filteredCorrectAns = (ArrayList<String>) correctAns.stream()
                                        .filter(str -> !Validator.isNullOrEmpty(str)).collect(Collectors.toList());

                        log.info("filteredCorrectAns :: " + filteredCorrectAns);

                        if (type.equals(AppConstants.MULTI_SELECT)) {
                                PreConditions.checkArgument((filteredCorrectAns.size() < 2),
                                                "Minimum of 2 correct answer should be given");
                        }

                        if (type.equals(AppConstants.SINGLE_SELECT) || type.equals(AppConstants.TRUE_OR_FALSE)) {
                                PreConditions.checkArgument((filteredCorrectAns.size() != 1),
                                                "Only one correct answer should be given");
                        }

                        ArrayList<String> matchingCorrectAns = new ArrayList<>();
                        if (type.equals(AppConstants.TRUE_OR_FALSE)) {

                                ArrayList<String> list = new ArrayList<>();
                                list.add(0, "True");
                                list.add(1, "False");
                                quizPayload.put("options", list);

                                optionsList.clear();
                                optionsList.addAll(list);

                                PreConditions.checkArgument(
                                                !filteredCorrectAns.stream()
                                                                .anyMatch(answer -> ("true".equalsIgnoreCase(answer)
                                                                                || "false".equalsIgnoreCase(answer))),
                                                "Only one correct answer should be given");

                        }

                        if (!AppConstants.PARAGRAPH.equalsIgnoreCase(type)) {

                                matchingCorrectAns = (ArrayList<String>) filteredCorrectAns.stream()
                                                .filter(str -> optionsList.contains(str)).collect(Collectors.toList());

                                String msg = "Should have the correct answer value in options";
                                if (type.equals(AppConstants.TRUE_OR_FALSE)) {
                                        msg = "Correct answer should be True or False : Case Sensitive";
                                }

                                PreConditions.checkArgument((!matchingCorrectAns.containsAll(filteredCorrectAns)), msg);
                        }

                }

                Quiz quiz = new Quiz();

                if (!Validator.isNullOrEmpty(questionID)) {
                        quiz = QuizDAO.instance().get(questionID);
                        PreConditions.checkArgument((quiz == null || Validator.isNullOrEmpty(quiz.getKey())),
                                        "Invalid QuizID");

                }

                quiz = saveQuizz(quizPayload, questionID);

                HashMap<String, Object> responseMap = new HashMap<>();
                responseMap.put("quiz", quiz);
                return responseMap;
        }

        private static Quiz saveQuizz(HashMap<String, Object> quizzPayload, String questionID) throws Exception {

                Quiz quiz = new Quiz();

                String type = (String) quizzPayload.get("type");

                quiz.setKey((!Validator.isNullOrEmpty(questionID) ? questionID : String.valueOf(UUID.randomUUID())));

                quiz.setCourseID((String) quizzPayload.get("courseID"));
                quiz.setDateAdded(new Date().getTime());
                quiz.setType(type);

                if (!AppConstants.PARAGRAPH.equalsIgnoreCase(type)) {
                        quiz.setCorrectAnswer((List<String>) quizzPayload.get("correctAns"));
                        quiz.setOptions((List<String>) quizzPayload.get("options"));
                }

                quiz.setQuestion(new Text((String) quizzPayload.get("question")));

                QuizDAO.instance().set(quiz);

                return quiz;
        }

        public HashMap<String, Object> getQuiz(String courseID, String cursor) throws Exception {

                PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
                Course course = CourseDAO.instance().get(courseID);
                PreConditions.checkArgument((course == null || Validator.isNullOrEmpty(course.getKey())),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

                HashMap<String, Object> responseMap = new HashMap<>();

                HashMap<String, Object> quizForThisCourseID = QuizQueryHelper.instance()
                                .getQuizForThisCourseID(courseID, cursor);

                if (!Validator.isNullOrEmpty(quizForThisCourseID)) {

                        List<Entity> quizEntityList = (List<Entity>) quizForThisCourseID.get("quiz");

                        if (!Validator.isEmptyList(quizEntityList)) {

                                List<Quiz> quizList = new ArrayList<>();

                                for (Entity obj : quizEntityList) {
                                        if (!Validator.isNull(obj)) {
                                                Quiz quiz = QuizDAO.instance()
                                                                .get(QuizDAO.instance().entityToObject(obj).getKey());
                                                if (!Validator.isNull(quiz.getKey())) {
                                                        quizList.add(quiz);
                                                }
                                        }
                                }

                                responseMap.put("quiz", quizList);
                                if (quizForThisCourseID.containsKey("cursor")) {
                                        responseMap.put("cursor", quizForThisCourseID.get("cursor"));
                                }
                        }
                }

                return responseMap;
        }

        public HashMap<String, Object> validateQuiz(String payload) throws Exception {

                PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

                HashMap<String, Object> responseMap = new HashMap<>();

                HashMap<String, Object> validatePayload = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

                PreConditions.checkArgument(
                                !validatePayload.containsKey("questionID") || Validator
                                                .isNullOrEmpty(String.valueOf(validatePayload.get("questionID"))),
                                "Question id cannot be empty");

                Quiz quiz = QuizDAO.instance().get(String.valueOf(validatePayload.get("questionID")));

                PreConditions.checkArgument(Validator.isNull(quiz) || Validator.isNullOrEmpty(quiz.getKey()),
                                "Invalid Question ID");

                PreConditions.checkArgument(
                                !validatePayload.containsKey("selectedAnswer")
                                                || (validatePayload.containsKey("selectedAnswer") && Validator
                                                                .isNull(validatePayload.get("selectedAnswer"))),
                                "Selected answer cannot be empty");

                ArrayList<String> selectedAnswer = new ArrayList((Collection<?>) validatePayload.get("selectedAnswer"));

                ArrayList<String> filteredAnswers = (ArrayList<String>) selectedAnswer.stream()
                                .filter(str -> !Validator.isNullOrEmpty(str)).collect(Collectors.toList());

                PreConditions.checkArgument(Validator.isNullOrEmpty(filteredAnswers),
                                "Selected answer cannot be empty");

                responseMap.put("type", quiz.getType());

                if (!AppConstants.PARAGRAPH.equalsIgnoreCase(quiz.getType())) {
                        if (filteredAnswers.equals(quiz.getCorrectAnswer())) {
                                responseMap.put("status", "correct");

                        } else {
                                responseMap.put("status", "in correct");
                        }
                        responseMap.put("correctAnswer", quiz.getCorrectAnswer());
                } else {
                        responseMap.put("status", "correct");
                }

                return responseMap;
        }

        public HashMap<String, Object> validateQuizStatus(String payload, String contactId) throws Exception {
                HashMap<String, Object> responseMap = new HashMap<>();

                PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

                HashMap<String, Object> validatePayload = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

                String courseID = validatePayload.containsKey("courseID")
                                ? String.valueOf(validatePayload.get("courseID"))
                                : "";

                PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

                Course course = CourseDAO.instance().get(courseID);

                PreConditions.checkArgument((course == null || Validator.isNullOrEmpty(course.getKey())),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

                int totalQuestion = validatePayload.containsKey("totalQuestion")
                                ? Integer.parseInt(String.valueOf(validatePayload.get("totalQuestion")))
                                : 0;

                int correctAnswer = validatePayload.containsKey("correctAnswer")
                                ? Integer.parseInt(String.valueOf(validatePayload.get("correctAnswer")))
                                : 0;

                QuizStatus quizStatus = new QuizStatus();

                quizStatus.setKey(contactId + course.getKey());
                quizStatus.setContactID(contactId);
                quizStatus.setCourseID(course.getKey());
                quizStatus.setDateCompleted(new Date().getTime());

                if (correctAnswer != 0 && totalQuestion != 0 && correctAnswer <= totalQuestion) {

                        log.info("calculated value" + (Math.abs((correctAnswer * 100 / totalQuestion))));
                        log.info("calculated value" + (course.getPercentage()));

                        quizStatus.setStatus((Math.abs((correctAnswer * 100 / totalQuestion)) >= course.getPercentage())
                                        ? "pass"
                                        : "fail");

                        // if (!"fail".equalsIgnoreCase(quizStatus.getStatus()))

                        responseMap.put("quizStatus", quizStatus);
                        responseMap.put("passPercentage", course.getPercentage());
                        responseMap.put("resultPercentage", (Math.abs((correctAnswer * 100 / totalQuestion))));
                } else {

                        quizStatus.setStatus("fail");
                        responseMap.put("quizStatus", quizStatus);
                        responseMap.put("passPercentage", course.getPercentage());
                        responseMap.put("resultPercentage", 0);
                }

                QuizStatusDAO.instance().set(quizStatus);
                return responseMap;
        }

        public HashMap<String, Object> getQuizStatus(String courseID, String contactID) throws Exception {

                PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
                Course course = CourseDAO.instance().get(courseID);
                PreConditions.checkArgument((course == null || Validator.isNullOrEmpty(course.getKey())),
                                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

                HashMap<String, Object> responseMap = new HashMap<>();

                QuizStatus quizStatus = QuizStatusDAO.instance().get(contactID + course.getKey());

                if (!Validator.isNull(quizStatus) && !Validator.isNullOrEmpty(quizStatus.getKey())) {
                        responseMap.put("quizStatus", quizStatus);
                        responseMap.put("passPercentage", course.getPercentage());
                        responseMap.put("status", "completed");
                } else {
                        responseMap.put("status", "notcompleted");
                }

                return responseMap;
        }

        public HashMap<String, Object> getQuizStatusForAllUser(String courseID, String cursor) throws Exception {

                HashMap<String, Object> responseMap = new HashMap<>();

                HashMap<String, Object> quizForAllCourseID = QuizQueryHelper.instance()
                                .getQuizStatusForThisCourseID(courseID, cursor);

                if (!Validator.isNullOrEmpty(quizForAllCourseID)) {

                        List<Entity> quizEntityList = (List<Entity>) quizForAllCourseID.get("quizStatus");

                        if (!Validator.isEmptyList(quizEntityList)) {

                                List<HashMap<String, Object>> quizList = new ArrayList<>();
                                HashMap<String, Object> passFailResponse = new HashMap();

                                for (Entity obj : quizEntityList) {
                                        if (!Validator.isNull(obj)) {
                                                QuizStatus quizStatus = QuizStatusDAO.instance().entityToObject(obj);
                                                Course course = CourseDAO.instance().get(quizStatus.getCourseID());

                                                if (course != null && !Validator.isNullOrEmpty(course.getKey())) {

                                                        log.info("test" + !Validator.isNull(passFailResponse
                                                                        .get(quizStatus.getCourseID())));

                                                        if (!Validator.isNull(passFailResponse
                                                                        .get(quizStatus.getCourseID()))) {
                                                                HashMap<String, Object> update = (HashMap<String, Object>) passFailResponse
                                                                                .get(course.getKey());
                                                                if ("pass".equalsIgnoreCase(quizStatus.getStatus())) {
                                                                        log.info("eachTime " + Integer.parseInt(
                                                                                        update.get("pass").toString()));
                                                                        update.put("pass", (Integer.parseInt(
                                                                                        update.get("pass").toString())
                                                                                        + 1));
                                                                } else if ("fail".equalsIgnoreCase(
                                                                                quizStatus.getStatus())) {
                                                                        log.info("eachTime " + Integer.parseInt(
                                                                                        update.get("pass").toString()));
                                                                        update.put("fail", (Integer.parseInt(
                                                                                        update.get("fail").toString())
                                                                                        + 1));
                                                                }
                                                        } else {
                                                                HashMap<String, Object> jsonObject = new HashMap();
                                                                log.info("FirstTime ");
                                                                jsonObject.put("name", course.getCourseName());
                                                                if ("pass".equalsIgnoreCase(quizStatus.getStatus())) {
                                                                        jsonObject.put("pass", 1);
                                                                } else
                                                                        jsonObject.put("pass", 0);

                                                                if ("fail".equalsIgnoreCase(quizStatus.getStatus())) {
                                                                        jsonObject.put("fail", 1);
                                                                } else
                                                                        jsonObject.put("fail", 0);

                                                                passFailResponse.put(course.getKey(), jsonObject);
                                                        }

                                                }

                                        }

                                }
                                quizList.add(passFailResponse);

                                responseMap.put("report", quizList);
                        }
                }

                return responseMap;
        }
}
