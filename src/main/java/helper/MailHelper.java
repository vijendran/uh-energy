package helper;

import com.google.appengine.api.mail.MailService;
import com.google.appengine.api.mail.MailServiceFactory;

public class MailHelper {

    public static void sendMail(String from, String to, String subject, String content) throws Exception {

        MailService mailService = MailServiceFactory.getMailService();
        MailService.Message message = new MailService.Message();

        message.setSender(from);
        message.setTo(to);

        message.setSubject(subject);
        message.setTextBody(content);

        mailService.send(message);

        System.out.println("mail sent successfully");

    }


}
