package helper;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import dao.CourseDAO;
import dao.CourseStatusDAO;
import dao.LessonDAO;
import entity.Course;
import entity.CourseStatus;
import entity.Lesson;
import enums.ErrorResponse;
import org.apache.commons.lang3.StringEscapeUtils;
import query.CourseStatusQueryHelper;
import query.LessonQueryHelper;
import utility.JSONUtils;
import utility.PreConditions;
import utility.Validator;

import java.util.*;

public class LessonHelper {

    private static LessonHelper instance;

    private LessonHelper() {
    }

    public static synchronized LessonHelper instance() {
        if (instance == null)
            return new LessonHelper();
        return instance;
    }

    public static HashMap<String, Object> getLesson(String lessonID) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(lessonID),
                ErrorResponse.CommonErrorMsg.INVALID_LESSON_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        Lesson objLesson = LessonDAO.instance().get(lessonID);

        if (!Validator.isNullOrEmpty(objLesson.getKey())) {
            responseMap.put("lesson", objLesson);
        }

        return responseMap;
    }

    public static HashMap<String, Object> createLesson(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        String lessonName = payloadMap.containsKey("lessonName") ? (String) payloadMap.get("lessonName") : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(lessonName),
                ErrorResponse.CommonErrorMsg.INVALID_CONSTRAINTS.value());

        String courseID = payloadMap.containsKey("courseID") ? (String) payloadMap.get("courseID") : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

        Course course = CourseDAO.instance().get(courseID);

        PreConditions.checkArgument((Validator.isNull(course) || Validator.isNullOrEmpty(course.getKey())),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

        ArrayList<String> lessonImage = payloadMap.containsKey("lessonImage")
                ? (ArrayList<String>) payloadMap.get("lessonImage")
                : new ArrayList<>();

        ArrayList<String> lessonVideo = payloadMap.containsKey("lessonVideo")
                ? (ArrayList<String>) payloadMap.get("lessonVideo")
                : new ArrayList<>();

        String lessonString = payloadMap.containsKey("lessonText") ? (String) payloadMap.get("lessonText") : "";

        if (payloadMap.containsKey("lessonText")) {
            lessonString = StringEscapeUtils.unescapeHtml3(lessonString);
            System.out.println(lessonString);
        }

        Text lessonText = new Text(lessonString);

        System.out.println(lessonName + " lessonNAme " + lessonImage + " lessonImage " + lessonVideo
                + " lessonvideo " + lessonString + " lessonText ");

        HashMap<String, Object> responseMap = new HashMap<>();

        if (!Validator.isNullOrEmpty(lessonString) || !Validator.isEmptyList(lessonVideo)
                || !Validator.isEmptyList(lessonImage)) {

            Lesson objLesson = new Lesson();

            objLesson.setKey(String.valueOf(UUID.randomUUID()));
            objLesson.setDateAdded(new Date().getTime());
            objLesson.setLessonName(lessonName);
            objLesson.setLessonText(lessonText);
            objLesson.setLessonImage(lessonImage);
            objLesson.setLessonVideo(lessonVideo);
            objLesson.setcourseID(courseID);

            LessonDAO.instance().set(objLesson);

            responseMap.put("lesson", objLesson);
        }

        return responseMap;
    }

    public static HashMap<String, Object> updateLesson(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload),
                ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        String lessonID = payloadMap.containsKey("lessonID") ? String.valueOf(payloadMap.get("lessonID")) : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(lessonID),
                ErrorResponse.CommonErrorMsg.INVALID_LESSON_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        Lesson objLesson = LessonDAO.instance().get(lessonID);

        if (!Validator.isNull(objLesson.getKey())) {

            String lessonName = payloadMap.containsKey("lessonName")
                    && !Validator.isNullOrEmpty(String.valueOf(payloadMap.get("lessonName")))
                    ? String.valueOf(payloadMap.get("lessonName"))
                    : objLesson.getLessonName();
            ArrayList lessonImage = payloadMap.containsKey("lessonImage")
                    ? (ArrayList<String>) (payloadMap.get("lessonImage"))
                    : objLesson.getLessonImage();
            ArrayList lessonVideo = payloadMap.containsKey("lessonVideo")
                    ? (ArrayList<String>) (payloadMap.get("lessonVideo"))
                    : objLesson.getLessonVideo();
            String lessonString = "";// payloadMap.containsKey("lessonText")
            // ? String.valueOf(payloadMap.get("lessonText"))
            // : objLesson.getLessonText().getValue();

            if (payloadMap.containsKey("lessonText")) {
                lessonString = StringEscapeUtils
                        .unescapeHtml3(String.valueOf(payloadMap.get("lessonText")));
                System.out.println(lessonString);
            } else {
                lessonString = objLesson.getLessonText().getValue();
            }

            Text lessonText = new Text(lessonString);

            objLesson.setLessonName(lessonName);
            objLesson.setLessonText(lessonText);
            objLesson.setLessonImage(lessonImage);
            objLesson.setLessonVideo(lessonVideo);

            LessonDAO.instance().set(objLesson);

            responseMap.put("lesson", objLesson);
        }

        return responseMap;
    }

    public static HashMap<String, Object> getAllLessonForThisCourse(String courseID, String cursor,
                                                                    String contactID) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID),
                ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        HashMap<String, Object> lessonMap = LessonQueryHelper.getLessonsForThisCourseID(courseID, cursor);

        if (!Validator.isNullOrEmpty(lessonMap)) {

            List<Entity> lessonsEntityList = (List<Entity>) lessonMap.get("lessons");

            System.out.println(lessonMap + " lessonMap");

            if (!Validator.isEmptyList(lessonsEntityList)) {

                List<Lesson> lessonList = new ArrayList<>();

                for (Entity obj : lessonsEntityList) {
                    if (!Validator.isNull(obj)) {
                        Lesson objLesson = LessonDAO.instance()
                                .get(LessonDAO.instance().entityToObject(obj).getKey());
                        if (!Validator.isNull(objLesson.getKey())) {

                            CourseStatus objCouseStatus = CourseStatusDAO.instance()
                                    .get(contactID + objLesson.getKey());
                            if (Validator.isNullOrEmpty(objCouseStatus.getKey())) {
                                objLesson.setLessonStatus("");
                            } else {
                                objLesson.setLessonStatus("completed");
                            }
                            lessonList.add(objLesson);
                        }
                    }
                }

                responseMap.put("lesson", lessonList);
                if (lessonMap.containsKey("cursor")) {
                    responseMap.put("cursor", lessonMap.get("cursor"));
                }
            }
        }

        return responseMap;
    }

    public static HashMap<String, Object> getActiveLessonForTheUser(String courseID, String contactID) {

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID),
                ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID),
                ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        CourseStatus objCourseStatus = CourseStatusQueryHelper.getActiveLesson(courseID, contactID);

        if (!Validator.isNull(objCourseStatus)) {
            responseMap.put("lessonID", objCourseStatus.getLessonID());
            responseMap.put("lesson", objCourseStatus);
        }

        return responseMap;
    }

    public static HashMap<String, Object> deleteLesson(String lessonID) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(lessonID), ErrorResponse.CommonErrorMsg.INVALID_LESSON_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        LessonDAO.instance().delete(new Lesson(), lessonID);

        responseMap.put("deleted", true);
        return responseMap;
    }
}
