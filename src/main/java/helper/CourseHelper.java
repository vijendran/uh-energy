package helper;

import com.google.appengine.api.datastore.Entity;
import dao.CourseDAO;
import entity.Course;
import enums.ErrorResponse;
import query.CourseQueryHelper;
import utility.AppConstants;
import utility.JSONUtils;
import utility.PreConditions;
import utility.Validator;

import java.util.*;

public class CourseHelper {

    private static CourseHelper instance;

    private CourseHelper() {
    }

    public static synchronized CourseHelper instance() {
        if (instance == null) return new CourseHelper();
        return instance;
    }

    public static HashMap<String, Object> createCourse(String payload) throws Exception { // need to check about percentage alone

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        PreConditions.checkArgument(Validator.isNullOrEmpty(payloadMap), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        String courseName = payloadMap.containsKey("courseName") ? String.valueOf(payloadMap.get("courseName")) : "";
        int quizPercentage = payloadMap.containsKey("quizPercentage") && (Integer.parseInt(payloadMap.get("quizPercentage").toString()) > 0) && (Integer.parseInt(payloadMap.get("quizPercentage").toString()) <= 100) ? Integer.parseInt(payloadMap.get("quizPercentage").toString()) : AppConstants.DEFAULT_QUIZ_PERCENTAGE;
        String courseImage = payloadMap.containsKey("courseImage") && !Validator.isNullOrEmpty(String.valueOf(payloadMap.get("courseImage"))) ? String.valueOf(payloadMap.get("courseImage")) : AppConstants.DEFAULT_COURSE_IMAGE;

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseName), ErrorResponse.CourseMsg.INVALID_COURSE_NAME.value());

        // When createing if the user gives the percentage as zero should we store the default percentage
        // What will be the default percentage.

        HashMap<String, Object> responseMap = new HashMap<>();

        Course objCourse = new Course();

        objCourse.setKey(String.valueOf(UUID.randomUUID()));
        objCourse.setCourseName(courseName);
        objCourse.setCourseImage(courseImage);
        objCourse.setDateAdded(new Date().getTime());
        objCourse.setPercentage(quizPercentage);
        objCourse.setStatus(AppConstants.DRAFTED);

        CourseDAO.instance().set(objCourse);

        responseMap.put("course", objCourse);
        return responseMap;
    }

    public static HashMap<String, Object> getCourse(String courseID) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID), ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();
        Course objCourse = CourseDAO.instance().get(courseID);

        if (!Validator.isNull(objCourse.getKey())) {
            responseMap.put("course", objCourse);
        }

        return responseMap;
    }

    public static HashMap<String, Object> updateCourse(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        String courseID = payloadMap.containsKey("courseID") ? String.valueOf(payloadMap.get("courseID")) : "";

        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID), ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());

        HashMap<String, Object> responseMap = new HashMap<>();

        Course objCourse = CourseDAO.instance().get(courseID);

        if (!Validator.isNull(objCourse.getKey())) {

            String courseName = payloadMap.containsKey("courseName") && !Validator.isNullOrEmpty(String.valueOf(payloadMap.get("courseName"))) ? String.valueOf(payloadMap.get("courseName")) : objCourse.getCourseName();
            String courseImage = payloadMap.containsKey("courseImage") && !Validator.isNullOrEmpty(String.valueOf(payloadMap.get("courseImage"))) ? String.valueOf(payloadMap.get("courseImage")) : objCourse.getCourseImage();
            int quiz_percentage = payloadMap.containsKey("quizPercentage") && (Integer.parseInt(payloadMap.get("quizPercentage").toString()) > 0) && (Integer.parseInt(payloadMap.get("quizPercentage").toString()) <= 100) ? (Integer.parseInt(payloadMap.get("quizPercentage").toString())) : objCourse.getPercentage();
            String status = payloadMap.containsKey("status") && !Validator.isNullOrEmpty(String.valueOf(payloadMap.get("status"))) ? String.valueOf(payloadMap.get("status")) : objCourse.getStatus();

            objCourse.setCourseName(courseName);
            objCourse.setPercentage(quiz_percentage);
            objCourse.setCourseImage(courseImage);
            objCourse.setStatus(status);

            CourseDAO.instance().set(objCourse);

            responseMap.put("course", objCourse);
        }

        return responseMap;
    }

    public static HashMap<String, Object> getAllCourse(String cursor) throws Exception {

        HashMap<String, Object> responseMap = new HashMap<>();

        HashMap<String, Object> courseMap = CourseQueryHelper.instance().getAllCourse(cursor);

        if (!Validator.isNullOrEmpty(courseMap)) {

            List<Entity> courseMapList = (List<Entity>) courseMap.get("courses");
            System.out.println(courseMap + " courseMap");
            if (!Validator.isEmptyList(courseMapList)) {

                List<Course> courseList = new ArrayList<>();

                for (Entity obj : courseMapList) {
                    if (!Validator.isNull(obj)) {
                        Course objCourse = CourseDAO.instance().get(CourseDAO.instance().entityToObject(obj).getKey());
                        if (!Validator.isNullOrEmpty(objCourse.getKey())) {
                            courseList.add(objCourse);
                        }
                    }
                }

                responseMap.put("course", courseList);
                if (courseMap.containsKey("cursor")) {
                    responseMap.put("cursor", courseMap.get("cursor"));
                }
            }
        }

        return responseMap;
    }

}
