package helper.upload;

import com.google.appengine.api.blobstore.*;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

//import org.omg.CORBA_2_3.portable.OutputStream;

public class UploadHelper {

    private static final String LINE_FEED = "\r\n";

    private final String boundary = "314159265358979323846";
    private final OutputStream outputStream = null;
    private HttpURLConnection httpConn = null;
    private PrintWriter writer = null;

    private static int bufferSize = 1015808;

    private static final String GCS_HOST = "https://storage.googleapis.com/";

    private static final String BUCKET_NAME = "gajendran-mohan.appspot.com";

    private static final Logger log = Logger.getLogger(UploadHelper.class.getSimpleName());

    private static UploadHelper instance;

    private UploadHelper() {
    }

    public static synchronized UploadHelper instance() {
        if (instance == null) return new UploadHelper();
        return instance;
    }

    private final GcsService gcsService =
            GcsServiceFactory.createGcsService(
                    new RetryParams.Builder().retryMinAttempts(0).retryMaxAttempts(1).build());

    private static String jsonPath = "";
    private BlobstoreService blobStoreService = BlobstoreServiceFactory.getBlobstoreService();

    public HashMap<String, Object> upload(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HashMap<String, Object> responseData = new HashMap<>();
        byte[] bytes;

        // get the file details from request
        List<BlobKey> blobKeys = blobStoreService.getUploads(request).get("files");
        log.info("blobKeys :: " + blobKeys.get(0));
        BlobKey blobKey =
                new BlobKey(
                        blobKeys.get(0).getKeyString().replace("<BlobKey: ", "").replace(">", "").trim());

        // getting the file info so that i can get the blob key
        Map<String, List<FileInfo>> uploads = blobStoreService.getFileInfos(request);
        List<FileInfo> fileInfos = uploads.get("files");
        FileInfo fileInfo = fileInfos.get(0);

        log.info("size from blob load :: " + " from the file info" + fileInfo.getSize());

        // read the file data from file

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(new BlobstoreInputStream(blobKey)));

        bytes = GCSUpload.instance().readImageData(blobKey, fileInfo.getSize());

        ClassLoader classLoader = getClass().getClassLoader();

        ServiceAccountCredentials serviceAccountCredentials;

        serviceAccountCredentials =
                ServiceAccountCredentials.fromStream(new FileInputStream("servicelogin.json"));

        Storage storage =
                StorageOptions.newBuilder().setCredentials(serviceAccountCredentials).build().getService();

        BlobId blobId = BlobId.of(BUCKET_NAME, fileInfo.getFilename());

        BlobInfo blobInfo =
                BlobInfo.newBuilder(blobId).setContentType(fileInfo.getContentType()).build();

        log.info("fileInfo" + fileInfo.getFilename());

        Map<String, List<com.google.appengine.api.blobstore.BlobInfo>> blobsData =
                blobStoreService.getBlobInfos(request);
        for (String key : blobsData.keySet()) {
            for (com.google.appengine.api.blobstore.BlobInfo blobObject : blobsData.get(key)) {

                log.info("blobObject :: " + blobObject);
            }
        }

        Blob blob =
                storage.create(
                        blobInfo,
                        bytes,
                        Storage.BlobTargetOption.predefinedAcl(Storage.PredefinedAcl.PUBLIC_READ));

        log.info("self link :: " + blob.getSelfLink());
        log.info("media link :: " + blob.getMediaLink());
        log.info("media link :: " + sizeConverter(blob.getSize()));

        responseData.put("size", sizeConverter(blob.getSize()));

        responseData.put("url", getGcsServingUrl(fileInfo.getFilename()));
        responseData.put("name", fileInfo.getFilename());

        blobStoreService.delete(blobKey);

        log.info("blobKey :: " + blobKey);

        return responseData;
    }

    public static String streamToString(InputStream is) throws IOException {
        if (is == null) return null;

        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        return sb.toString();
    }

    public static InputStream getInputStream(HttpURLConnection conn) throws IOException {

        InputStream stream = null;
        try {
            stream = conn.getInputStream();
        } catch (IOException e) {
            if (conn.getResponseCode() != 200) {
                stream = conn.getErrorStream();
            }
        }
        return stream;
    }

    private void writeToOutputStream(InputStream input, int bufferSize) throws IOException {

        byte[] buffer = new byte[bufferSize < 0 ? 10 * 1024 : bufferSize];
        int read = -1;

        while ((read = input.read(buffer)) != -1) {
            outputStream.write(buffer, 0, read);
        }

        outputStream.flush();
        input.close();
    }

    private long sizeConverter(long size) {
        if (size < 1024) {
            return size + 'B';
        } else if (size < 1048576) {
            return (Math.round((size * 10) / 1024) / 10) + 'K';
        } else if (size < 1073741824) {
            return (Math.round((size * 10) / 1048576) / 10) + 'M';
        } else {
            return (Math.round((size * 10) / 1073741824) / 10) + 'G';
        }
    }

    private String getGcsServingUrl(String name) {
        if (name == null) {
            throw new IllegalArgumentException("File was not uploaded to GCS");
        }
        // substring(4) strips "/gs/" prefix
        return GCS_HOST + BUCKET_NAME + "/" + name;
    }
}
