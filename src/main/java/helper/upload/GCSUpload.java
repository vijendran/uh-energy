package helper.upload;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.tools.cloudstorage.*;
import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class GCSUpload {

  private GCSUpload() {}

  private static GCSUpload gcs = null;

  public static synchronized GCSUpload instance() {
    if (gcs == null) gcs = new GCSUpload();

    return gcs;
  }

  private BlobstoreService blobStoreService = BlobstoreServiceFactory.getBlobstoreService();

  @Override
  public Object clone() throws CloneNotSupportedException {
    throw new CloneNotSupportedException();
  }

  private final GcsService gcsService =
      GcsServiceFactory.createGcsService(
          new RetryParams.Builder().retryMinAttempts(0).retryMaxAttempts(1).build());

  public String uploadURL(String url, String bucket) {

    if (bucket == null) return BlobstoreServiceFactory.getBlobstoreService().createUploadUrl(url);
    else
      return BlobstoreServiceFactory.getBlobstoreService()
          .createUploadUrl(url, UploadOptions.Builder.withGoogleStorageBucketName(bucket).maxUploadSizeBytes(1024000000L)); //
  }

  void upload(String bucketName, String objectName, String mime, String name, byte[] content)
      throws Exception {
    GcsOutputChannel oc =
        gcsService.createOrReplace(
            new GcsFilename(bucketName, objectName),
            new GcsFileOptions.Builder().acl("public-read").mimeType(mime).build());
    oc.write(ByteBuffer.wrap(content));
    oc.close();
  }

  public void delete(String bucketName, String objectName, String blobKey) {
    try {
      gcsService.delete(new GcsFilename(bucketName, objectName));

      if (blobKey != null) {
        blobStoreService.delete(
            new BlobKey(blobKey.replace("<BlobKey: ", "").replace(">", "").trim()));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  byte[] writeToOutputStream(InputStream input, int bufferSize) throws IOException {

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    byte[] buffer = new byte[bufferSize < 0 ? 10 * 1024 : bufferSize];
    int read = -1;

    while ((read = input.read(buffer)) != -1) {
      outputStream.write(buffer, 0, read);
    }

    outputStream.flush();
    input.close();
    return outputStream.toByteArray();
  }

  public byte[] getByteArray(InputStream input, int bufferSize) throws IOException {

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    byte[] buffer = new byte[bufferSize < 0 ? 10 * 1024 : bufferSize];
    int read = -1;

    while ((read = input.read(buffer)) != -1) {
      outputStream.write(buffer, 0, read);
    }

    outputStream.flush();
    input.close();
    return outputStream.toByteArray();
  }

  public byte[] readDataUsingBlobKey(BlobKey blobKey, long blobSize) {
    byte[] bytes = new byte[0], chunks = null;

    long blobIndex = 0;

    boolean proceed = true;

    do {

      chunks = blobStoreService.fetchData(blobKey, blobIndex, blobIndex + 1015807);

      bytes = ArrayUtils.addAll(bytes, chunks);

      blobSize = blobSize - 1015808;

      blobIndex = blobIndex + 1015808;

      if (blobSize < 0) proceed = false;

    } while (proceed);

    return bytes;
  }

  public byte[] readImageData(BlobKey blobKey, long blobSize) {
    BlobstoreService blobStoreService = BlobstoreServiceFactory.getBlobstoreService();
    byte[] allTheBytes = new byte[0];
    long amountLeftToRead = blobSize;
    long startIndex = 0;
    while (amountLeftToRead > 0) {
        long amountToReadNow = Math.min(BlobstoreService.MAX_BLOB_FETCH_SIZE - 1, amountLeftToRead);
        byte[] chunkOfBytes = blobStoreService.fetchData(blobKey, startIndex, startIndex + amountToReadNow - 1);
        allTheBytes = ArrayUtils.addAll(allTheBytes, chunkOfBytes);
        amountLeftToRead -= amountToReadNow;
        startIndex += amountToReadNow;
    }
    return allTheBytes;
}
}
