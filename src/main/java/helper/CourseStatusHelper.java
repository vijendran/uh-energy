package helper;

import dao.CourseStatusDAO;
import entity.CourseStatus;
import enums.ErrorResponse;
import utility.JSONUtils;
import utility.PreConditions;
import utility.Validator;

import java.util.Date;
import java.util.HashMap;

public class CourseStatusHelper {

    private static CourseStatusHelper instance;

    private CourseStatusHelper() {
    }

    public static synchronized CourseStatusHelper instance() {
        if (instance == null) return new CourseStatusHelper();
        return instance;
    }

    public static HashMap<String, Object> markAsCompleted(String payload) throws Exception {

        PreConditions.checkArgument(Validator.isNullOrEmpty(payload), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        HashMap<String, Object> payloadMap = (HashMap<String, Object>) JSONUtils.convertJsonToMap(payload);

        PreConditions.checkArgument(Validator.isNullOrEmpty(payloadMap), ErrorResponse.CommonErrorMsg.INVALID_PAYLOAD.value());

        String courseID = payloadMap.containsKey("courseID") ? (String) payloadMap.get("courseID") : "";
        String lessonID = payloadMap.containsKey("lessonID") ? (String) payloadMap.get("lessonID") : "";
        String contactID = payloadMap.containsKey("contactID") ? (String) payloadMap.get("contactID") : "";


        PreConditions.checkArgument(Validator.isNullOrEmpty(courseID), ErrorResponse.CommonErrorMsg.INVALID_COURSE_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(lessonID), ErrorResponse.CommonErrorMsg.INVALID_LESSON_ID.value());
        PreConditions.checkArgument(Validator.isNullOrEmpty(contactID), ErrorResponse.CommonErrorMsg.INVALID_CONTACT_ID.value());


        HashMap<String, Object> responseMap = new HashMap<>();

        CourseStatus existingCourseStatus = CourseStatusDAO.instance().get(contactID + lessonID);

        if (Validator.isNull(existingCourseStatus) || Validator.isNullOrEmpty(existingCourseStatus.getKey())) {

            CourseStatus objCourseStatus = new CourseStatus();

            objCourseStatus.setKey(contactID + lessonID);
            objCourseStatus.setCourseID(courseID);
            objCourseStatus.setLessonID(lessonID);
            objCourseStatus.setContactID(contactID);
            objCourseStatus.setDateCompleted(new Date().getTime());

            CourseStatusDAO.instance().set(objCourseStatus);

            responseMap.put("courseStatus", objCourseStatus);
        } else {
            responseMap.put("courseStatus", existingCourseStatus);
        }

        return responseMap;
    }
}
