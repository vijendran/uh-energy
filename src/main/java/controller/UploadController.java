package controller;

import enums.ErrorResponse;
import helper.upload.GCSUpload;
import helper.upload.UploadHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import utility.GenericResponse;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Controller
public class UploadController {

  @RequestMapping(value = "/api/uploadUrl", method = RequestMethod.GET)
  @ResponseBody
  public static GenericResponse uploadURI() {

    GenericResponse response = new GenericResponse();
    try {

      HashMap<String, Object> uploadData = new HashMap<>();

      String uploadURL =
          GCSUpload.instance().uploadURL("/upload/lesson", "gajendran-mohan.appspot.com");

      if (!Validator.isNullOrEmpty(uploadURL)) {

        uploadData.put("uploadURL", uploadURL);

        response = GenericResponse.constructResponse(true, "", "", uploadData);

      } else {
        response =
            GenericResponse.constructResponse(
                false, "200", ErrorResponse.CommonErrorMsg.INVALID_CONSTRAINTS.value(), "");
      }

    } catch (Exception e) {
      response = GenericResponse.constructResponse(false, null, e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/upload/lesson", method = RequestMethod.POST)
  @ResponseBody
  public static GenericResponse upload(HttpServletRequest request, HttpServletResponse response) {

    GenericResponse output = new GenericResponse();
    try {

      HashMap<String, Object> uploader = UploadHelper.instance().upload(request, response);

      if (!Validator.isNullOrEmpty(uploader)) {
        return GenericResponse.constructResponse(true, null, "", uploader);
      }

    } catch (Exception e) {
      e.printStackTrace();
      output = GenericResponse.constructResponse(false, null, e.getMessage(), "");
    }

    return output;
  }
}
