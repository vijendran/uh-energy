package controller;

import enums.ErrorResponse;
import helper.LessonHelper;
import helper.session.CurrentSession;
import helper.session.SessionHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import utility.GenericResponse;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class LessonController {

    @RequestMapping(value = "/api/lesson", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getLesson(
            @RequestParam(value = "lessonID", required = false) String lessonID) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> lessonResponse = LessonHelper.getLesson(lessonID);

            if (!Validator.isNullOrEmpty(lessonResponse)) {

                response = GenericResponse.constructResponse(true, "", "", lessonResponse);

            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/lesson", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse createLesson(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> lessonResponse = LessonHelper.instance().createLesson(payload);

            if (!Validator.isNullOrEmpty(lessonResponse)) {

                response = GenericResponse.constructResponse(true, "", "", lessonResponse);

            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/lesson", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updateLesson(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> lessonResponse = LessonHelper.instance().updateLesson(payload);

            if (!Validator.isNullOrEmpty(lessonResponse)) {
                response = GenericResponse.constructResponse(true, "", "", lessonResponse);
            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/course/lesson", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getAllLessonForThisCourseID(
            @RequestParam(value = "courseID") String courseID, @RequestParam(value = "cursor", required = false) String cursor, HttpServletRequest request) {

        GenericResponse response = new GenericResponse();
        try {

            CurrentSession currentSession = SessionHelper.getCurrentUserFromSession(request);

            String contactID = "";

            if (!Validator.isNull(currentSession)) {
                contactID = currentSession.getUserID();
            }
            System.out.println(courseID + " courseID " + " cursor " + cursor + " contactID " + contactID);
            HashMap<String, Object> lessonResponse = LessonHelper.getAllLessonForThisCourse(courseID, cursor, contactID);

            if (!Validator.isNullOrEmpty(lessonResponse)) {

                response = GenericResponse.constructResponse(true, "", "", lessonResponse);

            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    /**
     * This method was written in order to fetch the active lesson for the user but it is reused in different way this can be omitted.
     *
     * @param courseID
     * @param contactID
     * @return
     */
    @RequestMapping(value = "/api/lesson/active", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getActiveLessonForTheUser(
            @RequestParam(value = "courseID") String courseID, @RequestParam(value = "contactID", required = true) String contactID) {

        GenericResponse response = new GenericResponse();
        try {


            HashMap<String, Object> lessonResponse = LessonHelper.getActiveLessonForTheUser(courseID, contactID);

            if (!Validator.isNullOrEmpty(lessonResponse)) {

                response = GenericResponse.constructResponse(true, "", "", lessonResponse);

            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/lesson/{lessonID}", method = RequestMethod.DELETE)
    @ResponseBody
    public static GenericResponse deleteLesson(
            @PathVariable("lessonID") String lessonID) {

        GenericResponse response = new GenericResponse();
        try {


            HashMap<String, Object> lessonResponse = LessonHelper.deleteLesson(lessonID);

            if (!Validator.isNullOrEmpty(lessonResponse)) {

                response = GenericResponse.constructResponse(true, "", "", lessonResponse);

            } else {
                response =
                        GenericResponse.constructResponse(
                                false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

}
