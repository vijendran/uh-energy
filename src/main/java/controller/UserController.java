package controller;

import enums.ErrorResponse;
import helper.UserHelper;
import helper.session.CurrentSession;
import helper.session.SessionHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import utility.GenericResponse;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Controller
public class UserController {

    @RequestMapping(value = "/api/user", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse saveAccount(@RequestBody String payload, HttpServletRequest request,
                                              HttpServletResponse response) {

        GenericResponse userResp = new GenericResponse();
        try {

            if (SessionHelper.hasUserSession(request)) {
                // redirect to home

            } else {
                HashMap<String, Object> userData = UserHelper.instance().user(payload, request, response);

                if (!Validator.isNull(userData)) {

                    userResp = GenericResponse.constructResponse(true, "", "", userData);

                } else {
                    userResp = GenericResponse.constructResponse(false, "200",
                            ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
                }
            }

        } catch (Exception e) {
            userResp = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }

    @RequestMapping(value = "/api/authenticate", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse authenticateUser(HttpServletRequest request, HttpServletResponse response,
                                                   @RequestBody String payload) {

        GenericResponse userResp = new GenericResponse();

        try {

            if (SessionHelper.hasUserSession(request)) {
                // /dashboard
            } else {

                HashMap<String, Object> responseMap = UserHelper.authenticateUser(request, response, payload);
                if (!Validator.isNullOrEmpty(responseMap)) {
                    userResp = GenericResponse.constructResponse(true, "", "", responseMap);

                } else {
                    userResp = GenericResponse.constructResponse(false, "200",
                            ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
                }
            }
        } catch (Exception e) {
            GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }

    @RequestMapping(value = "/api/user/name", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updateUser(HttpServletRequest request, HttpServletResponse response,
                                             @RequestBody String payload) {

        GenericResponse userResp = new GenericResponse();

        try {

            CurrentSession session = SessionHelper.getCurrentUserFromSession(request);

            HashMap<String, Object> responseMap = UserHelper.updateUserName(payload, session.getUserID());
            if (!Validator.isNullOrEmpty(responseMap)) {
                userResp = GenericResponse.constructResponse(true, "", "", responseMap);

            } else {
                userResp = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
            }
        } catch (Exception e) {
            GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }

    @RequestMapping(value = "/api/user/password", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updatePassword(HttpServletRequest request, HttpServletResponse response,
                                                 @RequestBody String payload) {

        GenericResponse userResp = new GenericResponse();

        try {

            HashMap<String, Object> responseMap = UserHelper.updatePassword(payload);
            if (!Validator.isNullOrEmpty(responseMap)) {
                userResp = GenericResponse.constructResponse(true, "", "", responseMap);

            } else {
                userResp = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
            }
        } catch (Exception e) {
            GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }

    @RequestMapping(value = "/api/user/role", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updateRole(HttpServletRequest request, HttpServletResponse response,
                                             @RequestBody String payload) {

        GenericResponse userResp = new GenericResponse();

        try {
            // CurrentSession session = SessionHelper.getCurrentUserFromSession(request);

            HashMap<String, Object> responseMap = UserHelper.updateRole(payload, "");
            if (!Validator.isNullOrEmpty(responseMap)) {
                userResp = GenericResponse.constructResponse(true, "", "", responseMap);

            } else {
                userResp = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
            }
        } catch (Exception e) {
            GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }

    @RequestMapping(value = "/api/user/admin", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse createAdmin(@RequestBody String payload) {

        GenericResponse userResp = new GenericResponse();
        try {


            HashMap<String, Object> userData = UserHelper.instance().createAdmin(payload);

            if (!Validator.isNull(userData)) {

                userResp = GenericResponse.constructResponse(true, "", "", userData);

            } else {
                userResp = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.USER_NOT_FOUND.value(), "");
            }


        } catch (Exception e) {
            userResp = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return userResp;
    }
}
