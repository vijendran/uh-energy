package controller;

import enums.ErrorResponse;
import helper.CourseHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import utility.GenericResponse;
import utility.Validator;

import java.util.HashMap;

@Controller
public class CourseController {

    @RequestMapping(value = "/api/course", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse createCourse(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseHelper.instance().createCourse(payload);

            if (!Validator.isNullOrEmpty(courseResponse)) {

                response = GenericResponse.constructResponse(true, "", "", courseResponse);
//                MailHelper.sendMail();
            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "")
                ;

            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/course", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getCourse(@RequestParam(value = "courseID", required = true) String courseID) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseHelper.instance().getCourse(courseID);

            if (!Validator.isNullOrEmpty(courseResponse)) {

                response = GenericResponse.constructResponse(true, "", "", courseResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.NO_COURSE_FOUND.value(), "");

            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/course", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updateCourse(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseHelper.instance().updateCourse(payload);

            if (!Validator.isNullOrEmpty(courseResponse)) {
                response = GenericResponse.constructResponse(true, "", "", courseResponse);
            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.NO_COURSE_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/course/all", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getAllCourse(@RequestParam(value = "cursor", required = false) String cursor) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseHelper.instance().getAllCourse(cursor);

            if (!Validator.isNullOrEmpty(courseResponse)) {

                response = GenericResponse.constructResponse(true, "", "", courseResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.NO_COURSE_FOUND.value(), "");

            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/course/status", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updateCourseStatus(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseHelper.instance().updateCourse(payload);

            if (!Validator.isNullOrEmpty(courseResponse)) {
                response = GenericResponse.constructResponse(true, "", "", courseResponse);
            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.NO_COURSE_FOUND.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }
}
