package controller;

import enums.ErrorResponse;
import helper.CourseStatusHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import utility.GenericResponse;
import utility.Validator;

import java.util.HashMap;

@Controller
public class CourseStatusController {

    @RequestMapping(value = "/api/course/status", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse markAsComplete(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> courseResponse = CourseStatusHelper.markAsCompleted(payload);

            if (!Validator.isNullOrEmpty(courseResponse)) {

                response = GenericResponse.constructResponse(true, "", "", courseResponse);
//                MailHelper.sendMail();
            } else {
                response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "")
                ;

            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

}
