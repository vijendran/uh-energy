package controller;

import enums.ErrorResponse;
import helper.SubscriptionHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import query.CourseStatusQueryHelper;
import utility.GenericResponse;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class SubscriptionController {

    @RequestMapping(value = "/api/user/subscription/status", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getPendingSubscription(
            @RequestParam(value = "cursor", required = false) String cursor) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> subscriptionResponse = SubscriptionHelper.instance().getPendingSubscription(cursor);

            if (!Validator.isNull(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.SubscriptionErrorMsg.NO_SUBSCRIBED_USERS.value(), "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/user/subscription/status", method = RequestMethod.PUT)
    @ResponseBody
    public static GenericResponse updatePaymentStatus(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> paymentStatusResponse = SubscriptionHelper.instance().updatePaymentStatus(payload);

            if (!Validator.isNullOrEmpty(paymentStatusResponse)) {

                response = GenericResponse.constructResponse(true, "", "", paymentStatusResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.SubscriptionErrorMsg.SUBSCRIPTION_FAILED.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/user/quiz/status", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getQuizStatu(@RequestParam(value = "cursor", required = false) String cursor) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> subscriptionResponse = CourseStatusQueryHelper.instance().getQuizStatus(cursor);

            System.out.println(subscriptionResponse);

            if (!Validator.isNull(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

                System.out.println("after response " + response.getData());

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.SubscriptionErrorMsg.NO_SUBSCRIBED_USERS.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/user/course", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getUserExistenceInCourse(@RequestParam(value = "courseID", required = true) String courseID, @RequestParam(value = "contactID", required = true) String contactID) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> subscriptionResponse = SubscriptionHelper.getUserExistence(courseID, contactID);

            System.out.println(subscriptionResponse);

            if (!Validator.isNullOrEmpty(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

                System.out.println("after response " + response.getData());

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.SubscriptionErrorMsg.NO_SUBSCRIBED_USERS.value(), "");
            }

        } catch (Exception e) {
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/user/enroll", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse enrollCourse(@RequestBody String payload, HttpServletRequest request) {

        GenericResponse response = new GenericResponse();
        try {


            HashMap<String, Object> subscriptionResponse = SubscriptionHelper.enrollUser(payload);

            System.out.println(subscriptionResponse);

            if (!Validator.isNullOrEmpty(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

                System.out.println("after response " + response.getData());

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.SubscriptionErrorMsg.NO_SUBSCRIBED_USERS.value(), "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }
}
