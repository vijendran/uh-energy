package controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;
@Controller
public class Hello {
  private static final Logger log = Logger.getLogger(Hello.class.getSimpleName());
  @RequestMapping(value = "/view/{page}/{subPage}", method = RequestMethod.GET)
  public ModelAndView update(
      @PathVariable(value = "page") String page, @PathVariable(value = "subPage") String subPage, HttpServletRequest request) {
    return new ModelAndView(page+"/"+subPage);
  }
}
