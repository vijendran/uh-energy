package controller;

import enums.ErrorResponse;
import helper.QuizHelper;
import helper.session.CurrentSession;
import helper.session.SessionHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import dao.QuizDAO;
import entity.Quiz;
import utility.GenericResponse;
import utility.PreConditions;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class QuizController {

  @RequestMapping(value = "/api/quiz", method = RequestMethod.POST)
  @ResponseBody
  public static GenericResponse createQuizz(@RequestBody String payload) {

    GenericResponse response = new GenericResponse();
    try {

      HashMap<String, Object> quizResponse = QuizHelper.instance().createQuizz(payload);

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      e.printStackTrace();
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz", method = RequestMethod.GET)
  @ResponseBody
  public static GenericResponse quiz(@RequestParam(value = "courseID") String courseID,
      @RequestParam(value = "cursor", required = false) String cursor) {

    GenericResponse response = new GenericResponse();
    try {

      HashMap<String, Object> quizResponse = QuizHelper.instance().getQuiz(courseID, cursor);

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      e.printStackTrace();
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz/each", method = RequestMethod.GET)
  @ResponseBody
  public static GenericResponse quiz(@RequestParam(value = "quizID") String quizID) {

    GenericResponse response = new GenericResponse();
    try {

      Quiz quiz = QuizDAO.instance().get(quizID);
      HashMap<String, Object> quizResponse = new HashMap<>();

      quizResponse.put("quiz", quiz);

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      e.printStackTrace();
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz/validate", method = RequestMethod.POST)
  @ResponseBody
  public static GenericResponse validateQuizAnswer(@RequestBody String payload) {

    GenericResponse response = new GenericResponse();
    try {

      HashMap<String, Object> quizResponse = QuizHelper.instance().validateQuiz(payload);

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz/status", method = RequestMethod.POST)
  @ResponseBody
  public static GenericResponse validateQuizStatus(@RequestBody String payload, HttpServletRequest request) {

    GenericResponse response = new GenericResponse();
    try {

      CurrentSession currentSession = SessionHelper.getCurrentUserFromSession(request);

      PreConditions.checkArgument(Validator.isNullOrEmpty(currentSession.getUserID()), "Invalid User");

      HashMap<String, Object> quizResponse = QuizHelper.instance().validateQuizStatus(payload,
          currentSession.getUserID());

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      e.printStackTrace();
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz/status", method = RequestMethod.GET)
  @ResponseBody
  public static GenericResponse getQuizStatus(@RequestParam(value = "courseID") String courseID,
      HttpServletRequest request) {

    GenericResponse response = new GenericResponse();
    try {

      CurrentSession currentSession = SessionHelper.getCurrentUserFromSession(request);

      PreConditions.checkArgument(Validator.isNullOrEmpty(currentSession.getUserID()), "Invalid User");

      HashMap<String, Object> quizResponse = QuizHelper.instance().getQuizStatus(courseID, currentSession.getUserID());

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);
      } else {
        response = GenericResponse.constructResponse(false, "200",
            ErrorResponse.CommonErrorMsg.UNABLE_TO_CREATE.value(), "");
      }

    } catch (Exception e) {
      e.printStackTrace();
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }

  @RequestMapping(value = "/api/quiz/{quizID}", method = RequestMethod.DELETE)
  @ResponseBody
  public static GenericResponse deleteLesson(@PathVariable("quizID") String quizID) {

    GenericResponse response = new GenericResponse();
    try {

      HashMap<String, Object> quizResponse = new HashMap<>();

      QuizDAO.instance().delete(new Quiz(), quizID);

      if (!Validator.isNullOrEmpty(quizResponse)) {

        response = GenericResponse.constructResponse(true, "", "", quizResponse);

      } else {
        response = GenericResponse.constructResponse(false, "200", ErrorResponse.CommonErrorMsg.NO_LESSON_FOUND.value(),
            "");
      }

    } catch (Exception e) {
      response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
    }

    return response;
  }
}
