package controller;

import dao.UserDAO;
import entity.User;
import helper.session.CurrentSession;
import helper.session.SessionHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import utility.AppConstants;
import utility.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.logging.Logger;

@Controller
public class FrontController {

    private static FrontController instance;

    private static final Logger log = Logger.getLogger(FrontController.class.getSimpleName());

    private FrontController() {
    }

    public static synchronized FrontController instance() {
        if (instance == null)
            return new FrontController();

        return instance;
    }

    @RequestMapping(value = { "/", "/{page}", "/{page}/{path}" }, method = RequestMethod.GET)
    public ModelAndView loadPage(@PathVariable(value = "page", required = false) String page,
            @PathVariable(value = "path", required = false) String path, HttpServletRequest request,
            HttpServletResponse response, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        try {

            if (SessionHelper.hasUserSession(request)) {

                CurrentSession currentSession = SessionHelper.getCurrentUserFromSession(request);

                if (Validator.isNull(currentSession)) {
                    return new ModelAndView("authenticate/login");
                }

                if (!Validator.isNullOrEmpty(currentSession.getUserID())) {

                    User user = UserDAO.instance().get(currentSession.getUserID());

                    String returnPage = "course";
                    if (!Validator.isNull(user) && !Validator.isNullOrEmpty(user.getKey())) {

                        modelAndView = new ModelAndView("users/index");
                        if ("quiz".equalsIgnoreCase(page)) {
                            returnPage = "quiz";
                        } else if ("course".equalsIgnoreCase(page)) {
                            if (!Validator.isNullOrEmpty(path)) {
                                returnPage = "lesson";
                            }
                        } else if ("admin".equalsIgnoreCase(page)) {
                            returnPage = "subscription";
                        } else if ("reset-password".equalsIgnoreCase(page)) {
                            modelAndView = new ModelAndView("authenticate/reset-password");
                            returnPage = "reset-password";
                        }

                        modelAndView.addObject("version", AppConstants.version);
                        modelAndView.addObject("page", returnPage);
                        modelAndView.addObject("contactID", currentSession.getUserID());
                        modelAndView.addObject("user", Validator.getJson(currentSession));

                    } else {
                        new ModelAndView("redirect:/logout");
                    }

                } else {
                    new ModelAndView("redirect:/logout");
                }

                log.info("user index");

                return modelAndView;

            } else if ("signup".equalsIgnoreCase(page)) {

                modelAndView = new ModelAndView("authenticate/signup");

                modelAndView.addObject("version", AppConstants.version);

                return modelAndView;

            } else if ("reset-password".equalsIgnoreCase(page)) {
                modelAndView = new ModelAndView("authenticate/reset-password");

                modelAndView.addObject("version", AppConstants.version);

                return modelAndView;
            } else {

                modelAndView = new ModelAndView("authenticate/login");

                modelAndView.addObject("version", AppConstants.version);

                return modelAndView;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ModelAndView("authenticate/login");
    }

    private String validPage(String page) {

        try {

            if (!Validator.isNullOrEmpty(page)) {

                if ("login".equalsIgnoreCase(page))
                    return "authenticate/login";
                if ("signup".equalsIgnoreCase(page))
                    return "authenticate/signup";
                if ("lesson".equalsIgnoreCase(page))
                    return "users/index";

            } else
                return "";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.removeAttribute("energy_user");
            SessionHelper.invalidateSession(request);
            return "redirect:" + "https://gajendran-mohan.el.r.appspot.com/login";

        } catch (Exception e) {
        }
        return "https://gajendran-mohan.el.r.appspot.com/login";
    }
}
