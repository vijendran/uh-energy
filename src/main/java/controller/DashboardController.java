package controller;

import enums.ErrorResponse;
import helper.DashboardHelper;
import helper.QuizHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import utility.GenericResponse;
import utility.Validator;

import java.util.HashMap;

@Controller
public class DashboardController {

    @RequestMapping(value = "/api/graph", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse getPendingSubscription() {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> subscriptionResponse = DashboardHelper.generateGraphData();

            if (!Validator.isNull(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.NO_DATA_FOUND.value(), "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/graph/quizStatus", method = RequestMethod.GET)
    @ResponseBody
    public static GenericResponse geetDummyData(@RequestParam(value = "courseID", required = false) String courseID) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> subscriptionResponse = QuizHelper.instance().getQuizStatusForAllUser(courseID, "");

            if (!Validator.isNull(subscriptionResponse)) {

                response = GenericResponse.constructResponse(true, "", "", subscriptionResponse);

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.NO_DATA_FOUND.value(), "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }

    @RequestMapping(value = "/api/updatepassword/mail", method = RequestMethod.POST)
    @ResponseBody
    public static GenericResponse updatePasswordMail(@RequestBody String payload) {

        GenericResponse response = new GenericResponse();
        try {

            HashMap<String, Object> responseMap = DashboardHelper.sendUpdatePasswordMail(payload);

            if (!Validator.isNull(responseMap)) {

                response = GenericResponse.constructResponse(true, "", "", responseMap);

            } else {
                response = GenericResponse.constructResponse(false, "200",
                        ErrorResponse.CommonErrorMsg.NO_DATA_FOUND.value(), "");
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = GenericResponse.constructResponse(false, "400", e.getMessage(), "");
        }

        return response;
    }
}
