 package dao;

 import entity.User;

 import java.util.Collection;
 import java.util.LinkedHashMap;
 import java.util.List;

 public class UserDAO extends DAO<User>{

     private UserDAO() {
         super(User.class);
     }

     private static UserDAO instance;

     public static synchronized UserDAO instance() {
         if (instance == null) instance = new UserDAO();

         return instance;
     }

     public User get(String key) throws Exception {
         User user = super.get(key);

         return user;
     }

     public List<User> get(Collection<String> keys) throws Exception {
         List<User> user = super.get(keys);

         return user;
     }

     public void set(User user) throws Exception {
         super.set(user, user.getKey());
     }

     public void set(Collection<User> users) throws Exception {
         LinkedHashMap<String, User> userMap = new LinkedHashMap<String, User>();

         for (User u : users) {
             userMap.put(u.getKey(), u);
         }
         super.set(userMap);
     }

 }
