 package dao;

 import com.google.appengine.api.datastore.*;
 import org.apache.commons.beanutils.PropertyUtils;
 import org.springframework.beans.PropertyAccessor;

 import org.springframework.beans.PropertyAccessorFactory;
 import org.springframework.util.SerializationUtils;
 import utility.Validator;

 import java.beans.PropertyDescriptor;
 import java.io.Serializable;
 import java.lang.annotation.Annotation;
 import java.util.*;
 import java.util.Map.Entry;

 public class DAO<T extends Serializable> extends Memcache {
     private Class<T> _class = null;

     public DAO(Class<T> _class) {
         this._class = _class;
     }

     @Override
     public Object clone() throws CloneNotSupportedException {
         throw new CloneNotSupportedException();
     }

     public static DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

     public static void setSystemProperty() {
         System.setProperty(
                 DatastoreServiceConfig.DATASTORE_EMPTY_LIST_SUPPORT, Boolean.TRUE.toString());
     }

     @SuppressWarnings("unchecked")
     public T get(String key) throws Exception {
         Entity ent = null;

         T t = (T) _class.newInstance();

         try {
             t = (T) super.getM(key);

             if (t == null) {
                 t = (T) _class.newInstance();

                 ent = ds.get(KeyFactory.createKey(t.getClass().getSimpleName(), key));

                 t = entityToObject(ent);
             }

         } catch (EntityNotFoundException e) {
             t = (T) _class.newInstance();
         } catch (Exception e) {

             throw e;
         }

         return t;
     }


     public List<T> get(Collection<String> keys) throws Exception {

         List<T> tList = new LinkedList<T>();

         T t = (T) _class.newInstance();

         if (!Validator.isNullOrEmpty(keys)) {
             Map<String, Object> tMap = (Map<String, Object>) super.getM(keys);

             if (Validator.isNullOrEmpty(tMap)) {
                 LinkedList<Key> properKeys = new LinkedList<>();
                 for (String key : keys) {
                     properKeys.add(
                             KeyFactory.createKey(
                                     t.getClass().getSimpleName(), key));
                 }

                 try {
                     Map<Key, Entity> e = ds.get(properKeys);

                     for (Key key : properKeys) {
                         if (!e.isEmpty() && e != null && !Validator.isNull(e.get(key))) {
                             tList.add((T) entityToObject(e.get(key)));
                         }
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }

             } else if (tMap.size() > 0) {
                 tList = new LinkedList<T>();

                 for (Entry<String, Object> entry : tMap.entrySet()) {
                     if (entry.getValue() != null) tList.add((T) entry.getValue());
                 }
             }
         }

         return tList;
     }

     public void set(T t, String key) throws Exception {
         try {
             Entity ent = objectToEntity(t, key);

             Transaction txn = ds.beginTransaction();

             try {
                 ds.put(txn, ent);
                 txn.commit();

                 super.setM(
                         key + t.getClass().getSimpleName().toLowerCase(), SerializationUtils.serialize(t));
             } finally {
                 if (txn.isActive()) {
                     txn.rollback();
                 }
             }
         } catch (Exception e) {
             if (!(e instanceof ConcurrentModificationException)) {
                 // Utils.printException(e);
                 throw e;
             }
         }
     }

     public void set(LinkedHashMap<String, T> t) throws Exception {
         try {
             TransactionOptions ops = TransactionOptions.Builder.withXG(true);

             Transaction txn = ds.beginTransaction(ops);

             try {
                 List<Entity> data = new ArrayList<Entity>();
                 LinkedHashMap<String, byte[]> memData = new LinkedHashMap<String, byte[]>();

                 for (Entry<String, T> entry : t.entrySet()) {
                     Entity ent = objectToEntity(entry.getValue(), entry.getKey());
                     data.add(ent);
                     memData.put(
                             entry.getKey() + "_" + t.getClass().getSimpleName().toLowerCase(),
                             SerializationUtils.serialize(entry.getValue()));
                 }

                 ds.put(txn, data);
                 txn.commit();

                 super.setM(memData);

             } finally {
                 if (txn.isActive()) {
                     txn.rollback();
                 }
             }

         } catch (Exception e) {
             // Utils.printException(e);
             throw e;
         }
     }

     public T entityToObject(Entity entity) throws InstantiationException, IllegalAccessException {
         T t = (T) _class.newInstance();

         try {
             PropertyDescriptor[] descriptor = PropertyUtils.getPropertyDescriptors(t);

             PropertyAccessor myAccessor = PropertyAccessorFactory.forBeanPropertyAccess(t);
             for (PropertyDescriptor des : descriptor) {

                 if (entity.getProperty(des.getName()) != null) {
                     myAccessor.setPropertyValue(des.getName(), entity.getProperty(des.getName()));
                 }
             }

             myAccessor.setPropertyValue("key", entity.getKey().getName());
         } catch (Exception e) {
             e.printStackTrace();
         }

         return t;
     }

     public Entity objectToEntity(T t, String key) {
         Entity entity = new Entity(t.getClass().getSimpleName(), key);

         try {
             PropertyDescriptor[] des = PropertyUtils.getPropertyDescriptors(t);

             Annotation[] annotations = null;

             for (int i = 0; i < des.length; i++) {
                 if (!des[i].getName().equalsIgnoreCase("class")
                         && !des[i].getName().equalsIgnoreCase("key")) {
                     annotations = t.getClass().getDeclaredField(des[i].getName()).getDeclaredAnnotations();

                     for (Annotation s : annotations) {
                         if (s.annotationType().getSimpleName().equalsIgnoreCase("UnIndexedProperty")) {
                             entity.setUnindexedProperty(des[i].getName(), des[i].getReadMethod().invoke(t));
                             break;
                         } else if (s.annotationType().getSimpleName().equalsIgnoreCase("PersistentProperty")) {
                             entity.setProperty(des[i].getName(), des[i].getReadMethod().invoke(t));
                             break;
                         }
                     }
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return entity;
     }

     public void delete(T t, String key) throws Exception {
         try {
             Transaction txn = ds.beginTransaction();

             try {
                 ds.delete(txn, KeyFactory.createKey(t.getClass().getSimpleName(), key));
                 txn.commit();

                 super.deleteM(key);
             } finally {
                 if (txn.isActive()) {
                     txn.rollback();
                 }
             }
         } catch (Exception e) {
             throw e;
         }
     }

     
     public void delete(T t, Map<String, T> map) {
         try {
             Transaction txn = ds.beginTransaction();

             try {
                 for (Entry<String, T> entry : map.entrySet()) {
                     ds.delete(txn, KeyFactory.createKey(t.getClass().getSimpleName(), entry.getKey()));
                     txn.commit();

                     super.deleteM(entry.getKey());
                 }
             } finally {
                 if (txn.isActive()) {
                     txn.rollback();
                 }
             }
         } catch (Exception e) {
             e.printStackTrace();
         }
     }
 }

