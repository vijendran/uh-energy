package dao;

import entity.Lesson;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class LessonDAO extends DAO<Lesson> {

    private LessonDAO() {
        super(Lesson.class);
    }

    private static LessonDAO instance;

    public static synchronized LessonDAO instance() {
        if (instance == null) instance = new LessonDAO();

        return instance;
    }

    public Lesson get(String key) throws Exception {
        Lesson objLesson = super.get(key);

        return objLesson;
    }

    public List<Lesson> get(Collection<String> keys) throws Exception {
        List<Lesson> objLesson = super.get(keys);

        return objLesson;
    }

    public void set(Lesson objLesson) throws Exception {
        super.set(objLesson, objLesson.getKey());
    }

    public void set(Collection<Lesson> objLesson) throws Exception {
        LinkedHashMap<String, Lesson> lessonMap = new LinkedHashMap<String, Lesson>();

        for (Lesson u : objLesson) {
            lessonMap.put(u.getKey(), u);
        }
        super.set(lessonMap);
    }

}
