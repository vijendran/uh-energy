package dao;

import entity.Course;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class CourseDAO extends DAO<Course> {

    private CourseDAO() {
        super(Course.class);
    }

    private static CourseDAO instance;

    public static synchronized CourseDAO instance() {
        if (instance == null) instance = new CourseDAO();

        return instance;
    }

    public Course get(String key) throws Exception {
        Course objCourse = super.get(key);

        return objCourse;
    }

    public List<Course> get(Collection<String> keys) throws Exception {
        List<Course> objCourse = super.get(keys);

        return objCourse;
    }

    public void set(Course objCourse) throws Exception {
        super.set(objCourse, objCourse.getKey());
    }

    public void set(Collection<Course> objCouse) throws Exception {
        LinkedHashMap<String, Course> courseMap = new LinkedHashMap<String, Course>();

        for (Course u : objCouse) {
            courseMap.put(u.getKey(), u);
        }
        super.set(courseMap);
    }

}
