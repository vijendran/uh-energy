package dao;

import entity.Quiz;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class QuizDAO extends DAO<Quiz> {

  private QuizDAO() {
    super(Quiz.class);
  }

  private static QuizDAO instance;

  public static synchronized QuizDAO instance() {
    if (instance == null) instance = new QuizDAO();

    return instance;
  }

  public Quiz get(String key) throws Exception {
    Quiz quiz = super.get(key);

    return quiz;
  }

  public List<Quiz> get(Collection<String> keys) throws Exception {
    List<Quiz> quiz = super.get(keys);

    return quiz;
  }

  public void set(Quiz quiz) throws Exception {
    super.set(quiz, quiz.getKey());
  }

  public void set(Collection<Quiz> quiz) throws Exception {
    LinkedHashMap<String, Quiz> courseMap = new LinkedHashMap<String, Quiz>();

    for (Quiz u : quiz) {
      courseMap.put(u.getKey(), u);
    }
    super.set(courseMap);
  }
}
