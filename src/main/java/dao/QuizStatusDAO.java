package dao;

import entity.QuizStatus;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class QuizStatusDAO extends DAO<QuizStatus> {

  private QuizStatusDAO() {
    super(QuizStatus.class);
  }

  private static QuizStatusDAO instance;

  public static synchronized QuizStatusDAO instance() {
    if (instance == null) instance = new QuizStatusDAO();

    return instance;
  }

  public QuizStatus get(String key) throws Exception {
    QuizStatus quiz = super.get(key);

    return quiz;
  }

  public List<QuizStatus> get(Collection<String> keys) throws Exception {
    List<QuizStatus> quiz = super.get(keys);

    return quiz;
  }

  public void set(QuizStatus quiz) throws Exception {
    super.set(quiz, quiz.getKey());
  }

  public void set(Collection<QuizStatus> quiz) throws Exception {
    LinkedHashMap<String, QuizStatus> courseMap = new LinkedHashMap<String, QuizStatus>();

    for (QuizStatus u : quiz) {
      courseMap.put(u.getKey(), u);
    }
    super.set(courseMap);
  }
}
