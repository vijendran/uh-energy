 package dao;

 import com.google.appengine.api.memcache.Expiration;
 import com.google.appengine.api.memcache.MemcacheService;
 import com.google.appengine.api.memcache.MemcacheServiceFactory;
 import org.springframework.util.SerializationUtils;
 import utility.Validator;

 import java.util.*;

 public class Memcache {
     private static Memcache instance;

     public static synchronized Memcache instance() {
         if (instance == null) instance = new Memcache();

         return instance;
     }

     protected Memcache() {}

     private static MemcacheService cache = MemcacheServiceFactory.getMemcacheService();

     public Object getM(String key) throws Exception {
         if (!Validator.isNullOrEmpty(key)) {
             Object value = cache.get(key);

             if (value != null) return SerializationUtils.deserialize((byte[]) value);
         }

         return null;
     }

     public Map<String, Object> getM(Collection<String> keys) throws Exception {

         List<String> kext = new ArrayList<>();

         for (String key : keys) kext.add(key);

         if (!Validator.isNullOrEmpty(kext)) {
             Map<String, Object> data = cache.getAll(kext);

             Map<String, Object> returner = new LinkedHashMap<>();

             if (data != null) {
                 for (String key : kext) {
                     if (data.containsKey(key) && data.get(key) != null) {
                         returner.put(
                                 key, SerializationUtils.deserialize((byte[]) data.get(key)));
                     }
                 }
             }

             return returner;
         }

         return null;
     }

     public void setM(String key, byte[] data) throws Exception {
         if (!Validator.isNullOrEmpty(key) && data != null) {
             cache.put(key, data);
         }
     }

     public void setM(String key, byte[] data, int expirationTime) throws Exception {
         if (!Validator.isNullOrEmpty(key) && data != null && expirationTime > 0) {
             cache.put(key, data, Expiration.byDeltaSeconds(expirationTime));
         }
     }

     public void setM(Map<String, byte[]> data) throws Exception {
         if (!data.isEmpty()) {
             for (String key : data.keySet()) {
                 if (Validator.isNullOrEmpty(key) || data.get(key) == null) data.remove(key);
             }

             cache.putAll(data);
         }
     }

     public void deleteM(String key) throws Exception {
         if (!Validator.isNullOrEmpty(key) && cache.contains(key)) cache.delete(key);
     }

     public void deleteM(Collection<String> keys) throws Exception {
         if (!Validator.isNullOrEmpty(keys)) {
             Iterator<String> itr = keys.iterator();

             while (itr.hasNext()) {
                 String key = itr.next();

                 if (Validator.isNullOrEmpty(key)) itr.remove();
             }
         }

         if (!Validator.isNullOrEmpty(keys)) cache.deleteAll(keys);
     }
 }

