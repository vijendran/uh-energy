package dao;

import entity.Subscription;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class SubscriptionDAO extends DAO<Subscription> {

    private SubscriptionDAO() {
        super(Subscription.class);
    }

    private static SubscriptionDAO instance;

    public static synchronized SubscriptionDAO instance() {
        if (instance == null) instance = new SubscriptionDAO();

        return instance;
    }

    public Subscription get(String key) throws Exception {
        Subscription objSubscription = super.get(key);

        return objSubscription;
    }

    public List<Subscription> get(Collection<String> keys) throws Exception {
        List<Subscription> objSubscription = super.get(keys);

        return objSubscription;
    }

    public void set(Subscription objSubscription) throws Exception {
        super.set(objSubscription, objSubscription.getKey());
    }

    public void set(Collection<Subscription> subscription) throws Exception {
        LinkedHashMap<String, Subscription> objSubscriptionMap = new LinkedHashMap<String, Subscription>();

        for (Subscription u : subscription) {
            objSubscriptionMap.put(u.getKey(), u);
        }
        super.set(objSubscriptionMap);
    }

}
