package dao;

import entity.CourseStatus;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class CourseStatusDAO extends DAO<CourseStatus> {

    private CourseStatusDAO() {
        super(CourseStatus.class);
    }

    private static CourseStatusDAO instance;

    public static synchronized CourseStatusDAO instance() {
        if (instance == null) instance = new CourseStatusDAO();

        return instance;
    }

    public CourseStatus get(String key) throws Exception {
        CourseStatus objCourseStatus = super.get(key);

        return objCourseStatus;
    }

    public List<CourseStatus> get(Collection<String> keys) throws Exception {
        List<CourseStatus> objCourseStatus = super.get(keys);

        return objCourseStatus;
    }

    public void set(CourseStatus objCourseStatus) throws Exception {
        super.set(objCourseStatus, objCourseStatus.getKey());
    }

    public void set(Collection<CourseStatus> objCourseStatus) throws Exception {
        LinkedHashMap<String, CourseStatus> courseStatusMap = new LinkedHashMap<String, CourseStatus>();

        for (CourseStatus u : objCourseStatus) {
            courseStatusMap.put(u.getKey(), u);
        }
        super.set(courseStatusMap);
    }

}
