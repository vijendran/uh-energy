package utility;

public final class PreConditions {

    public static void checkArgument(boolean expression, String message) {

        if (expression) {
            throw new IllegalArgumentException(message);
        }

    }

}
