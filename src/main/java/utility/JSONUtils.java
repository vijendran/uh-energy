package utility;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class JSONUtils {

    public static Map<String, Object> convertJsonToMap(String json) {

        try {
            return new JacksonObjectMapper().readValue(json, new TypeReference<HashMap<String, Object>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertJsonMapToString(Map<String, Object> jsonMap) {

        try {
            return new JacksonObjectMapper().writeValueAsString(jsonMap);
        } catch (IOException e) {
            return null;
        }
    }

    public static String getJson(Object obj) {
        try {
            return new org.codehaus.jackson.map.ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T convertSafeToType(String json, Class<T> clazz) {
        try {
            return new JacksonObjectMapper().readValue(json, clazz);
        } catch (IOException e) {
            return null;
        }
    }

    public static <T> T convertObjToType(Object data, Class<T> clazz) {

        JacksonObjectMapper objMapper = new JacksonObjectMapper();
        return objMapper.convertValue(data, clazz);
    }

    public static <T> List<T> convertJsonToList(String json, Class<T> type) throws IOException {
        return convertJsonString(json, TypeFactory.defaultInstance().constructCollectionType(List.class, type));
    }

    public static <T> T convertJsonString(String json, JavaType type) throws IOException {
        ObjectMapper jacksonObjectMapper = new ObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        return jacksonObjectMapper.readValue(json, type);
    }

    public static  <T> Map<String, Object> convertFileToMap (File file, Class<T> type ) throws IOException{

        Map<String, Object>     map     =   ( Map<String, Object> ) new ObjectMapper().readValue( file, type);
        return map;
    }

    public static <T> HashMap<String, Object> writeWithView(Class<T> viewClass, Object object) throws IOException {

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true)
                .configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false).setSerializationInclusion(JsonInclude.Include.NON_NULL);

        String json = mapper.writerWithView(viewClass).writeValueAsString(object);

        HashMap<String, Object> map = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {});

        return map;
    }
}
