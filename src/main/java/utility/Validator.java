package utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.appengine.api.datastore.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static boolean isNullOrEmpty(String value) {
        return (value == null || value.trim().length() <= 0);
    }

    public static boolean isBlank(Text value) {
        return (value == null || isBlank(value.getValue()));
    }

    public static boolean isBlank(String value) {
        return (value == null || value.trim().length() <= 0);
    }

    public static boolean isNullOrEmpty(Collection<?> obj) {
        return (obj == null || obj.isEmpty());
    }

    public static boolean isNullOrEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    public static String nullToEmpty(String value) {
        return value == null ? "" : value;
    }

    public static boolean isRequestNullOrEmpty(String... requestParams) {

        boolean resp = false;
        for (int i = 0; i < requestParams.length; i++) {
            if (requestParams[i] == null || requestParams[i] == "" || requestParams[i].trim() == "") {
                resp = true;
                break;
            }
        }
        return resp;
    }

    public static String getJson(Object object) {

        try {
            return new JacksonObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmptyList(ArrayList<?> list) {
        return list == null || list.isEmpty() || list.size() < 1;
    }

    public static boolean isEmptyList(List<?> list) {
        return list == null || list.isEmpty() || list.size() < 1;
    }

    public static boolean isNumber(String num) {
        try {

            if ((Long) Long.parseLong(num) instanceof Long) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidJSONString(String JSONString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(JSONString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidEmail(String email) {
        if (Validator.isNullOrEmpty(email)) {
            return false;
        }

        String FIRST_CHAR = "^[a-zA-Z0-9]";

        String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        Pattern firstCharPattern = Pattern.compile(FIRST_CHAR);
        Matcher firstCharMatcher = firstCharPattern.matcher("" + email.charAt(0));

        if (matcher.matches() && firstCharMatcher.matches()) {

            String localPart = email.split("@")[0]; //checking that local part can not have more that 64 chars
            if (localPart.length() > 64) {
                return false;
            }

            return true;
        }

        return false;
    }

}
