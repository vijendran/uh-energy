package utility;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Utils {

  public static String convertToMD5HashValue(String input) {

    String hashtext = "";
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");

      byte[] messageDigest = md.digest(input.getBytes());

      BigInteger no = new BigInteger(1, messageDigest);

      hashtext = no.toString(16);
      while (hashtext.length() < 32) {
        hashtext = "0" + hashtext;
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return hashtext;
  }
}
