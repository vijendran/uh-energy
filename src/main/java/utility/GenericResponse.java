package utility;

import java.io.Serializable;
import java.util.HashMap;

public class GenericResponse implements Serializable {
    private boolean success = false;
    private int errorCode;
    private String errorMessage = "Something went wrong";
    private HashMap<String, Object> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }

    public static GenericResponse constructResponse(boolean success, String errorCode, String errorMessage,
            Object obj) {

        GenericResponse objgr = new GenericResponse();
        try {
            System.out.println("success " + success + " errorCode " + errorCode + " errorMessage " + errorMessage);

            objgr.setSuccess(success);
            System.out.println();
            if (!Validator.isNullOrEmpty(errorCode)) {
                objgr.setErrorCode(Integer.parseInt(errorCode));
            } else {
                objgr.setErrorCode(200);
            }
            objgr.setErrorMessage(errorMessage);
            if (!Validator.isNull(obj) && obj != "")
                objgr.setData((HashMap<String, Object>) obj);

            return objgr;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return objgr;

    }
}
