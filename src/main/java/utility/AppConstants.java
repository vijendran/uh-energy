package utility;

public class AppConstants {

    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_DENIED = "DENIED";
    public static final String DEFAULT_COURSE_IMAGE = "";
    public static final int DEFAULT_QUIZ_PERCENTAGE = 50;
    public static final String MAIL_SENDER = "imgaje@gmail.com";
    public static final String DRAFTED = "DRAFTED";
    public static final String COURSE_ENABLED = "ENABLED";

    // for quizz types
    public static final String SINGLE_SELECT = "singleselect";
    public static final String MULTI_SELECT = "multiselect";
    public static final String TRUE_OR_FALSE = "trueorfalse";
    public static final String PARAGRAPH = "paragraph";
    public static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static final String version = "60";

    public static final String[] QUIZZ_TYPE = {
            "paragraph", "singleselect", "multiselect", "trueorfalse"
    };
}
