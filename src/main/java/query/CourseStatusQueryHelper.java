package query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.apphosting.api.DatastorePb.QueryResult;

import org.json.JSONObject;

import dao.CourseDAO;
import dao.CourseStatusDAO;
import dao.QuizStatusDAO;
import dao.SubscriptionDAO;
import dao.UserDAO;
import entity.Course;
import entity.CourseStatus;
import entity.QuizStatus;
import entity.User;
import utility.AppConstants;
import utility.JSONUtils;
import utility.Validator;

public class CourseStatusQueryHelper {

    private static CourseStatusQueryHelper instance;

    private CourseStatusQueryHelper() {
    }

    public static synchronized CourseStatusQueryHelper instance() {
        if (instance == null)
            return new CourseStatusQueryHelper();

        return instance;
    }

    public static CourseStatus getActiveLesson(String courseID, String contactID) {

        try {

            Query query = new Query("CourseStatus").setFilter(Query.CompositeFilterOperator.and(
                    new Query.FilterPredicate("courseID", Query.FilterOperator.EQUAL, courseID),
                    new Query.FilterPredicate("contactID", Query.FilterOperator.EQUAL, contactID)));

            query.addSort("dateCompleted", Query.SortDirection.DESCENDING);

            Entity entity = DatastoreServiceFactory.getDatastoreService().prepare(query).asSingleEntity();

            CourseStatus objCourseStatus = (!Validator.isNull(entity))
                    ? CourseStatusDAO.instance().entityToObject(entity)
                    : null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public HashMap<String, Object> getQuizStatus(String cursor) {
        try {

            HashMap<String, Object> responseMap = new HashMap<>();
            int limit = 50;
            Query query = new Query("QuizStatus");

            FetchOptions fetchOptions = FetchOptions.Builder.withLimit(limit);

            if (!Validator.isNullOrEmpty(cursor)) {
                fetchOptions = FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
            }

            query.addSort("dateCompleted", Query.SortDirection.DESCENDING);

            System.out.println(" Query " + query);

            QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query)
                    .asQueryResultList(fetchOptions);

            System.out.println(queryList.size());

            if (!Validator.isNull(queryList)) {
                if (queryList.size() == limit && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                    responseMap.put("cursor", queryList.getCursor().toWebSafeString());
                }

                if (!Validator.isEmptyList(queryList)) {

                    List<HashMap> userList = new ArrayList<>();

                    for (Entity obj : queryList) {
                        if (!Validator.isNull(obj)) {
                            QuizStatus quizStatus = QuizStatusDAO.instance().entityToObject(obj);
                            User objUser = UserDAO.instance().get(quizStatus.getContactID());
                            if (!Validator.isNull(objUser.getKey())) {
                                HashMap<String, Object> responseObject = new HashMap<>();
                                System.out.println("update" + Validator.getJson(objUser));
                                JSONObject json = new JSONObject();
                                responseObject.put("user", objUser);
                                Course course = CourseDAO.instance().get(quizStatus.getCourseID());
                                responseObject.put("course", course);
                                responseObject.put("quizStatus", quizStatus);

                                userList.add(responseObject);
                            }
                        }
                    }

                    System.out.println(userList);

                    responseMap.put("quizStatus", userList);
                }

                return responseMap;
            }
            return responseMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
