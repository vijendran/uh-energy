package query;

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import dao.UserDAO;
import entity.User;
import utility.Validator;

import java.util.HashMap;

public class UserQueryHelper {

    private static UserQueryHelper instance;

    private UserQueryHelper() {
    }

    public static synchronized UserQueryHelper instance() {
        if (instance == null) return new UserQueryHelper();

        return instance;
    }

    public static User queryForUserNameAndPassword(String emailID, String password) {

        try {

            System.out.println(emailID + " " + password);
            Query query =
                    new Query("User")
                            .setFilter(
                                    CompositeFilterOperator.and(
                                            new FilterPredicate("email", FilterOperator.EQUAL, emailID),
                                            new FilterPredicate("password", FilterOperator.EQUAL, password)));

            Entity entity = DatastoreServiceFactory.getDatastoreService().prepare(query).asSingleEntity();

            User user = (!Validator.isNull(entity)) ? UserDAO.instance().entityToObject(entity) : null;

            System.out.println("user " + user);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static User queryForUserName(String emailID) throws IllegalAccessException, InstantiationException {

        Query query =
                new Query("User").setFilter(new FilterPredicate("email", FilterOperator.EQUAL, emailID));

        Entity entity = DatastoreServiceFactory.getDatastoreService().prepare(query).asSingleEntity();

        User user = (!Validator.isNull(entity)) ? UserDAO.instance().entityToObject(entity) : null;

        return user;
    }

    public static HashMap<String, Object> getAdminMailID(String role) throws IllegalAccessException, InstantiationException {

        Query query =
                new Query("User").setFilter(new FilterPredicate("role", FilterOperator.EQUAL, role));

        FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();

        QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query).asQueryResultList(fetchOptions);

        HashMap<String, Object> responeMap = new HashMap<>();

        if (!Validator.isEmptyList(queryList)) {
            responeMap.put("users", queryList);
        }

        return responeMap;
    }

}
