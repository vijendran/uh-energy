package query;

import com.google.appengine.api.datastore.*;
import utility.Validator;

import java.util.HashMap;

public class LessonQueryHelper {

    private static LessonQueryHelper instance;

    private LessonQueryHelper() {
    }

    public static synchronized LessonQueryHelper instance() {
        if (instance == null) return new LessonQueryHelper();

        return instance;
    }

    public static HashMap<String, Object> getLessonsForThisCourseID(String courseID, String cursor) {

        HashMap<String, Object> responseMap = new HashMap<>();

        int limit = 50;

        Query query = new Query("Lesson").setFilter(new Query.FilterPredicate("courseID", Query.FilterOperator.EQUAL, courseID));

        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(limit);

        if (!Validator.isNullOrEmpty(cursor)) {
            fetchOptions = FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
        }

        query.addSort("dateAdded", Query.SortDirection.ASCENDING);

        System.out.println(" Query " + query);

        QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query).asQueryResultList(fetchOptions);


        if (!Validator.isNull(queryList)) {

            responseMap.put("lessons", queryList);

            if (queryList.size() == limit && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                responseMap.put("cursor", queryList.getCursor().toWebSafeString());
            }
        }

        return responseMap;

    }
}
