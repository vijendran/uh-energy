package query;

import com.google.appengine.api.datastore.*;
import utility.Validator;

import java.util.HashMap;

public class QuizQueryHelper {

    private static QuizQueryHelper instance;

    private QuizQueryHelper() {
    }

    public static synchronized QuizQueryHelper instance() {
        if (instance == null) return new QuizQueryHelper();

        return instance;
    }

    public static HashMap<String, Object> getQuizForThisCourseID(String courseID, String cursor) {

        HashMap<String, Object> responseMap = new HashMap<>();

        int limit = 50;

        Query query =
                new Query("Quiz")
                        .setFilter(new Query.FilterPredicate("courseID", Query.FilterOperator.EQUAL, courseID));

        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(limit);

        if (!Validator.isNullOrEmpty(cursor)) {
            fetchOptions =
                    FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
        }

        query.addSort("dateAdded", Query.SortDirection.ASCENDING);

        System.out.println(" Query " + query);

        QueryResultList<Entity> queryList =
                DatastoreServiceFactory.getDatastoreService()
                        .prepare(query)
                        .asQueryResultList(fetchOptions);

        if (!Validator.isNull(queryList)) {

            responseMap.put("quiz", queryList);

            if (queryList.size() == limit
                    && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                responseMap.put("cursor", queryList.getCursor().toWebSafeString());
            }
        }

        return responseMap;
    }

    public static HashMap<String, Object> getQuizStatusForThisCourseID(String courseID, String cursor) {

        HashMap<String, Object> responseMap = new HashMap<>();
        int limit  = 50;

        Query query =
                new Query("QuizStatus");

        FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();

        if (!Validator.isNullOrEmpty(cursor)) {
            fetchOptions =
                    FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
        }

        query.addSort("dateCompleted", Query.SortDirection.DESCENDING);

        System.out.println(" Query " + query);

        QueryResultList<Entity> queryList =
                DatastoreServiceFactory.getDatastoreService()
                        .prepare(query)
                        .asQueryResultList(fetchOptions);

        if (!Validator.isNull(queryList)) {

            responseMap.put("quizStatus", queryList);

            if (queryList.size() == limit
                    && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                responseMap.put("cursor", queryList.getCursor().toWebSafeString());
            }
        }

        return responseMap;
    }
}
