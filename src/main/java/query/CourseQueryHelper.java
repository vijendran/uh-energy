package query;

import com.google.appengine.api.datastore.*;
import utility.Validator;

import java.util.HashMap;

public class CourseQueryHelper {

    private static CourseQueryHelper instance;

    private CourseQueryHelper() {
    }

    public static synchronized CourseQueryHelper instance() {
        if (instance == null) return new CourseQueryHelper();

        return instance;
    }

    public static HashMap<String, Object> getAllCourse(String cursor) {

        HashMap<String, Object> responseMap = new HashMap<>();
        int limit = 50;
        Query query = new Query("Course");

        query.addSort("dateAdded", Query.SortDirection.ASCENDING);

        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(limit);

        if (!Validator.isNullOrEmpty(cursor)) {
            fetchOptions = FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
        }

        System.out.println(" Query " + query);

        QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query).asQueryResultList(fetchOptions);

        if (!Validator.isNull(queryList)) {

            responseMap.put("courses", queryList);

            if (queryList.size() == limit && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                responseMap.put("cursor", queryList.getCursor().toWebSafeString());
            }
        }
        return responseMap;

    }
}
