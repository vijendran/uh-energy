package query;

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import dao.SubscriptionDAO;
import entity.Subscription;
import utility.AppConstants;
import utility.Validator;

import java.util.HashMap;

public class SubscriptionQueryHelper {

    private static SubscriptionQueryHelper instance;

    private SubscriptionQueryHelper() {
    }

    public static synchronized SubscriptionQueryHelper instance() {
        if (instance == null)
            return new SubscriptionQueryHelper();

        return instance;
    }

    public static HashMap<String, Object> getPendingUsers(String cursor, String status) {
        try {

            if (AppConstants.STATUS_PENDING.equalsIgnoreCase(status.trim())) {
                status = AppConstants.STATUS_PENDING;
            } else if (AppConstants.STATUS_DENIED.equalsIgnoreCase(status.trim())) {
                status = AppConstants.STATUS_DENIED;
            } else {
                status = AppConstants.STATUS_APPROVED;
            }
            HashMap<String, Object> responseMap = new HashMap<>();
            int limit = 500;
            Query query = new Query("Subscription")
                    .setFilter(new Query.FilterPredicate("status", Query.FilterOperator.EQUAL, status));

            FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();

            if (!Validator.isNullOrEmpty(cursor)) {
                fetchOptions = FetchOptions.Builder.withStartCursor(Cursor.fromWebSafeString(cursor)).limit(limit);
            }

            System.out.println(" Query " + query);

            QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query)
                    .asQueryResultList(fetchOptions);

            if (!Validator.isNull(queryList)) {

                responseMap.put("users", queryList);

                if (queryList.size() == limit && !Validator.isNullOrEmpty(queryList.getCursor().toWebSafeString())) {
                    responseMap.put("cursor", queryList.getCursor().toWebSafeString());
                }
            }
            return responseMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Subscription getPaymentStatusForThisContactID(String contactID, String courseID) {

        try {

            Query query = new Query("Subscription").setFilter(
                    CompositeFilterOperator.and(new FilterPredicate("contactID", Query.FilterOperator.EQUAL, contactID),
                            new FilterPredicate("courseID", Query.FilterOperator.EQUAL, courseID)));

                            System.out.println(query);

            Entity entity = DatastoreServiceFactory.getDatastoreService().prepare(query).asSingleEntity();

            Subscription objSubscription = (!Validator.isNull(entity))
                    ? SubscriptionDAO.instance().entityToObject(entity)
                    : null;

            return objSubscription;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Subscription getPaymentStatusForThisContactIDWithStatus(String courseID, String contactID,
            String status) {

        try {
            Query query = new Query("Subscription").setFilter(
                    CompositeFilterOperator.and(new FilterPredicate("contactID", Query.FilterOperator.EQUAL, contactID),
                            new FilterPredicate("courseID", Query.FilterOperator.EQUAL, courseID),
                            new FilterPredicate("status", Query.FilterOperator.EQUAL, status)));

            Entity entity = DatastoreServiceFactory.getDatastoreService().prepare(query).asSingleEntity();

            System.out.println("entity :: " + entity);
            Subscription objSubscription = (!Validator.isNull(entity))
                    ? SubscriptionDAO.instance().entityToObject(entity)
                    : null;

            System.out.println("objSubscription :: " + objSubscription);

            return objSubscription;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void tempMethod() {

        try {

            Query query = new Query("Subscription");

            FetchOptions fetchOptions = FetchOptions.Builder.withDefaults();
            QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query)
                    .asQueryResultList(fetchOptions);

            System.out.println("queryList :: " + queryList.size());

            for (Entity obj : queryList) {

                if (!Validator.isNull(obj)) {

                    Subscription subscription = SubscriptionDAO.instance().entityToObject(obj);

                    if (!Validator.isNullOrEmpty(subscription.getKey())) {

                        subscription.setCourseID("45e098c3-8003-4dcb-b87d-30fec38edc32");
                        SubscriptionDAO.instance().set(subscription);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean checkUserAlreadySubscribed(String contactID) {

        try {

            Query query = new Query("Subscription")
                    .setFilter(new FilterPredicate("contactID", Query.FilterOperator.EQUAL, contactID));

            FetchOptions fetchOptions = FetchOptions.Builder.withLimit(2);
            QueryResultList<Entity> queryList = DatastoreServiceFactory.getDatastoreService().prepare(query)
                    .asQueryResultList(fetchOptions);

            if (queryList != null && queryList.size() > 1) {
                return true;
            }

            return false;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
