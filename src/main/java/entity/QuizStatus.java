package entity;

import annotation.PersistentProperty;

import java.io.Serializable;

public class QuizStatus implements Serializable {

  @PersistentProperty private String key;
  @PersistentProperty private String contactID;
  @PersistentProperty private long dateCompleted;
  @PersistentProperty private String status;
  @PersistentProperty private String courseID;

  public long getDateCompleted() {
    return dateCompleted;
  }

  public void setDateCompleted(long dateCompleted) {
    this.dateCompleted = dateCompleted;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getContactID() {
    return contactID;
  }

  public void setContactID(String contactID) {
    this.contactID = contactID;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCourseID() {
    return courseID;
  }

  public void setCourseID(String courseID) {
    this.courseID = courseID;
  }
}
