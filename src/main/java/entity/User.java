package entity;

import annotation.PersistentProperty;

import java.io.Serializable;

public class User implements Serializable {
  private static final long serialVersionUID = 1L;

  @PersistentProperty private String key; // contactID
  @PersistentProperty private String email;
  @PersistentProperty private long dateAdded;
  @PersistentProperty private String phoneNumber;
  @PersistentProperty private String role;
  @PersistentProperty private String password;
  @PersistentProperty private String name;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public long getDateAdded() {
    return dateAdded;
  }

  public void setDateAdded(long dateAdded) {
    this.dateAdded = dateAdded;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
