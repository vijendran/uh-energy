package entity;

import annotation.PersistentProperty;
import com.google.appengine.api.datastore.Text;

import javax.jdo.annotations.NotPersistent;
import java.io.Serializable;
import java.util.ArrayList;

public class Lesson implements Serializable {

    private static final long serialVersionUID = 1L;
    @PersistentProperty
    private String key;
    @PersistentProperty
    private String lessonName;
    @PersistentProperty
    private long dateAdded;
    @PersistentProperty
    private ArrayList<String> lessonImage;
    @PersistentProperty
    private ArrayList<String> lessonVideo;
    @PersistentProperty
    private Text lessonText;
    @PersistentProperty
    private String courseID;
    @NotPersistent
    private String lessonStatus;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public ArrayList<String> getLessonImage() {
        return lessonImage;
    }

    public void setLessonImage(ArrayList<String> lessonImage) {
        this.lessonImage = lessonImage;
    }

    public ArrayList<String> getLessonVideo() {
        return lessonVideo;
    }

    public void setLessonVideo(ArrayList<String> lessonVideo) {
        this.lessonVideo = lessonVideo;
    }

    public Text getLessonText() {
        return lessonText;
    }

    public void setLessonText(Text lessonText) {
        this.lessonText = lessonText;
    }

    public String getcourseID() {
        return courseID;
    }

    public void setcourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getLessonStatus() {
        return lessonStatus;
    }

    public void setLessonStatus(String lessonStatus) {
        this.lessonStatus = lessonStatus;
    }
    
}
