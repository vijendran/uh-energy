package entity;

import annotation.PersistentProperty;
import com.google.appengine.api.datastore.Text;

import java.io.Serializable;
import java.util.List;

public class Quiz implements Serializable {

  private static final long serialVersionUID = 1L;
  @PersistentProperty private String key;
  @PersistentProperty private Text question;
  @PersistentProperty private long dateAdded;
  @PersistentProperty private String type;
  @PersistentProperty private String courseID;
  @PersistentProperty private List<String> options;
  @PersistentProperty private List<String> correctAnswer;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Text getQuestion() {
    return question;
  }

  public void setQuestion(Text question) {
    this.question = question;
  }

  public String getCourseID() {
    return courseID;
  }

  public void setCourseID(String courseID) {
    this.courseID = courseID;
  }

  public long getDateAdded() {
    return dateAdded;
  }

  public void setDateAdded(long dateAdded) {
    this.dateAdded = dateAdded;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<String> getOptions() {
    return options;
  }

  public List<String> getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(List<String> correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public void setOptions(List<String> options) {
    this.options = options;
  }
}
