package entity;

import annotation.PersistentProperty;

import java.io.Serializable;

public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @PersistentProperty
    private String key;
    @PersistentProperty
    private String courseName;
    @PersistentProperty
    private long dateAdded;
    @PersistentProperty
    private String courseImage;
    @PersistentProperty
    private int percentage;
    @PersistentProperty
    private String status;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getCourseImage() {
        return courseImage;
    }

    public void setCourseImage(String courseImage) {
        this.courseImage = courseImage;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
