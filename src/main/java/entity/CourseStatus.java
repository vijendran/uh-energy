package entity;

import annotation.PersistentProperty;

import java.io.Serializable;

public class CourseStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @PersistentProperty
    private String key;
    @PersistentProperty
    private long dateCompleted;
    @PersistentProperty
    private String lessonID;
    @PersistentProperty
    private String courseID;
    @PersistentProperty
    private String contactID;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(long dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getLessonID() {
        return lessonID;
    }

    public void setLessonID(String lessonID) {
        this.lessonID = lessonID;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }


}
