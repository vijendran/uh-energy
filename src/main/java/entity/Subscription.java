package entity;

import annotation.PersistentProperty;

import java.io.Serializable;

public class Subscription implements Serializable {

    private static final long serialVersionUID = 1L;
    @PersistentProperty
    private String key;

    @PersistentProperty
    private String contactID;

    @PersistentProperty
    private String courseID;

    @PersistentProperty
    private long dateAdded;

    @PersistentProperty
    private long dateModified;

    @PersistentProperty
    private String status;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }
    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getDateModified() {
        return dateModified;
    }

    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
