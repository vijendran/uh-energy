package enums;

public class ErrorResponse {

    public enum CommonErrorMsg {

        INVALID_COURSE_ID("Invalid courseID"),
        INVALID_LESSON_ID("Invalid lessonID"),
        INVALID_CONTACT_ID("Invalid contactID"),
        INVALID_EMAIL_ID("Email is not valid"),
        INVALID_PAYLOAD("Payload cannot be empty or null."),
        INVALID_CONSTRAINTS("No proper constraints."),
        NOT_AUTHORIZED("User not authorized"),
        UNABLE_TO_CREATE("Unable to create "),
        INVALID_NAME("Name cannot be empty"),
        NO_COURSE_FOUND("No such course found"),
        NO_LESSON_FOUND("No such lesson found"),
        USER_NOT_FOUND("user not found"),
        NOT_A_ADMIN(" User is not an admin "),
        INVALID_SUBSCRIPTION("Invalid subscription"),
        NO_DATA_FOUND("No data found"),
        INVALID_PASSWORD("Invalid password");

        private String message;

        CommonErrorMsg(String message) {
            this.message = message;
        }

        public String value() {
            return message;
        }
    }

    public enum SubscriptionErrorMsg {

        NO_SUBSCRIBED_USERS("No pending users found"),
        SUBSCRIPTION_FAILED("Failed to update the subscription status");
        private String message;

        SubscriptionErrorMsg(String message) {
            this.message = message;
        }

        public String value() {
            return message;
        }
    }

    public enum CourseMsg {

        INVALID_QUIZ_PERCENTAGE("Invalid quiz percentage"),
        INVALID_COURSE_NAME("Invalid course name");

        private String message;

        CourseMsg(String message) {
            this.message = message;
        }

        public String value() {
            return message;
        }
    }

}
