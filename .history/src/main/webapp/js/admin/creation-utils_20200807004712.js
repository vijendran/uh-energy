document.getElementById('sidebar-add').addEventListener('click', () => {

    let isQuizTab = false;

    if (document.getElementById('quiz-tab').classList.contains('active')) {
        isQuizTab = true;
    }

    if (document.getElementsByClassName('incomplete').length == 0) {

        var template = document.querySelector('#sidebar_li'),
            clonedNode = template.content.cloneNode(true);

        document.querySelectorAll('#sidebar_ul>li').forEach(e => e.classList.remove('active'));
        if (isQuizTab)
            clonedNode.firstElementChild.setAttribute("class", "quiz_li active incomplete");
        else clonedNode.firstElementChild.setAttribute("class", "active incomplete");

        document.getElementById('sidebar_ul').appendChild(clonedNode);

        if (isQuizTab) {
            buildQuestion();
            removeActiveClassForQuizBody();

            let temp = document.getElementById('quiz-view').querySelectorAll('li.each-question');
            temp[temp.length - 1].classList.add('active');

            buildOption('singleselect-create');
            removeActiveClassForSidebar();
            // hilighting the body

            document.querySelector('#sidebar_ul').querySelector('.quiz_li:last-child').classList.add('active');

            document.querySelector('#sidebar_ul').querySelector('.quiz_li:last-child').addEventListener('click', function (node) {
                if (isQuizTab) {
                    bindSidebarClickEvent(node.target);
                }
            });
            document.querySelectorAll('.question-type').forEach(function (node) {
                node.querySelector('select').addEventListener('change', function (e) {
                    selectOptions(e.target);
                });
            });

            document.querySelector('li.each-question').querySelectorAll('.b-input').forEach(function (node) {
                node.addEventListener('click', (e) => {
                    checkUncheck(e.target.previousElementSibling);
                });
            });
           
        }
    }
});

showTab = (tab) => {
    let node = document.querySelectorAll('.creation-main')[0];
    node.classList.forEach(function (className) {
        console.log(className);
        if (className.indexOf('viewport') != -1) {
            node.classList.remove(className);
        }
        node.classList.add('viewport-' + tab);
    });

    document.getElementById('header-tabs').querySelectorAll('a').forEach(function (node) {
        node.classList.remove('active');
    });
    document.getElementById(tab + '-tab').classList.add('active');


    if ('quiz' == tab)
        document.getElementById('sidebar-add').innerHTML = '+ Add Quiz';
    else if ('lesson' == tab)
        document.getElementById('sidebar-add').innerHTML = '+ Add Lesson';
};