function checkUncheck(node) {
  let type = node.parentNode.parentNode.getAttribute('type');

  if (type.trim().toLowerCase() != "multiselect") {
    document.querySelectorAll('.checked').forEach(e => e.classList.remove('checked'));
    node.classList.add('checked');
  } else {
    if (node.classList.contains('checked'))
      node.classList.remove('checked');
    else node.classList.add('checked');
  }
}

function buildQuestion(allQuestion) {
  // build questions
  let questionTemplate = document.querySelector('#each-question');

  var questionClonedNode = questionTemplate.content.cloneNode(true);

  if (allQuestion && allQuestion.question && allQuestion.question.value) {
    if (document.getElementsByName(allQuestion.key).length != 0)
      questionClonedNode = document.getElementsByName(allQuestion.key)[0];
    questionClonedNode.querySelector('input.question-input').value = allQuestion.question.value;
    questionClonedNode.firstElementChild.setAttribute('name', allQuestion.key);

    if (document.getElementsByName(allQuestion.key).length == 0)
      document.getElementById('quiz-view').appendChild(questionClonedNode);
  } else document.getElementById('quiz-view').appendChild(questionClonedNode);



  if (allQuestion) {
    var sidebar_template = document.querySelector('#sidebar_li'),
      sidebar_clonedNode = sidebar_template.content.cloneNode(true);
    sidebar_clonedNode.firstElementChild.setAttribute("id", allQuestion.key);
    sidebar_clonedNode.firstElementChild.setAttribute("class", 'quiz_li');
    sidebar_clonedNode.firstElementChild.innerText = allQuestion.question.value;
    if (!document.getElementById(allQuestion.key))
      document.querySelector('#sidebar_ul').appendChild(sidebar_clonedNode);
  }

}

function buildOption(optionType, allQuestion) {
  console.log(optionType);

  //build options
  if (allQuestion && allQuestion.type)
    optionType = optionType + '-create';
  let template = document.querySelector('#' + optionType);
  let numberOfIteration = 4;
  if ('paragraph-create' == optionType)
    numberOfIteration = 1;
  if ('trueorfalse-create' == optionType)
    numberOfIteration = 2;

  for (let i = 0; i < numberOfIteration; i++) {
    var clonedNode = template.content.cloneNode(true);
    clonedNode.firstElementChild.setAttribute('id', 'options' + (i + 1));
    clonedNode.firstElementChild.setAttribute('type', optionType.replace('-create', ''));

    if (allQuestion && allQuestion.options && allQuestion.options[i]) {
      clonedNode.querySelector('.input-option').innerHTML = allQuestion.options[i];
      clonedNode.querySelector('.input-option').setAttribute('answer', allQuestion.options[i]);
      clonedNode.querySelector('.input-option').value = allQuestion.options[i];
    }

    if ('trueorfalse-create' == optionType) {
      let answerForBoolean = (i == 0) ? 'True' : 'False';
      clonedNode.querySelector('.input-option').innerHTML = answerForBoolean;
      clonedNode.querySelector('.input-option').setAttribute('answer', answerForBoolean);
      clonedNode.querySelector('.input-option').value = answerForBoolean;
    }

    if (allQuestion && allQuestion.key)
      document.getElementsByName(allQuestion.key)[0].querySelector('.lesson-main').appendChild(clonedNode);
    else document.querySelector('li.each-question.active').querySelector('.lesson-main').appendChild(clonedNode);
    // else document.querySelector('li.each-question.active:last-child').querySelector('.lesson-main').appendChild(clonedNode);
  }

  document.getElementById('quiz_main_ul').querySelectorAll('.b-input').forEach(function (node) {
    node.addEventListener('click', (e) => {
      checkUncheck(e.target.previousElementSibling);
    });
  });

  document.querySelectorAll('.question-type')[0].setAttribute('option-type', optionType.replace('-create', ''));

  if (allQuestion && allQuestion.correctAnswer) {
    for (let eachAns in allQuestion.correctAnswer) {
      document.querySelectorAll("input[answer]").forEach(function (node) {
        if (allQuestion.correctAnswer[eachAns] == node.value)
          node.nextElementSibling.firstElementChild.classList.add('checked');
      });
      // document.querySelectorAll("input[answer=" + allQuestion.correctAnswer[eachAns] + "]")[0].
    }
  }
  if (allQuestion && allQuestion.key) {
    document.getElementsByName(allQuestion.key)[0].querySelectorAll('option').forEach(function (node) {
      node.removeAttribute('selected');
    });
    document.getElementsByName(allQuestion.key)[0].querySelectorAll('option[value=' + optionType + ']')[0].setAttribute('selected', 'selected');
  }

}

function selectOptions(node) {
  console.log(node);

  document.querySelectorAll('.question-type')[0].querySelectorAll('option').forEach(function (node) {
    node.removeAttribute('selected');
  });

  let eNode = document.querySelectorAll('.question-type')[0];
  eNode.setAttribute('option-type', node.value.replace('-create', ''));
  node.parentNode.nextElementSibling.innerHTML = '';
  buildOption(node.value);
}

validQuestion = () => {
  let input = document.querySelector('.question-input').value;

  if (input && input.trim().length > 0) {
    return input;
  } else return '';
};


validOptions = () => {
  let input = document.getElementById('quiz_main_ul');

  let optionsArray = [];

  input.querySelectorAll('li > input').forEach(function (node) {
    console.log(node.value);
    if (node.value && node.value.trim().length > 0)
      optionsArray.push(node.value);
  });

  return optionsArray;
};



getSelectedCorrectAns = () => {

  let correctAns = [];
  document.getElementById('quiz_main_ul').querySelectorAll('input.checked').forEach(function (node) {

    if (node.parentNode.previousElementSibling.value)
      correctAns.push(node.parentNode.previousElementSibling.value);
    console.log('ans', node.parentNode.previousElementSibling.value);
  });
  return correctAns;
};

saveOrUpdateQuiz = async () => {
  // check valid question
  let quizJSON = {};
  let question = validQuestion();
  if (!question) {
    statusMsg('Question cannot be empty', "error");
    return;
  }
  quizJSON.question = question;

  // valid options
  let options = validOptions();
  if (!validOptions) {
    statusMsg('Invalid options', "error");
    return;
  }

  quizJSON.options = options;

  let type = document.querySelectorAll('.question-type')[0].getAttribute('option-type');

  console.log(type);

  quizJSON.type = type;
  // valid correctAns

  let correctAns = getSelectedCorrectAns();

  if (!correctAns || ('paragraph' != type && correctAns.length <= 0)) {
    statusMsg('Please select proper correct answer', "error");
    return;
  }

  quizJSON.correctAns = correctAns;

  quizJSON.courseID = window.location.search.split('=')[1];
  let response = await xhr("POST", "/api/quiz", quizJSON);

  if (response && response.success) {
    console.log(response);
  } else statusMsg(capitalize(response.errorMessage), "error");
};

document.getElementById('save').addEventListener('click', function () {
  saveOrUpdateQuiz();
  // check valid correct ans
});

document.getElementById('quiz-tab').addEventListener('click', async function () {
  showTab('quiz');
  document.querySelectorAll('#sidebar_ul>li').forEach(e => {
    if (!e.classList.contains('add-lesson')) {
      e.remove();
    }
  });
  let response = await xhr("GET", "/api/quiz?courseID=" + window.location.search.split('=')[1]);

  if (response && response.success) {
    console.log(response);
    let quizObject = response.data.quiz;

    for (let data in quizObject) {
      buildQuestion(quizObject[data]);
      buildOption(quizObject[data].type, quizObject[data]);
    }

    document.querySelector('#sidebar_ul').querySelectorAll('.quiz_li').forEach(function (node) {
      node.addEventListener('click', async () => {
        bindSidebarClickEvent(node);
      });
    });

    document.getElementById('quiz-view').querySelectorAll('li')[0].classList.add('active');
    document.getElementById('sidebar_ul').querySelectorAll('li')[1].classList.add('active');


  } else {
    buildQuestion();
    buildOption('singleselect-create');
  }


  document.querySelectorAll('.question-type').forEach(function (node) {
    node.querySelector('select').addEventListener('change', function (e) {
      selectOptions(e.target);
    });
  });
  // document.querySelector('.question-type').querySelector('select').addEventListener('change', function (e) {
  //   selectOptions(e.target);
  // });

});


function removeActiveClassForQuizBody() {
  if (document.getElementById('quiz-view').querySelectorAll('li')) {
    document.getElementById('quiz-view').querySelectorAll('li').forEach(function (node) {
      node.classList.remove('active');
    });
  }
}

function bindSidebarClickEvent(node) {

  if (node) {
    removeActiveClassForSidebar();
    node.classList.add('active');

    if (node.getAttribute('id')) {
      removeActiveClassForQuizBody();
      document.getElementsByName(node.getAttribute('id'))[0].classList.add('active');

    } else {
      removeActiveClassForQuizBody();
      document.getElementById('quiz-view').querySelector('li.each-question:last-child').classList.add('active');
    }
  }
}

function removeActiveClassForSidebar() {
  if (document.querySelector('#sidebar_ul').querySelectorAll('.quiz_li')) {
    document.querySelector('#sidebar_ul').querySelectorAll('.quiz_li').forEach(function (node) {
      if (!node.classList.contains('add-lesson'))
        node.classList.remove('active');
    });
  }
}



// document.getElementById('delete_btn').addEventListener('click', ()=> {

//   document.getElementById('les_name').value = "";

//    if(document.querySelectorAll('#quiz-view>li').length != 0) {
//       document.querySelectorAll('#quiz-view>li').forEach(e => e.remove());
//    }

//    var prevElement = document.querySelector('#sidebar_ul>li.active').previousElementSibling,
//        nextElement =document.querySelector('#sidebar_ul>li.active').nextElementSibling;

//    document.querySelector('#sidebar_ul>li.active').remove();

//    if(document.querySelectorAll('#sidebar_ul>li').length == 1) {
//       document.getElementById('sidebar-add').click();
//    } else {

//       if(prevElement) {

//           if(!prevElement.classList.contains('add-lesson')) {
//               prevElement.click();
//           } else {
//               if(nextElement){
//                   nextElement.click();
//               } else {
//                   document.getElementById('sidebar-add').click();
//               }
//           }
//       }
//    }


// });

document.getElementById('quiz-cancel').addEventListener('click', function (e) {
  loadOnCancel();
});

async function loadOnCancel() {

  if (document.querySelector('#sidebar_ul>li.active').getAttribute('id')) {
    let response = await xhr("GET", "/api/quiz/each?quizID=" + document.querySelector('#sidebar_ul>li.active').getAttribute('id'));

    // document.getElementById('quiz-view').querySelectorAll('li').forEach(function (node) {
    //   node.classList.remove('active');
    // });
    if (response && response.success) {

      // document.getElementsByName(response.data.quiz.key)[0].classList.add('active');
      document.getElementsByName(response.data.quiz.key)[0].querySelector('.lesson-main').innerHTML = '';
      buildQuestion(response.data.quiz);
      buildOption(response.data.quiz.type, response.data.quiz);

      // removeActiveClassForQuizBody();

      // document.getElementsByName(response.data.quiz.key)[0].classList.add('active');
    }
  } else {
    document.querySelector('#sidebar_ul>li.active').remove();
    document.querySelector('#quiz-view>li.active').remove();
  }
}